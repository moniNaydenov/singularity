<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Sample configuration file
 *
 * Copy this file and rename to config.php
 *
 * @project: singularity
 * @author: Simeon Naydenov (moniNaydenov@gmail.com)
 */


unset($CFG);
global $CFG;

$CFG = new stdClass();


/**
 * wwwroot is the complete website url, written without a trailing slash, e.g. 'http://google.com'
 */

$CFG->wwwroot = 'http://google.com';

/**
 * dataroot - directory, where singularity stores files
 */

$CFG->dataroot = '';

/**
 * title - title of the website, e.g. Singularity. To be used with page titles and navbar
 */

$CFG->title = 'singularity';

/**
 * Salts to be used when generating token hashes
 */

$CFG->onetimetokensalt = 'some_not-as-long_random_string_like_this_fsdjfds#J@*!JDA(j2188JAW';

/**
 * Database connection details
 */

$CFG->dbusername = '';
$CFG->dbpassword = '';
$CFG->dbhost = 'localhost';
$CFG->dbport = '3306';
$CFG->dbname = 'singularity';

/**
 * site name - path to the site to be loaded when loading singularity
 * if empty, default index page will be loaded
 */

$CFG->sitename = '';

/**
 * additional css files to be loaded with every page
 */

$CFG->additionalcss = [];

/**
 * additional js files to be loaded with every page
 */

$CFG->additionaljs = [];

/**
 * additional css files to be loaded with every page - external means cannot be accessed using $CFG->wwwroot
 */

$CFG->externalcss = [];

/**
 * external js files to be loaded with every page - external means cannot be accessed using $CFG->wwwroot
 */

$CFG->externaljs = [];

/**
 * favicon - path to favicon file
 */

$CFG->favicon = '/favicon.ico';

/**
 * languages - array of all languages supported by that site
 */

$CFG->languages = ['en'];

/**
 * defaultlang - default language of the site
 */

$CFG->defaultlang = 'en';


/**
 * defaulttimezone - default timezone
 */
$CFG->defaulttimezone = 'Europe/Sofia';

/**
 * debug - enable debugging mode
 */
$CFG->debug = false;

/**
 * Call setup.php to continue page initialization
 */

define('INTERNAL', true);

require_once(__DIR__ . '/lib/setup.php');

