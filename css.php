<?php

ini_set('max_execution_time', 300);

define('DISABLE_GLOBAL_COUNTER', true);

require_once(__DIR__ . '/config.php');

$path = required_param('p', PARAM_PATH);

@session_write_close(); // close session (prevent browser from hanging when serving files...)
@session_abort();

$scssserver = new \core\scssserver();
$compiled = $scssserver->get_compiled_css($path, false);

if ($compiled) {
    header('Content-Type: text/css');
    echo $compiled;
    die;
} else {
    security::die_404('/* File ' . $path . ' not found */');
}