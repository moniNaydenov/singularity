<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 2:58 PM
 */


require_once(__DIR__ . '/config.php');


$pageid = optional_param('pageid', $CFG->homepageid, PARAM_RAW);


if ($pageid) {

    $sp = \core\site_page::create_from_pageid_lang($pageid, $CFG->lang);

    if ($sp) {

        $sp->set_page_metatags();

        $PAGE->set_url('/page/' . $pageid);
        if ($pageid == $CFG->homepageid) {
            $PAGE->set_url('/');
        }

        $PAGE->set_title($sp->get_title());
        $PAGE->set_heading($sp->get_title());

        $PAGE->set_meta_opengraph_basic_tags($sp->get_title(), $sp->get_metadescription());

        $sp->add_breadcrumb();

        $output = $PAGE->get_output();

        echo $output->header();

        echo $output->render_site_page($sp);

        echo $output->footer();
        die;
    }
} else if (!empty($CFG->homescriptpath)) {
    require_once($CFG->homescriptpath);
    die;
}

require_login();
require_permission('core/admin:manage');

$PAGE->title = 'main page';

$output = $PAGE->get_output();

echo $output->header();

echo 'hello world';

echo $output->footer();