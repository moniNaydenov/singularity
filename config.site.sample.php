<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Sample site configuration file
 *
 * Copy this file inside site folder! and rename to config.php
 *
 * @project: singularity
 * @author: Simeon Naydenov (moniNaydenov@gmail.com)
 */

unset($SITE);
global $SITE;


$SITE = new stdClass();


/**
 * site name - name of the website, e.g. Singularity. To be used with page titles and navbar.
 * It will replace $CFG->sitename. Leave empty if you want to have nothing prepended to title
 */

$SITE->title = 'site name';


/**
 * homepageid - site page identifier to be used as home
 */

$SITE->homepageid = 'home';

/**
 * homescriptpath - path to a php script to be executed instead of a standard site page. Path is relative to $SITE->dirroot.
 * takes precedence over homepageid. e.g. /index.php
 */

$SITE->homescriptpath = '';

/**
 * additional css files to be loaded with every page
 * Everything will be appended to $CFG->additionalcss
 */

$SITE->additionalcss = [];

/**
 * additional js files to be loaded with every page
 */

$SITE->additionaljs = [];

/**
 * additional css files to be loaded with every page - external means cannot be accessed using $CFG->wwwroot
 */

$SITE->externalcss = [];

/**
 * external js files to be loaded with every page - external means cannot be accessed using $CFG->wwwroot
 */

$SITE->externaljs = [];

/**
 * favicon - path to favicon file
 */

$SITE->favicon = '/favicon.ico';

/**
 * languages - array of all languages supported by that site
 */

$SITE->languages = ['en'];

/**
 * defaultlang - default language of the site
 */

$SITE->defaultlang = 'en';

// !!! DO NOT CHANGE AFTER THIS LINE !!!

$SITE->dirroot = dirname(__FILE__);
require_once(__DIR__ . '/../config.php');

