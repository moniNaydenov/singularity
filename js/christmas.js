define('js/christmas', ['jquery'], function($) {
  var Christmas = function() {
      var instance = this;

      this.$flakes = [];

      $(document).ready(function() {
          instance.init();
      });
  };

  Christmas.prototype.createSnowflake = function() {
      var coin = Math.random();
      var flaketype = coin < 0.33 ? 'far fa-snowflake' : (coin < 0.66 ? 'fas fa-atom' : 'far fa-sun');
      var $snowflake = $('<i class="' + flaketype + ' snowflake"></i>');

      this.$body.append($snowflake);
      this.initSnowflake($snowflake);
  };

  Christmas.prototype.initSnowflake = function($snowflake) {
      var instance = this;
      $snowflake.css('left', (this.$body.width() * Math.random()) + 'px');
      var duration = Math.ceil(8 + Math.random() * 11);
      setTimeout(function() {
          $snowflake.css('transition-duration', duration + 's');
          $snowflake.addClass('animated');
          setTimeout(function () {
              instance.refreshSnowflake($snowflake);
              //$snowflake.remove();
              //instance.createSnowflake();
          }, (duration * 1000+50));
      }, 500);
  };

  Christmas.prototype.refreshSnowflake = function($snowflake) {
      $snowflake.css('transition-duration', '0s');
      $snowflake.removeClass('animated');
      this.initSnowflake($snowflake);
  };

  Christmas.prototype.init = function() {
      var instance = this;
      var i;
      this.$body = $('body');
      var bodyWidth = this.$body.width();
      var maxFlakes = 0;
      if (bodyWidth < 577) {
          maxFlakes = 20;
      } else if (bodyWidth < 720) {
          maxFlakes = 30;
      } else if (bodyWidth < 1140) {
          maxFlakes = 40;
      } else {
          maxFlakes = 50;
      }

      for (i = 0; i < 5; i++) {
          setTimeout(function(){ instance.createSnowflake();}, Math.ceil(Math.random() * 2000));
      }
      for (i = 0; i < maxFlakes; i++) {
          setTimeout(function(){ instance.createSnowflake();}, Math.ceil(2000 + Math.random() * 19000));
      }

  };
  return Christmas;
});