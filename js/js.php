<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


/**
 * php data to be transfered to php
 *
 * Copy this file and rename to config.php
 *
 * @project: singularity
 * @author: Simeon Naydenov (moniNaydenov@gmail.com)
 */

define('DISABLE_GLOBAL_COUNTER', true);

require_once(__DIR__ . '/../config.php');

header('Content-Type: application/javascript');

$include_js_bootstrap = get_config('include_js_bootstrap', true);
$include_js_jquery = get_config('include_js_jquery', true);

$bootstrapjs = $include_js_bootstrap ? ', \'bootstrap\'' : '';
$jqueryjs = $include_js_jquery ? '\'jquery\'' : '';

echo <<<"MAINJS"
define('core', [{$jqueryjs}{$bootstrapjs}], function ($) {

    if (window.preRequire.length > 0) {
    
        for (var i in window.preRequire) {
            var preReq = window.preRequire[i];
            require(preReq[0], preReq[1]);
        }
    }
});
MAINJS;
