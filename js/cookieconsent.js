define('js/cookieconsent', ['jquery'], function($) {
    $(document).ready(function() {
        $('#cookieconsent a.btn').click(function() {
            $.ajax({
                url: window.CFG.wwwroot + '/api/cookieconsent.php',
                data: {consent: 1, sessionid: window.CFG.sessionid},
                complete: function() {
                    $('#cookieconsent').removeClass('d-flex').addClass('d-none');
                }
            });
        });
    });
});