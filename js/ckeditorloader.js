define('ckeditor', [ 'ckeditormain', 'ckeditoradapter' ], function() {
    window.CKEDITOR.timestamp = window.CFG.revision;
    return true;
});