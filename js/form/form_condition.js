define('form/form_condition', ['jquery'], function($) {


    var FormCondition = function(data) {
        var instance = this;
        this.targetfield = data.targetfield;
        this.controlfield = data.controlfield;
        this.controlvalue = data.controlvalue;
        this.comparator = data.comparator;
        this.hide = data.hide;

        $(document).ready(function() {
            instance.init();
        });
    };

    FormCondition.prototype.check = function() {
        var value = null;
        if (this.$controlfield.attr('type') === 'radio') {
            value = this.$controlfield.filter(':checked').val();
        } else {
            value = this.$controlfield.val();
        }

        var valid = false;
        switch (this.comparator) {
            case 'eq':
                if (typeof value === 'string') {
                    valid = value.toUpperCase() === this.controlvalue.toUpperCase();
                } else {
                    valid = value === this.controlvalue;
                }
                break;
            case 'neq':
                valid = value !== this.controlvalue;
                break;
            case 'lt':
                valid = value < this.controlvalue;
                break;
            case 'lte':
                valid = value <= this.controlvalue;
                break;
            case 'gt':
                valid = value > this.controlvalue;
                break;
            case 'gte':
                valid = value >= this.controlvalue;
                break;
            case 'contains':
                valid = value.toUpperCase().indexOf(this.controlvalue.toUpperCase()) > -1;
                break;
            case 'containsnot':
                valid = value.toUpperCase().indexOf(this.controlvalue.toUpperCase()) === -1;
                break;
            case 'in':
                valid = this.controlvalue.indexOf(value) > -1;
                break;
            case 'notin':
                valid = this.controlvalue.indexOf(value) === -1;
                break;
            default:
                console.log('unknown comparator', this.comparator);
        }
        if (valid) {
            if (this.hide) {
                this.$targetfieldrow.hide();
            } else {
                this.$targetfield.prop('disabled', true);
            }
        } else {
            if (this.hide) {
                this.$targetfieldrow.show();
            } else {
                this.$targetfield.prop('disabled', false);
            }
        }
    };

    FormCondition.prototype.init = function() {
        var instance = this;
        this.$targetfield = $('[name="' + this.targetfield + '"]');
        this.$targetfieldrow = $('[data-fieldname="' + this.targetfield + '"]');
        if (this.$targetfield.length === 0) {
            this.$targetfield = this.$targetfieldrow.find('input'); // in case not found (checkbox groups...
        }
        if (this.$targetfield.length === 0) {
            this.$targetfield = this.$targetfieldrow.find('select'); // in case of select (dropdown, datetime...
        }

        this.$controlfield = $('[name="' + this.controlfield + '"]');
        this.$controlfield.on('change', function(e) { instance.check(); }); //TODO: implement for more complex form fields ;)
        this.check();
    };

    return FormCondition;
});