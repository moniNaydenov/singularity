define('form/file', [ 'jquery' ], function($) {
    function FileField(fieldName, parenthash) {
        var instance = this;
        this.fieldName= fieldName;
        this.parenthash = parenthash;
        $(document).ready(function() {instance.init()});
    }

    FileField.prototype.openPopup = function() {
        var root = '';
        if (this.parenthash.length > 0) {
            root = '&root=' + this.parenthash;
        }
        var url = window.CFG.wwwroot + '/admin/filebrowser.php?browseonly=true&submittoform=true&CKEditorFuncNum=' + this.fieldName + '&langCode=en' + root;
        var windowName = 'FileBrowser';
        var newwindow = window.open(url,windowName,'height=600,width=1024');
        if (window.focus) {
            newwindow.focus();
        }
    };
    FileField.prototype.clearField = function() {

        if (this.$label.length > 0) {
            this.$label.text('');
        } else if (this.$image.length > 0) {
            this.$image.attr('src', '');
        }
        this.$field.val('');
    };

    FileField.prototype.init = function() {
        var instance = this;
        this.$openbtn = $('#btn-open-' + this.fieldName).click(function(e) {e.preventDefault(); instance.openPopup()});
        this.$clearbtn = $('#btn-clear-' + this.fieldName).click(function(e) {e. preventDefault(); instance.clearField()});
        this.$field = $('#' + this.fieldName);
        this.$label = $('#label' + this.fieldName);
        this.$image = $('#image' + this.fieldName);
        window['submitFormFile_'+this.fieldName] = function(uniquehash, filename, href, parenthash) {
            instance.$field.val(uniquehash);
            instance.parenthash = parenthash;
            if (instance.$label.length > 0) {
                instance.$label.text(filename);
            }
            if (instance.$image.length > 0) {
                instance.$image.attr('src', href);
            }
        };
    };

    return FileField;
});