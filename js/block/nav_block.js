define('block/nav_block', ['jquery'], function($) {
    function toggleShowOnToggler($collapse, show) {
        var id = $collapse.attr('id');
        var $toggler = $collapse.siblings('[href="#' + id + '"]');
        var $icon = $toggler.find('i');
        if (show) {
            $icon.addClass('fa-rotate-90');
        } else {
            $icon.removeClass('fa-rotate-90');
        }
    }
    $(document).ready(function() {

        var $collapseParents = $('.nav-block .collapse .list-group-item.active').parentsUntil('.nav_block', '.collapse');
        var $collapseMenus = $('.nav-block .collapse');
        toggleShowOnToggler($collapseParents, true);
        $collapseParents.addClass('show');
        $collapseMenus.on('show.bs.collapse', function(e) {
            toggleShowOnToggler($(this), true);
        });
        $collapseMenus.on('hide.bs.collapse', function(e) {
            toggleShowOnToggler($(this), false);
        });
    });
});