/**
 * Created by moni on 5/21/17.
 */
require(['jquery'], function($) {
    var $filebrowser;
    var $fileItems;
    var $ff;
    var $contextmenu;
    var browseonly = false;
    var submittoform = false;
    var $contextTarget;
    var openedClass = 'contextmenu-opened';
    var $form;

    function submitFileToCKE(e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var funcNum = $filebrowser.data('ckfuncnum');
        window.opener.CKEDITOR.tools.callFunction( funcNum, $this.attr('href'));
        window.close();
    }

    function submitFileToForm(e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var funcNum = $filebrowser.data('ckfuncnum');
        window.opener['submitFormFile_' + funcNum]($this.data('uniquehash'), $this.data('filename'), $this.attr('href'), $this.data('parenthash'));
        window.close();
    }

    function closeOpenedContextMenus(e) {
        if (e !== undefined) {
            e.preventDefault();
        }
        $('.contextmenu.' + openedClass).removeClass(openedClass);
        $('.contextmenu-backdrop.' + openedClass).removeClass(openedClass);
    }


    function openContextMenu(e) {
        e.preventDefault();
        e.stopPropagation();

        var $this = $(this);
        var $target = $(e.target);

        var $menu = $('#file-browser-context-menu');

        closeOpenedContextMenus();

        if ($menu.length === 0) {
            console.log('aaaa');
            return;
        }
        $contextTarget = $this;
        $menu.addClass(openedClass).css('top', e.originalEvent.clientY + 'px').css('left', e.originalEvent.clientX + 'px');
        $('.contextmenu-backdrop').addClass(openedClass);

        $('#filename-delete').text($this.data('filename'));
        $('#filename-rename').text($this.data('filename'));
        $('#filename-rename-input').val($this.data('filename'));
    }

    function confirmDelete() {
        $('#field_uniquehash').val($contextTarget.data('uniquehash'));
        $('#field_delete').val('1');
        $('#field_rename').val('0');
        $('#field_newfilename').val('');
        $('#field___sf__fileeditform').parent('form').submit(); //dirty hack to submit form
    }


    function confirmRename() {
        $('#field_uniquehash').val($contextTarget.data('uniquehash'));
        $('#field_delete').val('0');
        $('#field_rename').val('1');
        $('#field_newfilename').val($('#filename-rename-input').val());
        $('#field___sf__fileeditform').parent('form').submit(); //dirty hack to submit form
    }

    function toggleFilePreview(e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.file-item img.preview').each(function() {
                var $this = $(this);
                $this.attr('src', $this.data('previewurl'));
            });
        } else {

            $('.file-item img.preview').each(function() {
                var $this = $(this);
                $this.attr('src', '');
            });
        }
    }

    $(document).ready(function() {
        $filebrowser = $('.file-browser');
        $fileItems = $('.file-browser .file-item');

        $ff = $('#field_customfile');
        $contextmenu = $('#file-browser-context-menu');

        $ff.change(function() {
            if ($ff.val() !== '') {
                $($ff.parents('form')).submit();
            }
        });

        browseonly = $filebrowser.hasClass('browse-only');
        submittoform = $filebrowser.hasClass('submit-to-form');

        if (submittoform) {
            $filebrowser.find('.file-item.type-file').click(submitFileToForm);
        } else if (browseonly) {
            $filebrowser.find('.file-item.type-file').click(submitFileToCKE);
        }
        $fileItems.contextmenu(openContextMenu);
        $('.contextmenu button').click(closeOpenedContextMenus);
        $('.contextmenu-backdrop').click(closeOpenedContextMenus).contextmenu(closeOpenedContextMenus);
        $('.confirm-delete-btn').click(confirmDelete);
        $('.confirm-rename-btn').click(confirmRename);

        $('#previewthumbnails').change(toggleFilePreview);
    });

});