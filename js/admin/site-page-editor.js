define('admin/site-page-editor', ['jquery', 'beautify', 'beautify-css', 'beautify-html', 'ckeditor', 'ace/ace' ],
    function($, js_beautify, css_beautify, html_beautify) {
    var openedClass = 'contextmenu-opened';
    var insertBlocktype;
    var insertBlockid;
    var currentACEeditor;

    var $spContent;
    var $contextTarget;
    var $formblockcontent;
    var $insertBlockModal;
    var $attributesModal;
    var $newblockError;


    function closeOpenedContextMenus(e) {
        if (e !== undefined) {
            e.preventDefault();
        }
        /*$('.contextmenu.' + openedClass).removeClass(openedClass);
        $('.spe.' + openedClass).removeClass(openedClass);
        $('.contextmenu-backdrop.' + openedClass).removeClass(openedClass);*/
        $('.' + openedClass).removeClass(openedClass);
    }

    function openContextMenu(e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        var menutype = null;

        if ($this.hasClass('row')) {
            menutype = 'row';
        } else if ($this.hasClass('col') ||
            $this.hasClass('col-1') ||
            $this.hasClass('col-2') ||
            $this.hasClass('col-3') ||
            $this.hasClass('col-4') ||
            $this.hasClass('col-5') ||
            $this.hasClass('col-6') ||
            $this.hasClass('col-7') ||
            $this.hasClass('col-8') ||
            $this.hasClass('col-9') ||
            $this.hasClass('col-10') ||
            $this.hasClass('col-11') ||
            $this.hasClass('col-12')
        ) {
            menutype = 'col';
        } else if ($this.hasClass('spe-html_block') ||
                   $this.hasClass('spe-inline_block')) {
            menutype = 'block';
            if ($this.hasClass('spe-block-editing')) {
                return; //do not show if already editing
            }
            if ($this.hasClass('spe-inline_block')) {
                $this = $(e.target);

            }
        } else if ($this.is($spContent)) {
            menutype = 'sp_content';
        } else {
            console.log('invalid element');
        }
        if (menutype === null) {
            return;
        }

        var $menu = $('.contextmenu.contextmenu-' + menutype);
        closeOpenedContextMenus();

        if ($menu.length === 0) {
            console.log('aaaa');
            return;
        }
        $contextTarget = $this;
        //$this.addClass(openedClass);
        //$this.parentsUntil('#site-page-editor-content', '.spe').addClass(openedClass);
        $menu.addClass(openedClass).css('top', e.originalEvent.clientY + 'px').css('left', e.originalEvent.clientX + 'px');
        $('.contextmenu-backdrop').addClass(openedClass);

    }


    /*
    * @param how: 0 - append, 1 - prepend, 2 - before, 3 - after
     */
    function insertElem(type, how) {
        var $elem = $('<div class="' + type + ' spe"></div>');
        switch (how) {
            case 0:
                $contextTarget.append($elem);
                break;
            case 1:
                $contextTarget.prepend($elem);
                break;
            case 2:
                $contextTarget.before($elem);
                break;
            case 3:
                $contextTarget.after($elem);

        }
        attachContextMenu();
    }

    function appendRow() {
        insertElem('row', 0);
        closeOpenedContextMenus();
    }

    function prependRow() {
        insertElem('row', 1);
        closeOpenedContextMenus();
    }

    function insertRowAbove() {
        insertElem('row', 2);
        closeOpenedContextMenus();
    }

    function insertRowBelow() {
        insertElem('row', 3);
        closeOpenedContextMenus();
    }

    function appendCol() {
        insertElem('col', 0);
        closeOpenedContextMenus();
    }

    function prependCol() {
        insertElem('col', 1);
        closeOpenedContextMenus();
    }

    function insertColBefore() {
        insertElem('col', 2);
        closeOpenedContextMenus();
    }

    function insertColAfter() {
        insertElem('col', 3);
        closeOpenedContextMenus();
    }

    function deleteElem() {
        $contextTarget.remove();
        closeOpenedContextMenus();
    }

    function updateFormData() {

        var $structureField = $('#field_structure');
        var $htmlblocksField = $('#field_htmlblocks');

        var $spContentCopy = $('<div>' + $spContent.html() + '</div>');
        var $htmlBlocks = $spContentCopy.find('.spe-html_block');
        var htmlBlocks = [];
        $htmlBlocks.each(function() {
            var $this = $(this);
            htmlBlocks.push({
                id: $this.data('id'),
                content: $this.children('.html-block').html()
            });
            $this.replaceWith('{{{#html_block}}}' + $this.data('blockid') + '{{{/html_block}}}');
        });

        var cleanedStructure = $spContentCopy.html();
        cleanedStructure = cleanHTML(cleanedStructure);
        //cleanedStructure = cleanedStructure.replace(/\s{2,}/gi, ' ');
        //cleanedStructure = cleanedStructure.replace(/\n{2,}/gi, '\n');

        //cleanedStructure = html_beautify.html_beautify(cleanedStructure);
        window.cleanedStructure = cleanedStructure;
        $structureField.val(cleanedStructure).trigger('change');
        $htmlblocksField.val(JSON.stringify(htmlBlocks));
    }

    function updateFormBlockidList() {
        toggleFormBlockidHolder(false);
        cleanFormBlockContent();
        toggleFormInsertButton(false);
        toggleFormNewblockidHolder(false);

        if (insertBlocktype === '-1') {
            return;
        }

        $.ajax({
            url: window.CFG.wwwroot + '/api/admin/site-page-editor/list_blocks.php',
            dataType: 'json',
            data: {
                blocktype: insertBlocktype,
                sessionid: window.CFG.sessionid
            },
            success: function(data) {
                var $blockid = $('#blockid');
                if (data.blocks.length > 0) {
                    var html = '<option value="-1" selected="selected">Please, choose</option>';
                    if (data.supportnewblock) {
                        html += '<option value="0">New block</option>';
                    }
                    for (var i = 0; i < data.blocks.length; i++) {
                        html += '<option value="' + data.blocks[i].id +'">' + data.blocks[i].blockid + ',' + data.blocks[i].lang + '</option>';
                    }

                    $blockid.html(html);
                    toggleFormBlockidHolder(true);
                } else {
                    $blockid.html('<option>comming soon...</option>');
                    toggleFormBlockidHolder(true);
                }
            }
        });
    }

    function updateFormBlockcontent() {
        toggleFormInsertButton(false);
        toggleFormNewblockidHolder(false);
        if (insertBlockid === '0') {
            toggleFormNewblockidHolder(true);
            toggleFormInsertButton(true);
            toggleFormBlockContent(false);
        } else {
            $.ajax({
                url: window.CFG.wwwroot + '/api/admin/site-page-editor/block_content.php',
                dataType: 'text',
                data: {
                    blocktype: insertBlocktype,
                    blockid: insertBlockid,
                    sessionid: window.CFG.sessionid
                },
                success: function(data) {
                    if (data.length > 0) {
                        $formblockcontent.html(data);
                        toggleFormBlockContent(true);
                        toggleFormInsertButton(true);
                    }
                }
            });
        }
    }

    function insertBlockToContext(e) {
        $newblockError.text('');
        if (insertBlockid === '0') {
            $.ajax({
                url: window.CFG.wwwroot + '/api/admin/site-page-editor/new_block.php',
                dataType: 'json',
                data: {
                    blocktype: insertBlocktype,
                    blockid: $('#newblockid').val(),
                    sessionid: window.CFG.sessionid
                },
                success: function(data) {
                    if (data.success) {
                        var $content = $(data.content);
                        $contextTarget.prepend($content);
                        $insertBlockModal.modal('hide');
                        attachContextMenu();
                    } else {
                        $newblockError.html(data.reason + '');

                    }
                }
            });

        } else {
            $.ajax({
                url: window.CFG.wwwroot + '/api/admin/site-page-editor/block_content.php',
                dataType: 'text',
                data: {
                    blocktype: insertBlocktype,
                    blockid: insertBlockid,
                    sessionid: window.CFG.sessionid,
                    foradmin: 1
                },
                success: function(data) {

                    if (data.length > 0) {
                        var $content = $(data);

                        $contextTarget.prepend($content);
                        attachContextMenu();

                    }
                }
            });
            $insertBlockModal.modal('hide');
        }
    }

    function editBlockCKE(e) {
        closeOpenedEditors();

        var $content;
        if ($contextTarget.hasClass('spe') ||
            $contextTarget.hasClass('spe-inline_block')) {
            $content = $contextTarget;
        } else {
            $content = $contextTarget.find('.block-content');
        }
        if (!$content.attr('contenteditable')) {
            $contextTarget.addClass('spe-block-editing');
            $content.attr('contenteditable', true);
            $content.ckeditor({
                inline: true,
                startupFocus: true,
                on: {
                    instanceReady: function() {
                        var focusManager = CKEDITOR.focusManager(this);
                        focusManager.lock();
                    }
                }
            });
            $('.close-editors-btn').show();
        }

        closeOpenedContextMenus();
    }

    function closeOpenedCKE() {
        for (var i in CKEDITOR.instances) {
            if (CKEDITOR.instances.hasOwnProperty(i)) {
                CKEDITOR.instances[i].destroy();
            }
        }
        $('[contenteditable]').removeAttr('contenteditable').removeClass('cke_focus');
        $('.spe-block-editing').removeClass('spe-block-editing');
        $('.close-editors-btn').hide();
    }

    function editBlockACE(e) {
        closeOpenedEditors();

        $contextTarget.addClass('spe-block-editing');

        var $content;
        if ($contextTarget.hasClass('spe') ||
            $contextTarget.hasClass('spe-inline_block')) {
            $content = $contextTarget;
        } else if ($contextTarget.is($spContent)) {
            $content = $contextTarget;
        } else {
            $content = $contextTarget.find('.block-content');
        }

        var html = $content.html();
        html = cleanHTML(html);

        var editor = ace.edit($content[0]);
        editor.setTheme("ace/theme/chrome");
        editor.session.setMode("ace/mode/html");
        editor.setValue(html);
        currentACEeditor = editor;

        $('.close-editors-btn').show();
        closeOpenedContextMenus();
    }

    function closeOpenedACE() {
        if (currentACEeditor !== undefined) {
            var content = currentACEeditor.getValue();
            var $container = $(currentACEeditor.container);
            currentACEeditor.destroy();
            $container.html(content).removeClass('ace_editor').removeClass('ace-chrome');
            currentACEeditor = undefined;
        }
        $('.spe-block-editing').removeClass('spe-block-editing');
        $('.close-editors-btn').hide();
        attachContextMenu();
    }

    function closeOpenedEditors() {
        closeOpenedACE();
        closeOpenedCKE();
        $('.close-editors-btn').hide();
    }

    function cleanHTML(html) {
        html = html.replace(/draggable="false"/gi, '');
        html = html_beautify.html_beautify(html, {
            "indent_size": "4",
            "indent_char": " ",
            "max_preserve_newlines": "-1",
            "preserve_newlines": false,
            "keep_array_indentation": false,
            "break_chained_methods": false,
            "indent_scripts": "normal",
            "brace_style": "collapse",
            "space_before_conditional": true,
            "unescape_strings": false,
            "jslint_happy": false,
            "end_with_newline": false,
            "wrap_line_length": "120",
            "indent_inner_html": false,
            "comma_first": false,
            "e4x": false,
            "indent_empty_lines": false
        });
        //html = html.replace(/\s{2,}/gi, ' ');
        //html = html.replace(/\n{2,}/gi, '\n');
        //html = html_beautify.html_beautify(html);
        return html;
    }

    function deleteBlock(e) {
        deleteElem();
        closeOpenedContextMenus();
    }

    function cleanFormBlockContent() {
        $formblockcontent.html('');
        toggleFormBlockContent(false);
    }

    function toggleFormBlockContent(show) {
        if (show) {
            $('#blockcontent-holder').show();
        } else {
            $('#blockcontent-holder').hide();
        }
    }

    function toggleFormInsertButton(show) {
        if (show) {
            $('#insert-block-to-page-btn').show();
        } else {
            $('#insert-block-to-page-btn').hide();
        }
    }

    function toggleFormBlockidHolder(show) {
        if (show) {
            $('#blockid-holder').show();
        } else {
            $('#blockid-holder').hide();
        }
    }

    function toggleFormNewblockidHolder(show) {
        if (show) {
            $('#newblockid-holder').show();
            $('#newblockid').val($spContent.data('pageid') + '-');
        } else {
            $('#newblockid-holder').hide();
            $('#newblockid').val('');
        }
    }

    function toggleBlockPreview(e) {
        if ($(this).prop('checked')) {
            $spContent.addClass('preview');

        } else {
            $spContent.removeClass('preview');
        }
    }

    function attachContextMenu() {


        // prevent standard behavior of links, buttons..
        $("#site-page-editor-content *").off().click(function(e) {e.preventDefault(); return false})

        var eventName = 'contextmenu.spe';
        $('.spe').off(eventName).on(eventName, openContextMenu);
        //$('.spe-col').off(eventName).on(eventName, openContextMenu);
        $('.spe-html_block').off(eventName).on(eventName, openContextMenu);
        $('.spe-inline_block').off(eventName).on(eventName, openContextMenu);
    }

    function showAttributesModel(e) {
        e.preventDefault();



        $attributesModal.modal('show');

        closeOpenedContextMenus();
    }

    $(document).ready(function() {
        $spContent = $('#site-page-editor-content');
        $formblockcontent = $('#blockcontent');
        $insertBlockModal = $('#site-page-editor-insert-block-modal');
        $attributesModal = $('#site-page-editor-attributes-modal');
        $newblockError = $('.newblock-error');

        $('body > header a').click(function(e) {e.preventDefault();}); //prevent nav links to be clickable
        $('body > div > header a').click(function(e) {e.preventDefault();}); //prevent nav links to be clickable

        $('.prepend-row-btn').click(prependRow);
        $('.append-row-btn').click(appendRow);
        $('.insert-row-above-btn').click(insertRowAbove);
        $('.insert-row-below-btn').click(insertRowBelow);
        $('.append-col-btn').click(appendCol);
        $('.prepend-col-btn').click(prependCol);
        $('.insert-col-before-btn').click(insertColBefore);
        $('.insert-col-after-btn').click(insertColAfter);
        $('.delete-elem-btn').click(deleteElem);
        $('.edit-source-btn').click(editBlockACE);
        $('.edit-wysiwyg-btn').click(editBlockCKE);
        $('.delete-block-btn').click(deleteBlock);
        $('.close-editors-btn').click(closeOpenedEditors);

        $('.show-form-btn').click(function() {
            closeOpenedEditors();
            updateFormData();
        });


        $('.save-form-btn').click(function() {
            closeOpenedEditors();
            updateFormData();
            $('#__formapply__subm_btn').click();
        });

        $('.saveandgoback-form-btn').click(function() {
            closeOpenedEditors();
            updateFormData();
            $('#__formsubmit__subm_btn').click();
        });

        $('.cancel-form-btn').click(function() {
            $('#__formcancel__subm_btn').click();
        });


        $('.prepend-row-root-btn').click(function() {
            $contextTarget = $spContent;
            prependRow();
        });

        $('.append-row-root-btn').click(function() {
            $contextTarget = $spContent;
            appendRow();
        });

        $('.insert-block-btn').click(function() {
            $('#blocktype').val('-1');
            toggleFormBlockidHolder(false);
            toggleFormBlockContent(false);
            toggleFormInsertButton(false);
            toggleFormNewblockidHolder(false);
            closeOpenedContextMenus();
        });

        $('#insert-block-to-page-btn').click(insertBlockToContext);


        $('#blocktype').change(function(e) {
            insertBlocktype = $(this).val();
            updateFormBlockidList();
        });

        $('#blockid').change(function(e) {
            insertBlockid = $(this).val();
            updateFormBlockcontent();
        });


        $('.contextmenu-backdrop').click(closeOpenedContextMenus).contextmenu(closeOpenedContextMenus);

        $('.toggle-block-preview').change(toggleBlockPreview);

        $('.edit-attributes-btn').click(showAttributesModel);


        attachContextMenu();

        $spContent.contextmenu(openContextMenu);

        $(window).bind('keydown', function(event) {
            if (event.ctrlKey || event.metaKey) {
                if (String.fromCharCode(event.which).toLowerCase() === 's') {
                    event.preventDefault();

                    closeOpenedEditors();
                    updateFormData();
                    $('#__formapply__subm_btn').click();
                }
            }
        });



    });
    return true;

});
