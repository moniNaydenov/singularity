define('admin/table_editor', ['jquery'], function($) {

    var TableEditor = function() {
        var instance = this;
        this.shiftPressed = false;
        this.showCheckboxes = false;
        this.$lastChangedCheckbox = null;
        $(document).ready(function() {
            instance.init();
        });
    };

    TableEditor.prototype.toggleCheckboxesUntil = function($to) {
        if (!this.$lastChangedCheckbox !== null && this.shiftPressed) {
            var $from = this.$lastChangedCheckbox;
            var $trFrom = $from.parentsUntil('tbody', 'tr');
            var $trTo = $to.parentsUntil('tbody', 'tr');
            var from = this.$rows.index($trFrom);
            var to = this.$rows.index($trTo);
            var $checkbox;
            if (from < to) {
                for (var i = from + 1; i < to; i++) {
                    $checkbox = $(this.$checkboxes.get(i));
                    $checkbox.prop('checked', !$checkbox.prop('checked'));
                }
            } else if (from > to) {
                for (var i = from - 1; i > to; i--) {
                    $checkbox = $(this.$checkboxes.get(i));
                    $checkbox.prop('checked', !$checkbox.prop('checked'));
                }
            }
        }
        this.$lastChangedCheckbox = $to;
    };


    TableEditor.prototype.toggleCheckboxesAll = function(checked) {
        this.$checkboxes.prop('checked', checked);
    };



    TableEditor.prototype.init = function() {
        var instance = this;

        this.$table = $('table.admin-table-editor');

        this.$checkboxes = this.$table.find('tbody tr td input[type="checkbox"]');
        this.showCheckboxes = this.$checkboxes.length > 0;

        this.$rows = this.$table.find('tbody tr');


        $('#deletequestionmodal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            $(this).find('.confirm-delete-button').data('href', button.data('href'));
        });

        $('.confirm-delete-button').click(function(e) {
            var $this = $(this);
            window.location = $this.data('href');
        });


        $(window).bind('keydown', function(event) {
            if (event.key === 'Shift') {
                instance.shiftPressed = true;
            }
        });

        $(window).bind('keyup', function(event) {
            if (event.key === 'Shift') {
                instance.shiftPressed = false;
            }
        });

        if (this.showCheckboxes) {
            this.$rows.click(function(e) {
                var $target = $(e.target);
                if (!$target.is('a.btn') && !$target.is('input[type="checkbox"]')) {
                    var $checkbox = $(this).find('input[type="checkbox"]');
                    var checked = !$checkbox.prop('checked');
                    $checkbox.prop('checked', checked);
                    instance.toggleCheckboxesUntil($checkbox);
                }
            });

            this.$checkboxes.on('change', function(e) {
                var $checkbox = $(this);
                instance.toggleCheckboxesUntil($checkbox);
            });

            $('.checkbox-selectall').click(function(e) { e.preventDefault(); instance.toggleCheckboxesAll(true); });
            $('.checkbox-selectnone').click(function(e) { e.preventDefault(); instance.toggleCheckboxesAll(false); });
        }

        this.$confirmmodal = $('#confirmmodal');
        this.$confirmcaller = null;
        $('[data-confirm]').click(function(e) {
            if (instance.$confirmcaller !== null) {
                instance.$confirmcaller = null;
                return;
            }
            e.preventDefault();

            instance.$confirmcaller = $(this);
            instance.$confirmmodal.modal();
            instance.$confirmmodal.find('.confirm-button').off().click(function(ee) {
                ee.preventDefault();
                instance.$confirmcaller[0].click();
            });
        });


    };

    return TableEditor;
});