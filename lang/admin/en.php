<?php

/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('INTERNAL') || die;

$string['home:welcome'] = 'Administration page';
$string['noredirect'] = 'Save without redirecting';
$string['actions'] = 'Actions';
$string['lang'] = 'Language';
$string['lastmodified'] = 'Last modified';
$string['customaction'] = 'Custom action';
$string['areyousureyouwanttocontinue'] = 'Are you sure you want to continue?';
$string['deleteareyousure'] = 'Are you sure you want to delete the selected record?';
$string['recorddeleteok'] = 'Record was successfully deleted!';
$string['recorddeletefail'] = 'An error occurred while attempting to delete record!';
$string['nocustomaction'] = 'No custom action defined!';
$string['editthisblock'] = 'Click to edit this block';
$string['showform'] = 'Show form';
$string['sitepageeditor'] = 'Site page editor';
$string['addrow'] = 'Add row';
$string['prependrow'] = 'Prepend row';
$string['appendrow'] = 'Append row';
$string['deleterow'] = 'Delete row';
$string['prependcol'] = 'Prepend column';
$string['deletecol'] = 'Delete column';
$string['appendcol'] = 'Append column';
$string['insertrowabove'] = 'Insert row above';
$string['insertrowbelow'] = 'Insert row below';
$string['insertcolbefore'] = 'Insert column before';
$string['insertcolafter'] = 'Insert column after';
$string['sortorder'] = 'Order';
$string['newdirname'] = 'New directory name';
$string['current_dir'] = 'Current directory';
$string['createnewdir'] = 'New directory';
$string['upload'] = 'Upload';
$string['admin_manage_files'] = 'Manage files';
$string['password_changed_fail'] = 'Password failed to change!';
$string['password_changed_ok'] = 'Password was successfully changed!';
$string['fileuploadok'] = 'File %s uploaded successfully';
$string['dircreatedok'] = 'Directory %s created successfully';
$string['addhtmlblock'] = 'Add a html block';
$string['insertblock'] = 'Insert block';
$string['blocktype'] = 'Block type';
$string['editraw'] = 'Edit raw';
$string['editinline'] = 'Edit inline';
$string['edit:source'] = 'Edit source code';
$string['edit:html'] = 'Edit live';
$string['saveandback'] = 'Save and go back';
$string['discardchanges'] = 'Discard changes';
$string['choosefromlist'] = 'Choose from list';
$string['cannotberestored'] = 'Warning! Deleted items cannot be restored!';
$string['deletefile'] = 'Delete file/directory';
$string['renamefile'] = 'Rename file/directory';
$string['originalname'] = 'Original name';
$string['newname'] = 'New name';
$string['filerenameok'] = 'File/directory <strong>%s</strong> successfully renamed to <strong>%s</strong>!';
$string['filerenamefail'] = 'WARNING! An error occurred while renaming file/directory <strong>%s</strong> to <strong>%s</strong>!';
$string['filedeleteok'] = 'File/directory <strong>%s</strong> successfully deleted!';
$string['filedeletefail'] = 'WARNING! An error occurred while deleting file/directory <strong>%s</strong>!';
$string['pagetype'] = 'Page type';
$string['showblocks'] = 'Show blocks';
$string['toggleblocks'] = 'Toggle blocks';
$string['editwysiwyg'] = 'Edit Visual';
$string['editsource'] = 'Edit source code';
$string['editattributes'] = 'Edit attributes';
$string['deleteblock'] = 'Delete block';
$string['closeeditors'] = 'Close editor';
$string['viewpage'] = 'View page';

$string['shortname'] = 'Short name';
$string['editnews'] = 'Edit news';

$string['notsupported'] = 'Not supported';

$string['moveup'] = 'Move up';
$string['movedown'] = 'Move down';

$string['block:html_block'] = 'HTML block';
$string['block:inline_block'] = 'Inline block';
$string['block:custom_block'] = 'Custom block';
$string['block:nav_block'] = 'Navigation block';
$string['block:gallery_block'] = 'Gallery block';

$string['log:view'] = 'View log';

$string['admin:field:action'] = 'Action';
$string['admin:field:affected_table'] = 'Affected table';
$string['admin:field:additional_info'] = 'Additional info';
$string['admin:field:ip'] = 'IP address';
$string['admin:field:userid'] = 'User id';
$string['admin:field:timestamp'] = 'Timestamp';
$string['log:severity:ok'] = 'OK';
$string['log:severity:warning'] = 'WARNING';
$string['log:severity:danger'] = 'DANGER';
$string['log:severity:fail'] = 'FAIL';
$string['timestamp'] = 'Timestamp';

$string['news:editshort'] = 'Edit short text';
$string['news:editfull'] = 'Edit full page';


$string['navitemid'] = 'Navigation item';
$string['fixedwidth:center'] = 'Fixed width (centered)';
$string['fixedwidth:left'] = 'Fixed width (left)';
$string['fixedwidth:right'] = 'Fixed width (right)';
$string['fullwidth'] = 'Full width (100%)';

$string['seourl'] = 'SEO url';
$string['imagealt'] = 'Image alt';

$string['metatag:description'] = '"Description" meta tag';


$string['target:self'] = 'Self (default)';
$string['target:blank'] = 'Blank';

$string['upgrade:success'] = 'Singularity successfully upgraded to version %s!';
$string['upgrade:site:success'] = 'Site <strong>%s</strong> successfully upgraded to version %s!';
$string['upgrade:noupgradepending'] = 'No upgrade pending!';


$string['generalsettings'] = 'General settings';

$string['cookieconsent:content'] = 'Cookie consent - text to display';
$string['cookieconsent:show'] = 'Display a "Cookie Consent" message';

$string['settings:update:ok'] = 'Settings were upated successfully!';

$string['showtitle'] = 'Show title on page';

$string['performanceinfo'] = 'Performance info';
$string['peakmemory'] = 'Peak memory usage';
$string['totalexectime'] = 'Total execution time';
$string['performanceinfo:show'] = 'Display performance info';

$string['christmas:from'] = 'Christmas mode starts';
$string['christmas:to'] = 'Christmas mode ends';


$string['settings:general'] = 'General';
$string['settings:admin'] = 'Admin';

$string['missing:sitenamestring'] = 'Warning! String <strong>sitename</strong> was not found! Please, add it to the language strings file!';

$string['site_install_ok'] = 'DB structure was successfully installed!!';

$string['gallery:galleries'] = 'Galleries';
$string['gallery:manage'] = 'Manage galleries';
$string['gallery:edit'] = 'Edit gallery';
$string['gallery:edititems'] = 'Edit items';
$string['gallery:edititem'] = 'Edit item';
$string['gallery:shortname'] = 'Short name (a-z1-9)';
$string['gallery:type'] = 'Gallery type';
$string['gallery:type:grid'] = 'Grid';
$string['gallery:type:list_horizontal'] = 'Horizontal list';
$string['gallery:type:click_carousel'] = 'Clickable carousel';