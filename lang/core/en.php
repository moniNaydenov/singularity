<?php

/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('INTERNAL') || die;

$string['welcome'] = 'Welcome to Singularity';

$string['builtonsingularity'] = 'Built on <a href="https://bitbucket.org/moniNaydenov/singularity" target="_blank">singularity</a>';


$string['admin_nav_items_title'] = 'Manage navigation';
$string['title'] = 'Title';
$string['language'] = 'Language';
$string['url'] = 'URL';
$string['site'] = 'Site';
$string['order'] = 'Order';
$string['sortorder'] = 'Order';
$string['save'] = 'Save';
$string['apply'] = 'Apply';
$string['pageid'] = 'Page identifier';
$string['back'] = 'Back';
$string['structure'] = 'Structure';
$string['insert'] = 'Insert';
$string['addnew'] = 'Add new';
$string['edit'] = 'Edit';
$string['id'] = 'ID';
$string['site'] = 'Site';
$string['pleasechoose'] = 'Please choose';
$string['rename'] = 'Rename';
$string['root'] = 'Root';

$string['none'] = 'None';
$string['delete'] = 'Delete';
$string['visible'] = 'Visible';
$string['hidden'] = 'Hidden';
$string['password'] = 'Password';
$string['oldpassword'] = 'Old password';
$string['password_retype'] = 'Password (re-type)';
$string['changepassword'] = 'Change your password';
$string['change'] = 'Change';
$string['cancel'] = 'Cancel';
$string['home'] = 'Home';
$string['content'] = 'Content';
$string['pages'] = 'Pages';
$string['sitepages'] = 'Site pages';
$string['htmlblocks'] = 'HTML blocks';
$string['managefiles'] = 'Manage files';
$string['filebrowser'] = 'File browser';
$string['navitems'] = 'Manage navigation';
$string['sitehome'] = 'Site home';
$string['adminhome'] = 'Admin home';
$string['send'] = 'Send';
$string['purgecaches'] = 'Purge caches';
$string['purgecaches:updaterevonly'] = 'Purge caches (rev only)';
$string['contactus'] = 'Contact us';
$string['topic'] = 'Topic';
$string['sending'] = 'Sending';
$string['message'] = 'Message';
$string['parent'] = 'Parent';
$string['link'] = 'Link';

$string['blockid'] = 'Block identifier';
$string['close'] = 'Close';
$string['levelup'] = 'Level up';
$string['create'] = 'Create';
$string['empty'] = 'Empty';
$string['name'] = 'Name';
$string['description'] = 'Description';
$string['youtube'] = 'YouTube';

$string['standard'] = 'Standard';
$string['partial'] = 'Partial';

$string['news'] = 'News';


$string['log'] = 'Log';

$string['image'] = 'Image';



$string['january'] = 'January';
$string['february'] = 'February';
$string['march'] = 'March';
$string['april'] = 'April';
$string['may'] = 'May';
$string['june'] = 'June';
$string['july'] = 'July';
$string['august'] = 'August';
$string['september'] = 'September';
$string['october'] = 'October';
$string['november'] = 'November';
$string['december'] = 'December';

$string['continue'] = 'Continue';
$string['pleasewait'] = 'Please, wait';
$string['choose'] = 'Choose';
$string['clear'] = 'Clear';


$string['date'] = 'Date';
$string['summary'] = 'Summary';
$string['read'] = 'Read';
$string['readmore'] = 'Read more';

$string['all'] = 'All';

$string['yes'] = 'Yes';
$string['no'] = 'No';

$string['username'] = 'Username';
$string['password'] = 'Password';
$string['signin'] = 'Sign in';
$string['authenticationrequired'] = 'Authentication required';
$string['wrongcredentials'] = 'Wrong credentials!';

$string['viewmore'] = 'View more';
$string['target'] = 'Target';

$string['confirm'] = 'Confirm';

$string['cookieconsentok'] = 'I understand!';

$string['duration'] = 'Duration';
$string['download'] = 'Download';
$string['comment'] = 'Comment';

$string['selectall'] = 'Select all';
$string['selectnone'] = 'Select none';
$string['selectallnone'] = 'Select all/none';

$string['from'] = 'from';
$string['to'] = 'to';

$string['search:result'] = 'Search results';
$string['search:empty'] = 'Nothing found.';

$string['newer'] = 'Newer';
$string['older'] = 'Older';

$string['gallery:link'] = 'Link (leave empty for no click';