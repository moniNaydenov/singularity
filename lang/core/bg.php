<?php

/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('INTERNAL') || die;

$string['welcome'] = 'Добре дошли в Singularity';

$string['title'] = 'Заглавие';
$string['language'] = 'Език';
$string['url'] = 'URL адрес';
$string['order'] = 'Подредба';
$string['sortorder'] = 'Подредба';
$string['save'] = 'Запази';
$string['back'] = 'Обратно';
$string['home'] = 'Начало';
$string['contactus'] = 'Контакти';
$string['send'] = 'Изпрати';
$string['sending'] = 'Изпращане';
$string['cancel'] = 'Откажи';
$string['close'] = 'Затвори';

$string['topic'] = 'Тема';
$string['message'] = 'Съобщение';
$string['news'] = 'Новини';


$string['pleasechoose'] = 'Моля, изберете';




$string['january'] = 'Януари';
$string['february'] = 'Февруари';
$string['march'] = 'Март';
$string['april'] = 'Април';
$string['may'] = 'Май';
$string['june'] = 'Юни';
$string['july'] = 'Юли';
$string['august'] = 'Август';
$string['september'] = 'Септември';
$string['october'] = 'Октомври';
$string['november'] = 'Ноември';
$string['december'] = 'Декември';

$string['password'] = 'Парола';

$string['continue'] = 'Продължи';
$string['clear'] = 'Изчисти';

$string['pleasewait'] = 'Моля, изчакайте';
$string['choose'] = 'Избери';
$string['read'] = 'Прочети';
$string['readmore'] = 'Прочети повече';


$string['all'] = 'Всички';
$string['yes'] = 'Да';
$string['no'] = 'Не';


$string['username'] = 'Потребителско име';
$string['password'] = 'Парола';
$string['signin'] = 'Влез';
$string['authenticationrequired'] = 'Необходима идентификация';
$string['wrongcredentials'] = 'Грешни данни за идентификация!';


$string['viewmore'] = 'Виж повече';


$string['confirm'] = 'Потвърди';

$string['cookieconsentok'] = 'Разбрах!';


$string['from'] = 'от';
$string['to'] = 'до';


$string['search:result'] = 'Търсене';
$string['search:empty'] = 'Няма намерени резултати';

$string['newer'] = 'По-нови';
$string['older'] = 'По-стари';