<?php

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../config.php');

$cookieconsent = required_param('consent', PARAM_BOOL);

require_sessionid();

if ($cookieconsent) {
    set_cookie(CONSENTCOOKIE, 'agree', 365 * 24 * 60 * 60 + time(), '/');
    echo 'success';
} else {
    remove_cookie(CONSENTCOOKIE);
}