<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../../config.php');

$blocktype = required_param('blocktype', PARAM_BLOCKTYPE);
$blockid = required_param('blockid', PARAM_TEXT);
$foradmin = optional_param('foradmin', false, PARAM_BOOL);


require_login();
require_permission('core/admin:manage');

if ($blocktype) {

    $classname = '\\core\\block\\' . $blocktype;

    if (class_exists($classname)) {
        $block = $classname::create_from_id($blockid);
        if ($block) {
            $pagetype = $foradmin ? 'admin' : '';

            echo $PAGE->get_output($pagetype,true)->render_block($block);
        }
    }
}


