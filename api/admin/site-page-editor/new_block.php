<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../../config.php');

$blocktype = required_param('blocktype', PARAM_BLOCKTYPE);
$blockid = required_param('blockid', PARAM_TEXT);



require_login();
require_permission('core/admin:manage');

require_sessionid();

$return = new stdClass;
$return->success = false;
$return->reason = 'Block type not found!';
$return->content = '';

if ($blocktype) {

    $classname = '\\core\\block\\' . $blocktype;


    if (class_exists($classname)) {
        $block = $classname::create_from_blockid_and_lang($blockid, $CFG->lang);
        if ($block) {
            $return->reason = 'Block type ' . $blocktype . ' with id ' . $blockid . ' already exists!';
        } else {
            $block = $classname::create_new($blockid, $CFG->lang);
            if ($block) {
                $return->success = true;
                $return->reason = '';
                $return->content = $PAGE->get_output('admin',true)->render_block($block);
            } else {
                $return->reason = 'An error occured while creating new block. Block name empty?';
            }
        }
    }
}


echo json_encode($return, JSON_UNESCAPED_UNICODE);