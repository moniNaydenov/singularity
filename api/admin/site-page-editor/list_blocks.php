<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

define('AJAX_SCRIPT', true);

require_once(__DIR__ . '/../../../config.php');

$blocktype = required_param('blocktype', PARAM_BLOCKTYPE);

require_login();
require_permission('core/admin:manage');

//require_sessionid();

$return = new stdClass;
$return->blocks = array();
$return->supportnewblock = false;

if ($blocktype) {

    $classname = '\\core\\block\\' . $blocktype;

    if (class_exists($classname)) {
        $blocks = $classname::get_all_instances($CFG->lang);

        foreach ($blocks as $block) {
            $context = $block->export_for_api();
            $return->blocks[] = $context;
        }

        $return->supportnewblock = $classname::support_new_block();

    }

}

echo json_encode($return);

