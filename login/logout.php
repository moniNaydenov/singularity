<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 4:00 PM
 */

require_once(__DIR__ . '/../config.php');

require_login();
require_permission('core/admin:manage');

require_sessionid();

$userhandler = \core\userhandler::instance_from_USER();
$userhandler->logout();

header('location: ' . $CFG->wwwroot . '/index.php');
die;