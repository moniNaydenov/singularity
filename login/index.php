<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 2:24 PM
 */

require_once(__DIR__ . '/../config.php');



class login_form extends \core\form {
    protected function def() {
        $this->add_field(new \core\form_field\text('username', '', get_string('username'), PARAM_RAW));
        $this->add_field(new \core\form_field\password('password', '', get_string('password')));
        $this->add_submit_buttons(get_string('signin'), null, null);
    }
}

$loginform = new login_form();


if ($data = $loginform->get_data()) {

    if (!is_null($data->username) && !is_null($data->password)) {
        if (!\core\userhandler::try_login($data->username, $data->password)) {
            $error = 1;
        }
    }
}


if(isloggedin()) {
    $wanturl = $CFG->wwwroot . '/index.php';
    if (!empty($_SESSION['wanturl'])) {
        $wanturl = $_SESSION['wanturl'];
        unset($_SESSION['wanturl']);
    }
    header('location: ' . $wanturl);
    die;
}


$PAGE->set_title(get_string('authenticationrequired'));

$output = $PAGE->get_output();
$output->set_main_additional_classes('mx-auto container pt-5 mt-5');

echo $output->header(false);

echo '<h3>' . $PAGE->get_title() . '</h3>';

if (isset($error)) {
    //print security::encrypt_password($password);
    echo '<p class="text-danger my-3">' . get_string('wrongcredentials') .'</p>';
}

echo $loginform->render($output);

echo $output->footer(false);