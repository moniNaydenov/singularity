<?php

require_once(__DIR__ . '/config.php');

$id = optional_param('id', false, PARAM_INT);

$output = $PAGE->get_output();

$newstitle = get_string('news');

$news = null;
if ($id) {
    $news = $DB->get_record('news', array('id' => $id));
    if (!$news) {
        redirect('/');
        die;
    }
} /*else {
    $news = $DB->get_record('news', array(), '*', 'timestamp DESC');
}
if (count($news) == 0) {
    redirect(new surl('/'));
    die;
}
*/

if ($news) {
    $sp = \core\site_page::create_from_id($news->sitepageid);

    $nextnews = array();
    foreach (array('>=' => 'ASC', '<=' => 'DESC') as $sign => $dir) {
        $nextsql = '
    SELECT
        n.*,
        sp.title
    FROM
        news n
    JOIN
        site_page sp ON sp.id = n.sitepageid
    WHERE
        n.timestamp ' . $sign . ' :timestamp AND
        n.id <> :id
    ORDER BY
        n.timestamp ' . $dir . '
    LIMIT 0,1';
        $nextnews[] = $DB->get_record_sql($nextsql, array('timestamp' => $news->timestamp, 'id' => $news->id));
    }
    list($prev, $next) = $nextnews;



    if (!$sp) {
        redirect(new surl('/'));
        die;
    }

    $sp->set_page_metatags();

    $PAGE->set_url(new surl('/news.php', array('id' => $id)));

    $PAGE->set_title($sp->get_title());

    $PAGE->add_breadcrumb($newstitle, $PAGE->get_url());

} else {
    $PAGE->set_title($newstitle);
    $PAGE->set_heading($newstitle);
}

echo $output->header();


$context = new stdClass;
if ($news) {
    $context->hasnews = true;
    $context->sitepage = $output->render_site_page($sp);
    $context->prev = $prev;
    $context->next = $next;
    $context->pagetype_class = $sp->get_pagetype_cssclass();
} else {
    $context->hasnews = false;
}

echo $output->render_news_page($context);

echo $output->footer();