<?php
/**
 * Created by PhpStorm.
 * User: stoen
 * Date: 3/10/17
 * Time: 11:40 PM
 */

defined('INTERNAL') || die;




class edit_form extends \core\form {
    public function def() {
        global $CFG;
        $this->add_field(new \core\form_field\hidden('id', -1, PARAM_INT));
        if ($this->options['new']) {
            $this->add_field(new \core\form_field\text('blockid', '', get_string('blockid'), PARAM_TEXT));
        } else {
            $this->add_field(new \core\form_field\textlabel('blockid', '', get_string('blockid')));
        }
        $this->add_field(new \core\form_field\lang('lang', $CFG->lang, get_string('language')));
        if (isset($this->options['source']) && $this->options['source']) {
            $this->add_field(new \core\form_field\codeeditor('content', '', get_string('content'), 'html'));
        } else {
            $this->add_field(new \core\form_field\htmleditor('content', '', get_string('content')));
        }
        $this->add_field(new \core\form_field\codeeditor('css', '', 'CSS', 'css'));

        $this->add_submit_buttons(get_string('saveandback', 'admin'), get_string('back'), get_string('save'));
    }
}