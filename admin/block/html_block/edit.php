<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../../config.php');
require_once(__DIR__ . '/edit_form.php');

$id = optional_param('id', false, PARAM_INT);
$source = optional_param('source', false, PARAM_BOOL);

$PAGE->set_url('/admin/block/html_block/edit.php');
$PAGE->set_title(get_string('htmlblocks'));
$PAGE->set_pagetype('admin');


require_login();
require_permission('core/admin:manage');

$output = $PAGE->get_output();

$spform = new edit_form(array('source' => $source, 'new' => ($id == -1)));

$redirectmessage = false;
$noredirect = false;

if ($spform->is_cancelled()) {
    redirect(new surl('/admin/block/html_block/index.php'));
    die;
} else if ($data = $spform->get_data()) {
    if ($data->id == -1) {
        $record = new stdClass();
        $record->id = -1;
    } else {
        $record = $DB->get_record('html_block', array('id' => $data->id));
    }
    if ($record != false) {
        $record->lang = $data->lang;
        $record->content = $data->content;
        $record->css = $data->css;
        $record->modifiedby = $USER->id;
        $record->lastmodified = time();
        if ($record->id == -1) {
            unset($record->id);
            $record->blockid = $data->blockid;
            $record->id = $DB->insert_record('html_block', $record);
            $redirectmessage = 'New record inserted successfully!';
        } else {
            $DB->update_record('html_block', $record);
            $redirectmessage = 'Record ' . $id . ' updated successfully!';
        }
        if ($spform->is_applied()) {
            $noredirect = true;
            $id = $record->id; 
        }
    } else {
        $redirectmessage = 'Record with id ' . $id . ' not found!';
    }
} else if ($id) {
    if ($id != -1) {
        $record = $DB->get_record('html_block', array('id' => $id));
        if ($record == false) {
            $redirectmessage = 'Record with id ' . $id . ' not found!';
        } else {
            $spform->set_values($record);
        }
    }
} else {
    $redirectmessage = 'Parameter {id} missing!';
}

if ($redirectmessage !== false) {
    if ($noredirect == true) {
        redirect(new surl('/admin/block/html_block/edit.php', array('id' => $id, 'source' => strval($source))), $redirectmessage);
        die;
    } else {
        redirect(new surl('/admin/block/html_block/index.php'), $redirectmessage);
        die;
    }
}

echo $output->header();

echo $spform->render();

echo $output->footer();