<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../../config.php');
//require_once(__DIR__ . '/html_block_form.php');

$id = optional_param('id', false, PARAM_INT);
$delete = optional_param('delete', false, PARAM_BOOL);


$PAGE->set_url('/admin/block/html_block/index.php');
$PAGE->set_title(get_string('htmlblocks'));
$PAGE->set_pagetype('admin');


require_login();
require_permission('core/admin:manage');

$tableeditor = new \core\admin\table_editor('html_block',
    array('id', 'blockid', 'lang', 'lastmodified'));

$tableeditor->add_row_action('editsource', get_string('edit:source', 'admin'));
$tableeditor->set_editbutton(true, get_string('edit:html', 'admin'));

$action = $tableeditor->get_row_action();

$output = $PAGE->get_output();

if ($id != false) {
    if ($id != false) {
        if ($delete) {
            if($tableeditor->delete_record($id)) {
                redirect($PAGE->get_url(), get_string('recorddeleteok', 'admin'));
                die;
            } else {
                redirect($PAGE->get_url(), get_string('recorddeletefail', 'admin'));
                die;
            }

        } else if ($action == 'editsource') {
            $url = new surl('/admin/block/html_block/edit.php', array('id' => $id, 'source' => 1));
            $url->add_sessionid();
            redirect($url);
            die;
        } else {
            $url = new surl('/admin/block/html_block/edit.php', array('id' => $id));
            $url->add_sessionid();
            redirect($url);
            die;
        }
    }
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer();