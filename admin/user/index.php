<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 3/7/15
 * Time: 6:15 PM
 */

require_once(__DIR__ . '/../../config.php');



$PAGE->set_url('/admin/user/index.php');

$PAGE->set_title(get_string('changepassword'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');


class change_password_form extends \core\form {
    public function def() {
        $this->add_field(new \core\form_field\text('oldpass', '', get_string('password'), PARAM_PASSWORD));
        $this->add_submit_buttons(get_string('change'), get_string('cancel'));
    }
}

$message = '';
$form = new change_password_form();

if ($result = $form->get_data()) {
    if ($form->is_cancelled()) {
        redirect(new \surl('/admin'));
    } else {
        $message = security::encrypt_password($result->oldpass);
    }
}


$output = $PAGE->get_output();

echo $output->header();


$content = $form->render();
if ($message != '') {
    $content .= html_writer::messagebox($message, 'success');
}

echo $content;

echo $output->footer(false);
