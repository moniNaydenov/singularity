<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../config.php');

$id = optional_param('id', false, PARAM_INT);
$delete = optional_param('delete', false, PARAM_BOOL);

$PAGE->set_url('/admin/site_pages.php');
$PAGE->set_title(get_string('pages'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$tableeditor = new \core\admin\table_editor('site_page',
    array('id', 'pageid', 'lang', 'title', 'lastmodified'), 'id, title ASC');
$tableeditor->add_row_action('editinline', get_string('editinline', 'admin'));
$tableeditor->add_row_action('viewpage', get_string('viewpage', 'admin'), '_blank');

$action = $tableeditor->get_row_action();

$output = $PAGE->get_output();

if ($id != false) {
    if ($delete) {
        if($tableeditor->delete_record($id)) {
            redirect($PAGE->get_url(), get_string('recorddeleteok', 'admin'));
            die;
        } else {
            redirect($PAGE->get_url(), get_string('recorddeletefail', 'admin'));
            die;
        }

    } else if ($action == 'editinline') {
        $url = new surl('/admin/site_page_editor.php', array('id' => $id));
        $url->add_sessionid();
        redirect($url);
        die;
    } else if ($action == 'viewpage') {
        $sp = \core\site_page::create_from_id($id);
        if ($sp) {
            $url = new surl('/page/' . $sp->get_pageid());
            redirect($url);
            die;
        }
    } else {
        $url = new surl('/admin/site_page.php', array('id' => $id));
        $url->add_sessionid();
        redirect($url);
        die;
    }
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer();