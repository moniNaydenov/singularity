<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../config.php');

$PAGE->set_url('/admin/log.php');

$PAGE->set_title(get_string('log'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$tableviewer = new \core\admin\table_viewer('log', array('action', 'affected_table', 'affected_object', 'additional_info', 'ip', 'userid', 'timestamp'), 'timestamp DESC', 20);
$tableviewer->set_datefields(array('timestamp' => '%Y-%m-%d %H:%M:%S'));


$output = $PAGE->get_output();

echo $output->header();

echo $output->render_table_viewer($tableviewer);

echo $output->footer(false);