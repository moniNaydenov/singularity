<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/site_page_form.php');

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_RAW);

$PAGE->set_url(new surl('/admin/site_page_editor.php', array(
    'id' => $id,
    'returnurl' => $returnurl
    )
));
$PAGE->set_title(get_string('sitepages'));
$PAGE->set_pagetype('standard');
$PAGE->add_js('admin/site-page-editor');

require_login();
require_permission('core/admin:manage');

$sitepage = $DB->get_record('site_page', array('id' => $id), '*', null, \core\db\MUST_EXIST);

$sp = new \core\site_page($sitepage);

$spform = new  site_page_form();
$spform->set_values($sitepage);

$output = $PAGE->get_output();
$adminoutput = $PAGE->get_output('admin', true);

$redirectmessage = false;
$noredirect = false;

if ($spform->is_cancelled()) {
    if ($returnurl) {
        $returnurl = rawurldecode($returnurl);
        redirect(new surl($returnurl));
    } else {
        redirect(new surl('/admin/site_pages.php'));
    }
    die;
} else if ($data = $spform->get_data()) {
    if ($data->id == -1) {
        $record = new stdClass();
        $record->id = -1;
    } else {
        $record = $DB->get_record('site_page', array('id' => $data->id));
    }
    if ($record != false) {
        $record->title = $data->title;
        $record->showtitle = $data->showtitle;
        $record->lang = $data->lang;
        $record->pageid = $data->pageid;
        $record->pagetype = $data->pagetype;
        $record->structure = $data->structure;
        $record->css = $data->css;
        $record->navitemid = $data->navitemid;
        $record->modifiedby = $USER->id;
        $record->lastmodified = time();
        if ($record->id == -1) {
            unset($record->id);
            $record->id = $DB->insert_record('site_page', $record);
            $redirectmessage = 'New record inserted successfully!';
        } else {
            $DB->update_record('site_page', $record);
            $redirectmessage = 'Record ' . $id . ' updated successfully!';
        }
        if (!empty($data->htmlblocks)) {
            $htmlblocks = json_decode($data->htmlblocks, false, 512, JSON_UNESCAPED_UNICODE);
            foreach ($htmlblocks as $htmlblock) {
                $block = $DB->get_record('html_block', array('id' => $htmlblock->id));
                if ($block) {
                    $block->content = $htmlblock->content;
                    $DB->update_record('html_block', $block);
                }
            }
        }
        // search indexing...
        $sp->index_for_search($output);

        if ($spform->is_applied()) {
            $noredirect = true;
            $id = $record->id;
        }
    } else {
        $redirectmessage = 'Record with id ' . $id . ' not found!';
    }
} else if ($id) {
    if ($id != -1) {
        $record = $DB->get_record('site_page', array('id' => $id));
        if ($record == false) {
            $redirectmessage = 'Record with id ' . $id . ' not found!';
        } else {
            $spform->set_values($record);
        }
    }
} else {
    $redirectmessage = 'Parameter {id} missing!';
}

if ($redirectmessage !== false) {
    if ($noredirect == true) {
        redirect($PAGE->get_url(), $redirectmessage);
        die;
    } else {
        if ($returnurl) {
            $returnurl = rawurldecode($returnurl);
            redirect(new surl($returnurl), $redirectmessage, 5);
        } else {
            redirect(new surl('/admin/site_pages.php'), $redirectmessage, 5);
        }
        die;
    }
}

echo $output->header();

$form = $spform->render();

echo $adminoutput->render_site_page_editor($sp, $form);

echo $output->footer(false);