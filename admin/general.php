<?php
require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/general_form.php');

$PAGE->set_url('/admin/general.php');

$PAGE->set_title(get_string('generalsettings', 'admin'));

$PAGE->set_pagetype('admin');


require_login();
require_permission('core/admin:manage');

$output = $PAGE->get_output();

$gsform = new general_settings_form();


if ($data = $gsform->get_data()) {
    set_config('cookieconsentshow', $data->cookieconsentshow);
    set_config('cookieconsetcontent', $data->cookieconsetcontent);
    set_config('performanceinfoshow', $data->performanceinfoshow);
    set_config('debug', $data->debug);
    set_config('christmas_from', $data->christmas_from);
    set_config('christmas_to', $data->christmas_to);
    redirect($PAGE->url, get_string('settings:update:ok', 'admin'));
    die;
} else {
    $data = new stdClass;
    $data->cookieconsentshow = get_config('cookieconsentshow', false);
    $data->cookieconsetcontent = get_config('cookieconsetcontent', '');
    $data->performanceinfoshow = get_config('performanceinfoshow', false);
    $data->debug = get_config('debug', false);
    $data->christmas_from = get_config('christmas_from', false);
    $data->christmas_to = get_config('christmas_to', false);
    $gsform->set_data($data);
}


echo $output->header();

echo $gsform->render();

echo $output->footer();