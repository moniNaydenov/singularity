<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 3/7/15
 * Time: 6:15 PM
 */

require_once(__DIR__ . '/../config.php');



$PAGE->set_url('/admin/changepassword.php');

$PAGE->set_title(get_string('changepassword'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');


class change_password_form extends \core\form {
    public function def() {
        $this->add_field(new \core\form_field\password('oldpass', '', get_string('oldpassword')));
        $this->add_field(new \core\form_field\password('pass', '', get_string('password')));
        $this->add_field(new \core\form_field\password('pass2', '', get_string('password_retype')));
        $this->add_submit_buttons(get_string('change'), get_string('cancel'));
    }
}

$message = '';
$form = new change_password_form();

if ($result = $form->get_data()) {
    if ($form->is_cancelled()) {
        redirect(new \surl('/admin'));
    } else {
        $userhandler = \core\userhandler::instance_from_USER();
        if ($userhandler->change_password($result->oldpass, $result->pass, $result->pass2)) {
            redirect($PAGE->get_url(), get_string('password_changed_ok'), 10);
        } else {
            redirect($PAGE->get_url(), get_string('password_changed_fail'), 10);
        }
    }
}


$output = $PAGE->get_output();

echo $output->header();


$content = $form->render();
if ($message != '') {
    $content .= html_writer::messagebox($message, 'success');
}

echo $content;

echo $output->footer(false);
