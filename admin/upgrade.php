<?php


require_once(__DIR__ . '/../config.php');

require_once($CFG->libroot . '/upgrade.php');

require_login();
require_permission('core/admin:manage');

$redirecturl = new surl('/admin/index.php');

if (upgrade_pending()) {
    upgrade_singularity();
    purge_caches();
    $message = sprintf(get_string('upgrade:success', 'admin'), $CFG->version);
    redirect($redirecturl, $message);
} if (site_upgrade_pending()) {
    upgrade_site();
    purge_caches();
    $message = sprintf(get_string('upgrade:site:success', 'admin'), $CFG->sitename, $SITE->version);
    redirect($redirecturl, $message);
} else {
    redirect($redirecturl, get_string('upgrade:noupgradepending', 'admin'));
}
die;
