<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


define('DURING_SETUP', true);
define('DISABLE_GLOBAL_COUNTER', true);

require_once(__DIR__ .'/../config.php');

// create folders in dataroot

$pleaseensure = sprintf('Please ensure %s ($CFG->dataroot) is empty before installing!', $CFG->dataroot);

$filesdir = $CFG->dataroot . '/files';

if (!file_exists($filesdir)) {
    mkdir($filesdir);
    echo 'Successfully created ' . $CFG->dataroot . '/files!' . PHP_EOL;
} else {
    raise_exception(sprintf('Directory "files" already exists! '. $pleaseensure));
}

file_put_contents($CFG->dataroot . '/.htaccess', 'deny from all
AllowOverride None
Note: this file is broken intentionally, we do not want anybody to undo it in subdirectory!');
echo 'Successfully created .htaccess';


$sql = file_get_contents(__DIR__ . '/singularity.sql');

//TODO: execute sql
