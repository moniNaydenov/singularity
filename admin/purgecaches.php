<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


require_once(__DIR__ . '/../config.php');

$revonly = optional_param('revonly', false, PARAM_BOOL);

$PAGE->set_url('/admin/purgecaches.php');

$PAGE->set_title(get_string('purgecaches'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

require_sessionid();

if ($revonly) {
    $nextrev = update_revision();
    $result = 'Updated revision to <strong>' . $nextrev . '</strong>';
} else {
    $result = purge_caches();

    $result = '<pre>' . $result . '</pre>';
}



redirect($_SERVER['HTTP_REFERER'], 'Caches successfully purged!' . $result, 1000);
