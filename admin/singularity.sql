-- Singularity is an intelligent, object-oriented and secure php platform
-- Copyright (C) 2020 Simeon Naydenov
--
-- Singularity is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Singularity is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Singularity.  If not, see <http:--www.gnu.org/licenses/>.

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(256) NOT NULL,
  `site` varchar(32) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `counter` (
  `id` int(10) unsigned NOT NULL,
  `shortname` varchar(128) NOT NULL,
  `count` bigint(20) unsigned NOT NULL,
  `lastmodified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(10) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  `parent` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `uniquehash` varchar(40) NOT NULL,
  `filesize` int(11) NOT NULL,
  `mimetype` varchar(64) NOT NULL,
  `checksum` varchar(40) NOT NULL,
  `lastmodified` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `gallery` (
 `id` int(10) unsigned NOT NULL,
 `shortname` varchar(64) NOT NULL,
 `title` text NOT NULL,
 `type` varchar(16) DEFAULT '0',
 `lastmodified` int(10) NOT NULL,
 `modifiedby` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `gallery_item` (
  `id` int(1) unsigned NOT NULL,
  `galleryid` int(10) NOT NULL,
  `sortorder` int(10) NOT NULL,
  `title` varchar(64) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `lastmodified` int(10) NOT NULL,
  `modifiedby` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `html_block` (
  `id` int(10) unsigned NOT NULL,
  `blockid` varchar(256) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `content` text NOT NULL,
  `css` text NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `lastmodified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) UNSIGNED NOT NULL,
  `action` varchar(128) NOT NULL,
  `severity` tinyint(4) NOT NULL,
  `affected_table` varchar(128) NOT NULL,
  `affected_objid` varchar(128) NOT NULL,
  `additional_info` varchar(512) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `userid` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `nav_item` (
  `id` int(10) UNSIGNED NOT NULL,
  `sortorder` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `visible` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(128) NOT NULL,
  `target` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `sitepageid` int(11) NOT NULL,
  `seourl` varchar(256) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `summary` text NOT NULL,
  `image` varchar(256) NOT NULL,
  `imagealt` varchar(256) NOT NULL,
  `visible` int(11) NOT NULL,
  `lastmodified` int(11) NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `search_index` (
  `id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `site_page` (
  `id` int(10) unsigned NOT NULL,
  `pageid` varchar(256) NOT NULL,
  `pagetype` varchar(128) NOT NULL,
  `navitemid` int(11) NOT NULL,
  `lang` varchar(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `showtitle` int(1) NOT NULL,
  `metadescription` text NOT NULL,
  `structure` text NOT NULL,
  `css` text NOT NULL,
  `modifiedby` int(11) NOT NULL,
  `lastmodified` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `lastip` varchar(40) DEFAULT NULL,
  `lastlogin` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1001 ;


-- primary keys

ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `counter`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniquehash` (`uniquehash`);

ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `gallery_item`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `html_block`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `nav_item`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `search_index`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `site_page`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE `username` (`username`);

-- autoincrements

ALTER TABLE `config`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `counter`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `file`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `gallery_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `html_block`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `nav_item`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `search_index`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `site_page`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;


INSERT INTO `user` (`id`, `username`, `password`, `lastip`, `lastlogin`) VALUES
  (999, 'guest', 'NOT_CACHED', NULL, NULL),
  (1000, 'root', '$2y$10$0XmXTVCCZI9gGo.fJrW1M.8QunpNBlhH.hiuoYOzTEtrHemXecHge', NULL, NULL);
-- username: root, password: toor

INSERT INTO `config` (`id`, `name`, `value`) VALUES
  (1, 'version', '0'),
  (2, 'revision', '0'),
  (3, 'analytics', NULL);

INSERT INTO `counter` (`shortname`, `count`, `lastmodified`) VALUES
  ('global', 0, 0);

INSERT INTO `file` (`id`, `type`, `parent`, `filename`, `uniquehash`, `filesize`, `mimetype`, `checksum`, `lastmodified`, `modifiedby`) VALUES
  (1, 1, -1, '/', '', 0, '', '', 0, 0);
