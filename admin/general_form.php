<?php

defined('INTERNAL') || die;


class general_settings_form extends \core\form {
    public function def() {
        global $CFG, $SITE, $DB;
        $options = array(
            0 => get_string('no'),
            1 => get_string('yes')
        );
        $this->add_field(new \core\form_field\dropdown('cookieconsentshow', 0, $options, get_string('cookieconsent:show', 'admin')));
        $this->add_field(new \core\form_field\htmleditor('cookieconsetcontent', '', get_string('cookieconsent:content', 'admin')));

        $this->add_field(new \core\form_field\dropdown('performanceinfoshow', 0, $options, get_string('performanceinfo:show', 'admin')));
        $this->add_field(new \core\form_field\dropdown('debug', 0, $options, 'debug'));
        $this->add_field(new \core\form_field\datetime('christmas_from', 0, get_string('christmas:from', 'admin'), \core\form_field\datetime::DATEANDTIME));
        $this->add_field(new \core\form_field\datetime('christmas_to', 0, get_string('christmas:to', 'admin'), \core\form_field\datetime::DATEANDTIME));

        $this->add_submit_buttons(get_string('save'));
    }
}