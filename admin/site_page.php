<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/site_page_form.php');

$id = optional_param('id', false, PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_RAW);

$PAGE->set_url(new surl('/admin/site_page.php', array(
        'id' => $id,
        'returnurl' => $returnurl
    )
));
$PAGE->set_title(get_string('sitepages'));
$PAGE->set_pagetype('admin');
$PAGE->add_inline_js(array('jquery'), '
        $(window).bind(\'keydown\', function(event) {
            if (event.ctrlKey || event.metaKey) {
                if (String.fromCharCode(event.which).toLowerCase() === \'s\') {
                    event.preventDefault();
                    $(\'input[name="__formapply__subm_btn"]\').click();
                }
            }
        });
', '$');

require_login();
require_permission('core/admin:manage');

$output = $PAGE->get_output();

$spform = new  site_page_form();

$redirectmessage = false;
$noredirect = false;

if ($spform->is_cancelled()) {
    if ($returnurl) {
        $returnurl = rawurldecode($returnurl);
        redirect(new surl($returnurl));
    } else {
        redirect(new surl('/admin/site_pages.php'));
    }
    die;
} else if ($data = $spform->get_data()) {
    if ($data->id == -1) {
        $record = new stdClass();
        $record->id = -1;
    } else {
        $record = $DB->get_record('site_page', array('id' => $data->id));
    }
    if ($record != false) {
        $record->title = $data->title;
        $record->showtitle = $data->showtitle;
        $record->lang = $data->lang;
        $record->pageid = $data->pageid;
        $record->structure = $data->structure;
        $record->css = $data->css;
        $record->pagetype = $data->pagetype;
        $record->navitemid = $data->navitemid;
        $record->metadescription = $data->metadescription;

        $record->modifiedby = $USER->id;
        $record->lastmodified = time();
        if ($record->id == -1) {
            unset($record->id);
            $record->id = $DB->insert_record('site_page', $record);
            add_to_log('insert', 'site_page', $record->id, '');
                $redirectmessage = 'New record inserted successfully!';
        } else {
            $DB->update_record('site_page', $record);
            add_to_log('update', 'site_page', $record->id, '');
            $redirectmessage = 'Record ' . $id . ' updated successfully!';
        }

        // search indexing...
        $sp = \core\site_page::create_from_id($record->id);
        $sp->index_for_search($output);

        if ($spform->is_applied()) {
            $noredirect = true;
            $id = $record->id;
        }
    } else {
        $redirectmessage = 'Record with id ' . $id . ' not found!';
    }
} else if ($id) {
    if ($id != -1) {
        $record = $DB->get_record('site_page', array('id' => $id));
        if ($record == false) {
            $redirectmessage = 'Record with id ' . $id . ' not found!';
        } else {
            $spform->set_values($record);
        }
    }
} else {
    $redirectmessage = 'Parameter {id} missing!';
}

if ($redirectmessage !== false) {
    if ($noredirect == true) {

        redirect($PAGE->get_url(), $redirectmessage);
        die;
    } else {
        if ($returnurl) {
            $returnurl = rawurldecode($returnurl);
            redirect(new surl($returnurl), $redirectmessage, 5);
        } else {
            redirect(new surl('/admin/site_pages.php'), $redirectmessage, 5);
        }
        die;
    }


}


add_to_log('view', 'site_page', $id);

echo $output->header();

echo $spform->render();

echo $output->footer(false);