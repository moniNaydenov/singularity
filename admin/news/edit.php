<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_form.php');

$id = optional_param('id', false, PARAM_INT);

$PAGE->set_url('/admin/news/edit.php');

$PAGE->set_title(get_string('editnews', 'admin'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$output = $PAGE->get_output();

$nform = new news_edit_form();

$redirectmessage = false;

if ($nform->is_cancelled()) {
    redirect(new surl('/admin/news/index.php'));
    $redirectmessage = '';
    die;
} else if ($data = $nform->get_data()) {

    if ($data->id == -1) {
        $record = new stdClass();
        $record->id = -1;
    } else {
        $record = $DB->get_record('news', array('id' => $data->id));
    }

    $site_page = null;

    if (!empty($record->sitepageid)) {
        $site_page = $DB->get_record('site_page', array('id' => $record->sitepageid));
        if ($site_page) {
            $site_page->title = $data->title;
            $site_page->lang = $data->lang;
            $site_page->pageid = 'news-' . $record->id;
            $site_page->metadescription = $data->metadescription;
            $site_page->modifiedby = $USER->id;
            $site_page->lastmodified = time();
            $DB->update_record('site_page', $site_page);
        }
    }

    if (empty($record->sitepageid) || empty($site_page)) {
        $site_page = new stdClass;
        $site_page->pageid = 'news--1';
        $site_page->pagetype = 'news';
        $site_page->lang = $data->lang;
        $site_page->title = $data->title;
        $site_page->showtitle = 1;
        $site_page->structure = '';
        $site_page->navitemid = -1;
        $site_page->css = '';
        $site_page->metadescription = $data->metadescription;
        $site_page->modifiedby = $USER->id;
        $site_page->lastmodified = time();
        $site_page->id = $DB->insert_record('site_page', $site_page);
        $record->sitepageid = $site_page->id;
    }

    if ($record != false) {
        $record->summary = $data->summary;
        $record->seourl = $data->seourl;

        $record->image = '';
        if ($data->image) {
            $record->image = $data->image->get_url();
        }
        $record->imagealt = $data->imagealt;
        $record->visible = $data->visible;
        $record->timestamp = $data->timestamp;
        $record->modifiedby = $USER->id;
        $record->lastmodified = time();
        if ($record->id == -1) {
            unset($record->id);
            $record->id = $DB->insert_record('news', $record);
            $site_page->pageid = 'news-' . $record->id;
            $DB->update_record('site_page', $site_page);


            $redirectmessage = 'New news "' . $data->title . '" (' . $record->id . ') added successfully!';

        } else {
            $DB->update_record('news', $record);
            $redirectmessage = 'News "' . $data->title . '" (' . $record->id . ') updated successfully!';
        }
    } else {
        $redirectmessage = 'Record with id ' . $id . ' not found!';
    }
} else if ($id) {
    if ($id != -1) {
        $record = $DB->get_record('news', array('id' => $id));
        if ($record == false) {
            $redirectmessage = 'Record with id ' . $id . ' not found!';
        } else {
            $site_page = $DB->get_record('site_page', array('id' => $record->sitepageid));
            $record->title = $site_page->title;
            $record->lang = $site_page->lang;
            $record->metadescription = $site_page->metadescription;
            if ($record->image) {
                $image = \core\file::create_from_url($record->image);
                $record->image = $image;
            }
            $nform->set_values($record);
        }
    }
} else {
    $redirectmessage = 'Parameter {id} missing!';
}

if ($redirectmessage !== false) {
    redirect(new surl('/admin/news/index.php'), $redirectmessage);
    die;
}

echo $output->header();

echo html_writer::heading(2, get_string('editnews', 'admin'));

echo $nform->render();

echo $output->footer(false);