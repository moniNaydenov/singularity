<?php
/**
 * Created by PhpStorm.
 * User: stoen
 * Date: 3/10/17
 * Time: 11:40 PM
 */

defined('INTERNAL') || die;




class news_edit_form extends \core\form {
    public function def() {
        global $CFG;
        $this->add_field(new \core\form_field\hidden('id', -1, PARAM_INT));
        $this->add_field(new \core\form_field\text('title', '', get_string('title'), PARAM_TEXT));
        $this->add_field(new \core\form_field\text('seourl', '', get_string('seourl', 'admin'), PARAM_TEXT));
        $this->add_field(new \core\form_field\lang('lang', $CFG->lang, get_string('language')));
        $this->add_field(new \core\form_field\textarea('metadescription', '', get_string('metatag:description', 'admin')));
        $visible = array(0 => get_string('hidden'), 1 => get_string('visible'));
        $this->add_field(new \core\form_field\dropdown('visible', 1, $visible, get_string('visible')));
        $this->add_field(new \core\form_field\datetime('timestamp', time(), get_string('date'), \core\form_field\datetime::DATEANDTIME));
        $this->add_field(new \core\form_field\image('image', get_string('image')));
        $this->add_field(new \core\form_field\text('imagealt', '', get_string('imagealt', 'admin'), PARAM_TEXT));
        $this->add_field(new \core\form_field\htmleditor('summary', '', get_string('summary'), 420, 300, PARAM_RAW));

        $this->add_submit_buttons(get_string('save'), get_string('back'));
    }
}