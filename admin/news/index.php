<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_form.php');

$id = optional_param('id', false, PARAM_INT);
$delete = optional_param('delete', false, PARAM_BOOL);

$PAGE->set_url('/admin/news/index.php');

$PAGE->set_title(get_string('editnews', 'admin'));

$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$sql = 'SELECT
    n.*,
    sp.title
FROM
    news n
JOIN
    site_page sp ON sp.id = n.sitepageid';

$tableeditor = new \core\admin\table_editor($sql,
    array('title', 'timestamp'), 'timestamp DESC', true, 'id');

$tableeditor->set_datefields(array('timestamp' => '%Y-%m-%d %H:%M'));
$tableeditor->add_row_action('editfull', get_string('news:editfull', 'admin'));
$tableeditor->set_editbutton(true, get_string('news:editshort', 'admin'));

$action = $tableeditor->get_row_action();

$output = $PAGE->get_output();

if ($id != false) {

    $record = $DB->get_record('news', array('id' => $id));
    if (!$record && $id != -1) {
        redirect($PAGE->get_url(), 'Record with id ' . $id . ' not found!');
        die;
    }

    if ($delete && $id != -1) {
        require_sessionid();
        if ($record) {
            $DB->delete_record('news', array('id' => $id));
            $DB->delete_record('site_page', array('id' => $record->sitepageid));
            redirect($PAGE->get_url(), get_string('recorddeleteok', 'admin'));
            die;
        } else {
            redirect($PAGE->get_url(), get_string('recorddeletefail', 'admin'));
            die;
        }

    } else if ($action == 'editfull'  && $id != -1) {
        $url = new surl('/admin/site_page_editor.php', array('id' => $record->sitepageid, 'returnurl' => $PAGE->get_url()->out(false, true)));
        $url->add_sessionid();
        redirect($url);
        die;
    } else {
        $url = new surl('/admin/news/edit.php', array('id' => $id));
        $url->add_sessionid();
        redirect($url);
        die;
    }
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer(false);