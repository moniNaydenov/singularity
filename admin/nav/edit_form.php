<?php
/**
 * Created by PhpStorm.
 * User: stoen
 * Date: 3/10/17
 * Time: 11:40 PM
 */

defined('INTERNAL') || die;




class nav_item_edit_form extends \core\form {
    public function def() {
        global $CFG, $DB;
        $this->add_field(new \core\form_field\hidden('id', -1, PARAM_INT));
        $this->add_field(new \core\form_field\text('title', '', get_string('title'), PARAM_TEXT));
        $this->add_field(new \core\form_field\lang('lang', $CFG->lang, get_string('language')));
        $this->add_field(new \core\form_field\text('url', '', get_string('url'), PARAM_RAW));

        $navitems = $DB->get_records_menu('nav_item', 'id', 'title', array(), '*', 'parent ASC, sortorder ASC');
        $navitems = array(0 => get_string('root')) + $navitems;

        $this->add_field(new \core\form_field\dropdown('parent', 0, $navitems, get_string('parent')));
        $this->add_field(new \core\form_field\text('sortorder', '', get_string('order'), PARAM_INT));

        $visible = array(0 => get_string('hidden'), 1 => get_string('visible'));
        $this->add_field(new \core\form_field\dropdown('visible', 1, $visible, get_string('visible')));

        $target = array('' => get_string('target:self', 'admin'), '_blank' => get_string('target:blank', 'admin'));
        $this->add_field(new \core\form_field\dropdown('target', '', $target, get_string('target')));

        $this->add_submit_buttons(get_string('save'), get_string('back'));
    }
}