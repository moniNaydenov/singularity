<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_form.php');

$id = optional_param('id', false, PARAM_INT);

$PAGE->set_url('/admin/nav/edit.php');

$PAGE->set_title(get_string('admin_nav_items_title'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$output = $PAGE->get_output();

$miform = new nav_item_edit_form();

$redirectmessage = false;

if ($miform->is_cancelled()) {
    redirect(new surl('/admin/nav/index.php'));
    $redirectmessage = '';
    die;
} else if ($data = $miform->get_data()) {

    if ($data->id == -1) {
        $record = new stdClass();
        $record->id = -1;
    } else {
        $record = $DB->get_record('nav_item', array('id' => $data->id));
    }

    if ($record != false) {
        $record->title = $data->title;
        $record->lang = $data->lang;
        $record->url = $data->url;
        $record->visible = $data->visible;
        $record->sortorder = $data->sortorder;
        $record->parent = $data->parent;
        $record->target = $data->target;

        if ($record->id == -1) {
            unset($record->id);
            $record->id = $DB->insert_record('nav_item', $record);
            $redirectmessage = 'New nav item "' . $data->title . '" (' . $record->id . ') inserted successfully!';
        } else {
            $DB->update_record('nav_item', $record);
            $redirectmessage = 'Nav item "' . $data->title . '" (' . $record->id . ') updated successfully!';
        }
    } else {
        $redirectmessage = 'Record with id ' . $id . ' not found!';
    }
} else if ($id) {
    if ($id != -1) {
        $record = $DB->get_record('nav_item', array('id' => $id));
        if ($record == false) {
            $redirectmessage = 'Record with id ' . $id . ' not found!';
        } else {
            $miform->set_values($record);
        }
    }
} else {
    $redirectmessage = 'Parameter {id} missing!';
}

if ($redirectmessage !== false) {
    redirect(new surl('/admin/nav/index.php'), $redirectmessage, 5);
    die;
}

echo $output->header();

echo html_writer::heading(2, get_string('admin_nav_items_title'));

echo $miform->render();

echo $output->footer(false);