<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 02.02.17
 * Time: 12:52
 */

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_form.php');

$id = optional_param('id', false, PARAM_INT);
$delete = optional_param('delete', false, PARAM_BOOL);

$PAGE->set_url('/admin/nav/index.php');

$PAGE->set_title(get_string('admin_nav_items_title'));
$PAGE->set_pagetype('admin');


require_login();
require_permission('core/admin:manage');

$tableeditor = new \core\admin\nested_table_editor('nav_item', 'parent',
    array('title', 'lang', 'url'), 'sortorder');

//$tableeditor->show_movebuttons(true);

$output = $PAGE->get_output();

if ($id != false) {


    if ($delete) {
        if($tableeditor->delete_record($id)) {
            redirect($PAGE->get_url(), get_string('recorddeleteok', 'admin'));
            die;
        } else {
            redirect($PAGE->get_url(), get_string('recorddeletefail', 'admin'));
            die;
        }
    } else {
        $url = new surl('/admin/nav/edit.php', array('id' => $id));
        $url->add_sessionid();
        redirect($url);
        die;
    }
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer(false);