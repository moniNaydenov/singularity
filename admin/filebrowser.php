<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.


require_once(__DIR__ . '/../config.php');


$browseonly = optional_param('browseonly', false, PARAM_BOOL);
$submittoform = optional_param('submittoform', false, PARAM_BOOL);
$ckfuncnum = optional_param('CKEditorFuncNum', false, PARAM_ALPHANUM);
$roothash = optional_param('root', 1, PARAM_FILEHASH);

$PAGE->set_url(new surl('/admin/filebrowser.php', array(
   'browseonly' => $browseonly,
   'CKEditorFuncNum' => $ckfuncnum,
   'submittoform' => $submittoform,
   'root' => $roothash
)));

$PAGE->set_title(get_string('admin_manage_files', 'admin'));
$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');


class fileuploadform extends \core\form {
    protected function def() {
        $this->add_field(new \core\form_field\fileupload('customfile', ''));
        $this->add_field(new \core\form_field\submit('send', get_string('upload', 'admin'), '', ''));
    }
}

class newdirform extends \core\form {
    protected function def() {
        $this->add_field(new \core\form_field\text('dirname', '', get_string('name'), PARAM_FILENAME));
        $this->add_field(new \core\form_field\submit('send', get_string('create'), '', ''));
    }
}

class fileeditform extends \core\form {
    protected function def() {
        $this->add_field(new \core\form_field\hidden('uniquehash', '', PARAM_FILEHASH));
        $this->add_field(new \core\form_field\hidden('newfilename', '', PARAM_FILENAME));
        $this->add_field(new \core\form_field\hidden('delete', '0', PARAM_BOOL));
        $this->add_field(new \core\form_field\hidden('rename', '0', PARAM_BOOL));
    }
}


if ($roothash == 1) {
    $root = \core\file::get_root();
} else {
    $root = \core\file::create_from_hash($roothash);
}


if ($root == false) {
    security::die_bad_request('invalid parent');
}
if ($root->is_file()) {
    redirect($root->get_url());
    die;
}


$uploadform = new fileuploadform();
$newdirform = new newdirform();
$fileeditform = new fileeditform();

$filelink = '';
if ($result = $uploadform->get_data()) {
    $file = $result->customfile;
    $f = \core\file::create_new_file_from_upload($file->filename, $file->tmpfilename, $root);
    if ($f != false) {
        redirect($PAGE->get_url(), sprintf(get_string('fileuploadok', 'admin'), $file->filename));
        die;
    }
}
if ($result = $newdirform->get_data()) {
    $dirname = $result->dirname;
    $d = $root->create_subdir($dirname);
    if ($d != false) {
        redirect($PAGE->get_url(), sprintf(get_string('dircreatedok', 'admin'), $d->get_filename()));
        die;
    }
}

if ($result = $fileeditform->get_data()) {
    $uniquehash = $result->uniquehash;
    $file = \core\file::create_from_hash($uniquehash);
    if ($file) {
        $filename = $file->get_filename();
        if (!$result->delete) {
            if ($file->rename($result->newfilename)) {
                redirect($PAGE->get_url(), sprintf(get_string('filerenameok', 'admin'), $filename, $file->get_filename()));
                die;
            } else {
                redirect($PAGE->get_url(), sprintf(get_string('filerenamefail', 'admin'), $filename, $file->get_filename()));
                die;
            }
        } else {
            if ($file->delete()) {
                redirect($PAGE->get_url(), sprintf(get_string('filedeleteok', 'admin'), $filename));
                die;
            } else {
                redirect($PAGE->get_url(), sprintf(get_string('filedeletefail', 'admin'), $filename));
                die;
            }
        }
    }
}

$output = $PAGE->get_output();

$PAGE->add_js('js/admin/filebrowser');

$fbr = new \core\filebrowser($root, $browseonly, $ckfuncnum, $submittoform, $uploadform, $newdirform, $fileeditform);

echo $output->header(!$browseonly);

echo $output->render($fbr);

echo $output->footer(!$browseonly);

