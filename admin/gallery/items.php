<?php

require_once(__DIR__ . '/../../config.php');

$galleryid = required_param('galleryid', PARAM_INT);
$id = optional_param('id', false, PARAM_INT);
$move = optional_param('move', false, PARAM_BOOL);
$up = optional_param('up', false, PARAM_BOOL);
$delete = optional_param('delete', false, PARAM_BOOL);

$PAGE->set_url(new surl('/admin/gallery/items.php', ['galleryid' => $galleryid]));

$PAGE->set_title(get_string('gallery:manage', 'admin'));

$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');

$gallery = \core\gallery\gallery::instance_from_dbid($galleryid);
if (!$gallery) {
    redirect('/admin/gallery/index.php', 'Invalid gallery id');
}

$tableeditor = new \core\admin\table_editor('gallery_item',
    array('title', 'description', 'image', 'lastmodified'), 'sortorder ASC');
$tableeditor->set_sql_filters(['galleryid' => $gallery->id]);
$tableeditor->set_datefields(array('lastmodified' => '%Y-%m-%d %H:%M:%S'));
$tableeditor->set_editbutton(true, get_string('edit', 'core'));
$tableeditor->show_movebuttons(true);
$tableeditor->set_record_handler(function($record) {
    $image = \core\file::create_from_hash($record->image);
    if ($image) {
        $record->image = '<img src="' . $image->get_url() . '" class="img-thumbnail" />';
    } else {
        $record->image = false;
    }
    return $record;
});

$action = $tableeditor->get_row_action();

$output = $PAGE->get_output();

if ($id != false) {
    if ($move) {
        $tableeditor->move_record($id, $up);
        redirect($PAGE->url);
        die;
    } else if ($delete) {
        $result = $gallery->delete_item($id);
        $message = $result ? get_string('recorddeleteok', 'admin') : get_string('recorddeletefail', 'admin');
        redirect($PAGE->url, $message);
        die;
    }

    $edititemurl = new surl('/admin/gallery/edit_item.php', ['id' => $id, 'galleryid' => $gallery->id]);
    redirect($edititemurl);
    die;
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer(false);