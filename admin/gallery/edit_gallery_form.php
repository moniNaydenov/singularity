<?php


use \core\form;
use \core\form_field;

class edit_gallery_form extends form {
    protected function def() { global $CFG;
        $this->add_field(new form_field\hidden('id', -1, PARAM_INT));
        $this->add_field(new form_field\text('shortname', '', get_string('gallery:shortname', 'admin'), PARAM_TEXT));
        $this->add_field(new form_field\text('title', '', get_string('title'), PARAM_TEXT));
        $types = \core\gallery\gallery::get_gallery_types();
        $this->add_field(new form_field\dropdown('type', 1, $types, get_string('gallery:type', 'admin')));
        $this->add_submit_buttons(get_string('save'), get_string('back'));
    }
}
