<?php


use \core\form;
use \core\form_field;

class edit_item_form extends form {
    protected function def() { global $CFG;
        $this->add_field(new form_field\hidden('id', -1, PARAM_INT));
        $this->add_field(new form_field\text('title', '', get_string('title'), PARAM_TEXT));
        $this->add_field(new form_field\image('image', get_string('image')));
        $this->add_field(new form_field\htmleditor('description', '', get_string('description'), PARAM_TEXT));
        $this->add_field(new form_field\text('link', '', get_string('gallery:link'), PARAM_TEXT));
        $this->add_submit_buttons(get_string('save'), get_string('back'));
    }
}
