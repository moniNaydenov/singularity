<?php

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_gallery_form.php');

$id = required_param('id', PARAM_INT);

$PAGE->set_url(new surl('/admin/gallery/edit_gallery.php'));

$PAGE->set_title(get_string('gallery:edit', 'admin'));

$PAGE->set_pagetype('admin');

$backurl = new surl('/admin/gallery/index.php');

require_login();
require_permission('core/admin:manage');

$form = new edit_gallery_form();

$output = $PAGE->get_output();

if ($id != -1) {
    $gallery = \core\gallery\gallery::instance_from_dbid($id);
    if (!$gallery) {
        redirect($backurl, 'Invalid gallery id!');
        die;
    }
}

if ($form->is_cancelled()) {
    redirect($backurl);
    die;
} else if ($data = $form->get_data()) {
    $data->lastmodified = time();
    $data->modifiedby = $USER->id;
    if ($id == -1) {
        $gallery = \core\gallery\gallery::create_from_obj($data);
        redirect($backurl, 'Gallery created successfully!');
        die;
    } else {
        $gallery->update_from_form($data);
        $gallery->update_to_db();
        redirect($backurl, 'Gallery updated successfully!');
        die;
    }
} else if ($id != -1) {
    $form->set_data($gallery->get_obj());
}

echo $output->header();

echo $form->render($output);

echo $output->footer(false);

