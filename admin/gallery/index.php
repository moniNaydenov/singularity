<?php

require_once(__DIR__ . '/../../config.php');

$id = optional_param('id', false, PARAM_INT);
$delete = optional_param('delete', false, PARAM_BOOL);

$PAGE->set_url(new surl('/admin/gallery/index.php'));

$PAGE->set_title(get_string('gallery:manage', 'admin'));

$PAGE->set_pagetype('admin');

require_login();
require_permission('core/admin:manage');


$tableeditor = new \core\admin\table_editor('gallery',
    array('title', 'shortname', 'lastmodified'), 'lastmodified DESC');

$tableeditor->set_datefields(array('lastmodified' => '%Y-%m-%d %H:%M:%S'));
$tableeditor->add_row_action('edititems', get_string('gallery:edititems', 'admin'));
$tableeditor->set_editbutton(true, get_string('edit', 'core'));

$action = $tableeditor->get_row_action();

$output = $PAGE->get_output();


if ($id != false) {

    $editgalleryurl = new surl('/admin/gallery/edit_gallery.php', ['id' => $id]);
    $edititemsurl = new surl('/admin/gallery/items.php', ['galleryid' => $id]);
    if ($action == false) {
        redirect($editgalleryurl);
        die;
    } else if ($action == 'edititems') {
        redirect($edititemsurl);
        die;
    }
}

echo $output->header();

echo $output->render_table_editor($tableeditor);

echo $output->footer(false);