<?php

require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/edit_item_form.php');

$galleryid = required_param('galleryid', PARAM_INT);
$id = required_param('id', PARAM_INT);

$PAGE->set_url('/admin/gallery/edit_item.php');

$PAGE->set_title(get_string('gallery:edititem', 'admin'));

$PAGE->set_pagetype('admin');

$backurl = new surl('/admin/gallery/items.php', ['galleryid' => $galleryid]);

require_login();
require_permission('core/admin:manage');

$form = new edit_item_form();

$output = $PAGE->get_output();

$gallery = \core\gallery\gallery::instance_from_dbid($galleryid);
if (!$gallery) {
    redirect($backurl, 'Invalid gallery  id!');
    die;
}

if ($id != -1) {
    $item = \core\gallery\item::instance_from_dbid($id);
    if (!$item) {
        redirect($backurl, 'Invalid gallery item id!');
        die;
    }
}

if ($form->is_cancelled()) {
    redirect($backurl);
    die;
} else if ($data = $form->get_data()) {
    $data->lastmodified = time();
    $data->modifiedby = $USER->id;
    if ($data->image) {
        $data->image = $data->image->get_uniquehash();
    }
    $data->link = trim($data->link);
    if (mb_strlen($data->link) == 0) {
        $data->link = null;
    }
    if ($id == -1) {
        $data->galleryid = $galleryid;
        $data->sortorder = $gallery->get_max_sortorder();
        $item = \core\gallery\item::create_from_obj($data);
        redirect($backurl, 'Gallery item added successfully!');
        die;
    } else {
        $item->update_from_form($data);
        $item->update_to_db();
        redirect($backurl, 'Gallery item updated successfully!');
        die;
    }
} else if ($id != -1) {
    $form->set_data($item->get_obj());
}

echo $output->header();

echo $form->render($output);

echo $output->footer(false);

