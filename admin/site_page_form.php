<?php
/**
 * Created by PhpStorm.
 * User: stoen
 * Date: 3/10/17
 * Time: 11:40 PM
 */

defined('INTERNAL') || die;




class site_page_form extends \core\form {
    public function def() {
        global $CFG, $SITE, $DB;

        $this->add_field(new \core\form_field\hidden('id', -1, PARAM_INT));
        $this->add_field(new \core\form_field\hidden('htmlblocks', '', PARAM_RAW));
        $this->add_field(new \core\form_field\text('pageid', '', get_string('pageid'), PARAM_TEXT));
        $this->add_field(new \core\form_field\dropdown('pagetype', 'standard', \core\site_page::get_available_pagetypes(), get_string('pagetype', 'admin')));
        $this->add_field(new \core\form_field\lang('lang', $CFG->lang, get_string('language')));
        $this->add_field(new \core\form_field\text('title', '', get_string('title'), PARAM_TEXT));
        $options = array(0 => get_string('no'), 1 => get_string('yes'));
        $this->add_field(new \core\form_field\dropdown('showtitle', 1, $options, get_string('showtitle', 'admin')));


        $navitems = $DB->get_records_menu('nav_item', 'id', 'title', array(), '*', 'parent ASC, sortorder ASC');
        $navitems = array(-1 => get_string('none')) + $navitems;
        $this->add_field(new \core\form_field\dropdown('navitemid', -1, $navitems, get_string('navitemid', 'admin')));

        $this->add_field(new \core\form_field\textarea('metadescription', '', get_string('metatag:description', 'admin')));
        $this->add_field(new \core\form_field\codeeditor('structure', '', get_string('structure'), 'html'));
        $this->add_field(new \core\form_field\codeeditor('css', '', 'CSS', 'css'));

        $this->add_submit_buttons(get_string('saveandback', 'admin'), get_string('back'), get_string('save'));
    }
}