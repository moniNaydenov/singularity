<?php
require_once(__DIR__ . '/config.php');

$what = optional_param('q', PARAM_TEXT);

$search = new \core\search\search();
$results = $search->search($what);

$PAGE->set_url(new surl('/search.php'));

$PAGE->set_title(get_string('search:result'));
$PAGE->set_heading(get_string('search:result'));

$output = $PAGE->get_output();

echo $output->header();

echo $output->render_from_template('search/results', $search->export_for_template($output));

echo $output->footer();