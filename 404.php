<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 2:58 PM
 */


require_once(__DIR__ . '/config.php');

header('HTTP/1.1 404 Not Found');

$pageid = '404';

$sp = \core\site_page::create_from_pageid_lang($pageid, $CFG->lang);

if ($sp) {

    $sp->set_page_metatags();


    $PAGE->set_url('/404.php');


    $PAGE->set_title($sp->get_title());
    $PAGE->set_heading($sp->get_title());


    $sp->add_breadcrumb();

    $output = $PAGE->get_output();

    echo $output->header();

    echo $output->render_site_page($sp);

    echo $output->footer();
    die;
}


$PAGE->title = 'not found';

$output = $PAGE->get_output();

echo $output->header();

echo '<div>Sorry, page not found :(</div>';

echo $output->footer();