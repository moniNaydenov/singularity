<?php

defined('INTERNAL') || die;

$version = YYYYMMDDXX; // e.g. 2018101800

$release = '1.0'; // Human readable version number.