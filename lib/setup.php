<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/28/15
 * Time: 7:50 PM
 */

defined('INTERNAL') || die();

// set debug to false - if not set before that
if (!isset($CFG->debug)) {
    $CFG->debug = false;
}

if ($CFG->debug == true) {
    ini_set('display_errors', 'on');
    ini_set('error_level', 'E_ALL');
} else {
    ini_set('display_errors', 'off');
    ini_set('error_level', 'E_NONE');
}

global $DB, $USER;

$CFG->scriptstarttime = microtime(true);

define('LANGUAGECOOKIE', 'language');
define('CONSENTCOOKIE', 'cookieconsent');

require_once(__DIR__ . '/spl.php');



require_once(__DIR__ . '/sec.php');
require_once(__DIR__ . '/common.php');
require_once(__DIR__ . '/html_writer.php');
require_once(__DIR__ . '/upgrade.php');
require_once(__DIR__ . '/classes/counter.php');
require_once(__DIR__ . '/Mustache/Autoloader.php');

Mustache_Autoloader::register();
//


// fix $CFG paths
$CFG->dirroot = dirname(__DIR__);
$CFG->libroot = $CFG->dirroot . '/lib';

// create surl instance for wwwroot
$CFG->wwwroot_surl = new \surl($CFG->wwwroot);

if (!defined('CLI_SCRIPT')) {
    security::check_domain();
}

// check if dataroot exists
if (!file_exists($CFG->dataroot)) {
    raise_exception(sprintf('$CFG->dataroot "%s" does not exist!', $CFG->dataroot));
}

// check if dataroot is writable
if (!is_writable($CFG->dataroot)) {
    raise_exception(sprintf('$CFG->dataroot "%s" not writable!', $CFG->dataroot));
}

// check if files directory exists

if (!file_exists($CFG->dataroot . '/files')) {
    if (!mkdir($CFG->dataroot . '/files')) {
        raise_exception(sprintf('Unable to create files directory - "%s/files" not writable!', $CFG->dataroot));
    }
}

// check if cache directory exists

if (!file_exists($CFG->dataroot . '/cache')) {
    if (!mkdir($CFG->dataroot . '/cache')) {
        raise_exception(sprintf('Unable to create files directory - "%s/cache" not writable!', $CFG->dataroot));
    }
}

// check if cache/mustache directory exists

if (!file_exists($CFG->dataroot . '/cache/mustache')) {
    if (!mkdir($CFG->dataroot . '/cache/mustache')) {
        raise_exception(sprintf('Unable to create files directory - "%s/cache" not writable!', $CFG->dataroot));
    }
}

// check if css cache directory exists

if (!file_exists($CFG->dataroot . '/cache/css')) {
    if (!mkdir($CFG->dataroot . '/cache/css')) {
        raise_exception(sprintf('Unable to create files directory - "%s/cache/css" not writable!', $CFG->dataroot));
    }
}

// setup singularity version
require_once($CFG->dirroot . '/version.php');

$CFG->version = $version;
$CFG->release = $release;

// init DB

$DB = new \core\db\provider_mysqli();

if (empty($DB->get_columns('config'))) {
    // DB structure is missing /
    $installsql = file_get_contents(__DIR__ . '/../admin/singularity.sql');
    $DB->multi_execute($installsql);

    set_config('version', $version);
    set_config('revision', time());
    redirect(new surl('/'), 'Singularity was successfully deployed!');
    die;
}

// set up revision - used when loading js, css files and images - deals with caching
$CFG->revision = get_config('revision', '0'); // to be used for loading css and js files!



// set up default homepage
$CFG->homepageid = false;
$CFG->homescriptpath = false;

// check for site and update config accordingly

if (empty($SITE)) {
    $sitepath = $CFG->dirroot . '/s';
    if (file_exists($sitepath) && is_dir($sitepath)) {
        if (file_exists($sitepath . '/config.php')) {
            require_once($sitepath . '/config.php');
        } else {
            raise_exception('Invalid site path / sitepath/config.php missing');
        }
    }
}

if (!empty($SITE)) {

    // get site dir name
    $CFG->sitename = basename($SITE->dirroot);

    // set homepage identifier
    if (!empty($SITE->homepageid)) {
        $CFG->homepageid = $SITE->homepageid;
    }
    if (!empty($SITE->homescriptpath)) {
        $SITE->homescriptpath = $SITE->dirroot . $SITE->homescriptpath;
        if (file_exists($SITE->homescriptpath) && !is_dir($SITE->homescriptpath)) {
            $CFG->homescriptpath = $SITE->homescriptpath;
        }
    }


    // setup site version
    require_once($SITE->dirroot . '/version.php');

    $SITE->version = $version;
    $SITE->release = $release;

    // configure site title
    if (isset($SITE->title)) {
        $CFG->title = $SITE->title;
    }

    // include additional css
    if (!empty($SITE->additionalcss)) {
        foreach ($SITE->additionalcss as $css) {
            $CFG->additionalcss[] = 's/' . $css;
        }
    }

    // include additional javascript
    if (!empty($SITE->additionaljs)) {
        foreach ($SITE->additionaljs as $js) {
            $CFG->additionaljs[] = 's/' . $js;
        }
    }

    // include external css
    if (!empty($SITE->externalcss)) {
        $CFG->externalcss = array_merge($CFG->externalcss, $SITE->externalcss);
    }

    // include external javascript
    if (!empty($SITE->externaljs)) {
        $CFG->externaljs = array_merge($CFG->externaljs, $SITE->externaljs);
    }

    // check for lib.php inside site folder
    if (file_exists($SITE->dirroot . '/lib.php')) {
        require_once($SITE->dirroot . '/lib.php');
    }


    // set favicon
    if (!empty($SITE->favicon)) {
        $CFG->favicon ='/s' .  $SITE->favicon;
    }

    $languages = $SITE->languages; //array_merge($CFG->languages, $SITE->languages);// TODO: fix if neccessary (bug appears when language is not implemented in SITE, but in Singularity (esp. en)
    $languages = array_keys(array_flip($languages));
    $CFG->languages = $languages;
    $CFG->defaultlang = $SITE->defaultlang;


} else {
    raise_exception('Site not found! Please check your configuration!');
}


$USER = null;

// initiate and increment global counter
$CFG->globalcounter = new counter('global');

if (!defined('DISABLE_GLOBAL_COUNTER') && !defined('AJAX_SCRIPT') && !defined('CLI_SCRIPT')) {
    $CFG->globalcounter->increment();
}

// configure timezone
if (!isset($CFG->defaulttimezone)) {
    raise_exception('Error! Default time zone not set! Please, set it in config.php $CFG->defaulttimezone');
}

date_default_timezone_set($CFG->defaulttimezone);

// set up current language
$CFG->lang = $CFG->defaultlang;
if (!defined('CLI_SCRIPT')) {
    $langcookie = get_cookie(LANGUAGECOOKIE, false, PARAM_ALPHANUM_EXT);
    if ($langcookie) {
        if (in_array($langcookie, $CFG->languages)) {
            $CFG->lang = $langcookie;
        } else {
            remove_cookie(LANGUAGECOOKIE);
        }
    }
    $langparam = optional_param('lang', false, PARAM_ALPHANUM_EXT);
    if ($langparam && in_array($langparam, $CFG->languages) && $langparam != $CFG->lang) {
        $CFG->lang = $langparam;
        set_cookie(LANGUAGECOOKIE, $langparam, time()+365*2*24*60*60, '/');
    }
    setlocale(LC_ALL, \core\stringmanager::$LOCALES[$CFG->lang]);
    setlocale(LC_NUMERIC, 'en_US');
}
$CFG->stringmanager = new \core\stringmanager();

$CFG->debug = get_config('debug', $CFG->debug);

$PAGE = new \core\page();

// initiate session
if (!defined('CLI_SCRIPT')) {
    security::initiate_session();
    \core\userhandler::preload_roles_and_permissions();
}

// setup cookie consent
if (!defined('CLI_SCRIPT')) {
    $cookieconsent = get_cookie(CONSENTCOOKIE, false, PARAM_ALPHA);
    $CFG->cookieconsent = $cookieconsent;
}

// check if upgrade is pending and perform upgrade

if (!defined('AJAX_SCRIPT') && !defined('CLI_SCRIPT')) {
    if (upgrade_pending() || site_upgrade_pending()) {
        $currenturl = current_page_url();
        $upgradeurl = new surl('/admin/upgrade.php');
        $adminurl = new surl('/admin/');
        if (isloggedin() && $currenturl->is_suburlof($adminurl) && !$currenturl->equals($upgradeurl) ) {
            redirect($upgradeurl);
            die;
        }
    }
}

