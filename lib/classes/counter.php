<?php

/**
 *
 * @package       sando
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('INTERNAL') || die();

class counter {
    private $counter;
    private $shortname;

    public function __construct($shortname) {
        $this->shortname = $shortname;
        $this->refresh_counter();
        if ($this->counter == false) {
            raise_exception(sprintf('Invalid counter name "%s"', $shortname));
        }

    }

    public function increment() {
        global $DB;
        $this->refresh_counter();
        $this->counter->count += 1;
        $this->counter->lastmodified = time();
        $DB->update_record('counter', $this->counter);
        $this->refresh_counter();
    }

    public function refresh_counter() {
        global $DB;
        $this->counter = $DB->get_record('counter', array('shortname' => $this->shortname));
    }

    public function get_count() {
        return $this->counter->count;
    }

    public function get_counter() {
        return $this->counter;
    }

    public function get_lastmodified() {
        return $this->counter->lastmodified;
    }

    public function get_shortname() {
        return $this->shortname;
    }
}