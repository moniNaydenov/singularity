<?php


namespace core\output;

use core\navigation\admin_nav;
use html_writer;
use core\admin\table_editor;
use core\admin\table_viewer;
use surl;
use stdClass;

defined('INTERNAL') || die;

class admin extends core {


    public function main_start() {
        global $PAGE;
        $context = new stdClass();
        if ($this->_showpageheader) {
            $context->nav = $this->nav();
        } else {
            $context->nav = '';
        }
        $context->heading = $PAGE->get_heading();
        $context->session_messages = $PAGE->get_session_messages();
        $context->has_session_messages = !empty($context->session_messages);
        $context->pagetitle = $PAGE->get_title();
        $output = $this->render_from_template('admin/main_start', $context);
        return $output;

    }

    public function get_global_wrapper_layout_classes() {
        return 'd-flex flex-column';
    }

    public function page_header() {
        $context = new stdClass();
        $context->shownav = false;
        $context->navbar = $this->navbar();
        $output = $this->render_from_template('admin/page/header', $context);
        return $output;
    }

    public function page_footer() {
        global $PAGE;
        $context = new stdClass();
        $output = $this->render_from_template('admin/page/footer', $context);
        return $output;
    }


    public function nav() {
        global $DB, $CFG, $PAGE;
        $context = $PAGE->get_nav()->export_for_template($this);
        $output = $this->render_from_template('admin/page/nav', $context);
        return $output;
    }

    public function navbar() {
        global $CFG, $USER, $PAGE;
        $context = new stdClass();
        $context->isloggedin = isloggedin();
        $context->isnotloggedin = !isloggedin();
        $context->loginurl = $CFG->wwwroot . '/login/index.php';
        $context->logouturl = $CFG->wwwroot . '/login/logout.php?sessionid=' . sessionid();
        $context->username = $USER->username;
        $context->title = $CFG->title;
        $context->sitename = $CFG->sitename;
        $context->pagetitle = $PAGE->get_title();
        $output = $this->render_from_template('/page/navbar', $context);
        return $output;
    }


    public function start_page($shortname = '') {
        /*$html = html_writer::start_tag('div', array('class' => 'admin-page ' . $shortname));
        $this->pages_started++;
        return $html;*/
        return '';
    }



    public function render_site_page_editor(\core\site_page $sp, $form) {
        $context = $sp->export_for_template($this);
        $context->form = $form;
        $context->blocktypes = \core\block\block::get_all_blocktypes();
        $context->attributes = \core\site_page::get_bootstrap_css_attributes();
        return $this->render_from_template('admin/page/editor/site_page_editor', $context);
    }

    public function render_block(\core\block\block $block) {

        return $this->render_from_template($block->get_admin_template_name(), $block->export_for_template($this));
    }

    public function render_table_editor(table_editor $ate) {
        global $PAGE;
        $context = $ate->export_for_template($this);
        $output = $this->render_from_template('admin/common/table_editor', $context);
        return $output;
    }

    public function render_table_viewer(table_viewer $tv) {
        $context = $tv->export_for_template($this);
        $output = $this->render_from_template('admin/common/table_viewer', $context);
        return $output;
    }
}