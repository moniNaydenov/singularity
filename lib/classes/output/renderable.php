<?php
/**
 *
 * @package       emvade
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2018
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\output;


interface renderable {
    public function export_for_template(core $output);

    public function get_template_name(core $output);
}