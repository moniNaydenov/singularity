<?php

namespace core\output;

use \stdClass;
use \html_writer;
use \surl;

defined('INTERNAL') || die;

class core {

    protected static $_mustache;

    protected static $_output_started = false;

    protected $pages_started = 0;

    protected $_showpageheader;

    protected $_main_additional_classes = '';

    protected $_navcontext = null;

    protected $_christmas = false;

    protected $_lang_menu;

    public function __construct() {
        $time = time();
        $christmasfrom = get_config('christmas_from', 0);
        $christmasto = get_config('christmas_to', 0);
        $this->_christmas = $time >= $christmasfrom && $time <= $christmasto;
    }

    public function get_mustache() {
        global $CFG, $SITE;
        if (empty(self::$_mustache)) {
            $loaders = array(new \Mustache_Loader_FilesystemLoader($CFG->libroot . '/templates'));

            if ($SITE && file_exists($SITE->dirroot . '/templates')) {
                array_unshift($loaders, new \Mustache_Loader_FilesystemLoader($SITE->dirroot . '/templates'));
            }
            self::$_mustache =  new \Mustache_Engine(array(
                    'loader' => new \Mustache_Loader_CascadingLoader($loaders),
                    'cache' => $CFG->dataroot . '/cache/mustache/',
                    'escape' => function($v) {
                        return clean_param($v, PARAM_TEXT);
                    },
                    'entity_flags' => ENT_QUOTES | ENT_HTML5,
                    'helpers' => array(
                        'str' => function ($arg, \Mustache_LambdaHelper $helper) {

                            $arg = $helper->render($arg);
                            $args = explode(',', $arg);
                            $args = array_map('trim', $args);
                            if (count($args) > 1) {
                                return get_string($args[0], $args[1]);
                            } else {
                                return get_string($args[0]);
                            }
                        },
                        'file' => function ($arg, \Mustache_LambdaHelper $helper) {
                            $arg = $helper->render($arg);
                            $arg = clean_param($arg, PARAM_FILEHASH);
                            if (!empty($arg)) {
                                $f = \core\file::create_from_hash($arg);
                                if ($f) {
                                    return $f->get_url();
                                } else {
                                    return get_string('filenotfound') . $arg;
                                }
                            }
                            return '';
                        },
                        'surl' => function($arg, \Mustache_LambdaHelper $helper) {
                            $arg = $helper->render($arg);
                            $url = new surl($arg);
                            return $url->out();
                        },
                        'pix' => function($arg, \Mustache_LambdaHelper $helper) {
                            global $PAGE;
                            $output = $PAGE->get_output();
                            $arg = $helper->render($arg);
                            $args = explode(',', $arg);
                            $args = array_map('trim', $args);
                            if (count($args) > 1) {
                                return $output->pix_url($args[0], $args[1]);
                            }
                            return $output->pix_url($arg);

                        },
                        'imgurl' => function($arg, \Mustache_LambdaHelper $helper) {
                            global $PAGE;
                            $output = $PAGE->get_output();
                            $arg = $helper->render($arg);
                            $args = explode(',', $arg);
                            $args = array_map('trim', $args);
                            if (count($args) > 1) {
                                return $output->pix_url($args[0], $args[1]);
                            }
                            return $output->pix_url($arg);

                        },
                        'js' => function($arg, \Mustache_LambdaHelper $helper) {
                            global $PAGE;
                            if ($PAGE->js_sent) {
                                raise_exception('Javascript already sent to server! Cannot use {{#js}}...{{/js}} helper anymore!');
                            }
                            $PAGE->add_inline_js(array(), $helper->render($arg));
                            return '';
                        },
                        'fa' => function($args, \Mustache_LambdaHelper $helper) { // FontAwesome 4 helper
                            $args = $helper->render($args);
                            $args = explode(',', $args);
                            $class = '';
                            foreach ($args as $arg) {
                                $class .= ' fa-' . trim($arg);
                            }

                            return '<i class="fa ' . $class . '" aria-hidden="true"></i>';
                        },
                        'far' => function($args, \Mustache_LambdaHelper $helper) {  // FontAwesome 5 regular helper
                            $args = $helper->render($args);
                            $args = explode(',', $args);
                            $class = '';
                            foreach ($args as $arg) {
                                $class .= ' fa-' . trim($arg);
                            }

                            return '<i class="far ' . $class . '" aria-hidden="true"></i>';
                        },
                        'fas' => function($args, \Mustache_LambdaHelper $helper) { // FontAwesome 5 solid helper
                            $args = $helper->render($args);
                            $args = explode(',', $args);
                            $class = '';
                            foreach ($args as $arg) {
                                $class .= ' fa-' . trim($arg);
                            }

                            return '<i class="fas ' . $class . '" aria-hidden="true"></i>';
                        },
                        'fab' => function($args, \Mustache_LambdaHelper $helper) { // FontAwesome 5 brands helper
                            $args = $helper->render($args);
                            $args = explode(',', $args);
                            $class = '';
                            foreach ($args as $arg) {
                                $class .= ' fa-' . trim($arg);
                            }

                            return '<i class="fab ' . $class . '" aria-hidden="true"></i>';
                        },
                        'time' => function($args, \Mustache_LambdaHelper $helper) {
                            $args = explode(',', $args);
                            $format = $args[0];
                            if (count($args) > 1) {
                                $args[1] = $helper->render($args[1]);
                                $time = intval($args[1]);
                            } else {
                                $time = time();
                            }

                            return strftime($format, $time);
                        },
                        'strftime' => function($args, \Mustache_LambdaHelper $helper) {
                            $args = explode(',', $args);
                            $format = $args[0];
                            if (count($args) > 1) {
                                $args[1] = $helper->render($args[1]);
                                $time = intval($args[1]);
                            } else {
                                $time = time();
                            }

                            return strftime($format, $time);
                        },
                        'striptags' => function($args, \Mustache_LambdaHelper $helper) {
                            $args = $helper->render($args);
                            return strip_tags($args);
                        },
                        'block' => function($args, \Mustache_LambdaHelper $helper) {
                            global $CFG, $PAGE;
                            //$args = $helper->render($args);
                            return \core\block\block::parse_allblocks_html($args, $CFG->lang, $PAGE->get_output());
                        },
                        'if' => function($args, \Mustache_LambdaHelper $helper) {
                            $args = $helper($args);
                            $args = explode(',', $args);
                            $args = array_map('trim', $args);
                            if (count($args) != 4) {
                                return implode(',', $args) . '#cmp expects exactly 4 parameters (operator, val1, val2, result)';
                            }
                            list($op, $val1, $val2, $true) = $args;
                            switch ($op) {
                                case '==':
                                    return $val1 == $val2 ? $true : '';
                                case '!=':
                                    return $val1 != $val2 ? $true : '';
                                case '>':
                                    return $val1 > $val2 ? $true : '';
                                case '<':
                                    return $val1 > $val2 ? $true : '';
                                case '>=':
                                    return $val1 > $val2 ? $true : '';
                                case '<=':
                                    return $val1 > $val2 ? $true : '';
                            }
                            raise_warning('Unknown operator in ' .  implode(', ', $args));
                            return implode(',', $args);
                        }
                    )
                )
            );
        }
        return self::$_mustache;
    }

    public function render_from_template($templatename, $context = array()) {
        return $this->get_mustache()->render($templatename, $context);
    }

    public function render(renderable $obj) {
        return $this->render_from_template($obj->get_template_name($this), $obj->export_for_template($this));
    }


    public function header($showpageheader = true) {
        global $PAGE, $CFG;
        if (self::$_output_started) {
            raise_exception('OUTPUT ALREADY STARTED');
            return '';
        }
        $this->_showpageheader = $showpageheader;
        $context = new stdClass();
        $context->lang = $CFG->lang;
        $titleappend = '';
        if (!empty($CFG->title)) {
            $titleappend = $CFG->title;
        }

        $context->christmas = $this->_christmas;
        if ($this->_christmas) {
            $PAGE->add_bodyclass('christmas');
        }

        $context->title = strip_tags($PAGE->title . $titleappend);
        $context->css = $PAGE->get_css();
        $context->meta = $PAGE->get_meta();
        $context->favicon = $CFG->wwwroot . $CFG->favicon;
        $context->bodyclasses = $PAGE->get_bodyclasses();
        $context->metadescription = $PAGE->metadescription;
        $context->webmanifest = $PAGE->get_webmanifest();
        $context->canonicalurl = $PAGE->url->out();

        $context->showheader = $showpageheader;
        if ($showpageheader) {
            $context->page_header = $this->page_header();
        } else {
            $context->page_header = '';
        }

        if (!isset($CFG->cookieconsent) || $CFG->cookieconsent != 'agree') {
            $context->cookieconsentshow = get_config('cookieconsentshow', false);
            $context->cookieconsentcontent = get_config('cookieconsetcontent', '');
        }


        $context->global_wrapper_classes = $this->get_global_wrapper_layout_classes();

        $context->main_start = $this->main_start();
        $context->page = $PAGE;


        $output = $this->render_from_template('header', $context);

        self::$_output_started = true;
        return $output;
    }

    public function main_start() {
        global $PAGE;
        $context = new stdClass();
        $context->session_messages = $PAGE->get_session_messages();
        $context->has_session_messages = !empty($context->session_messages);
        $context->heading = $PAGE->get_heading();
        $context->additional_classes = $this->get_main_additional_classes();
        $output = $this->render_from_template('main_start', $context);
        return $output;
    }

    public function get_main_additional_classes() {
        return $this->_main_additional_classes;
    }

    public function set_main_additional_classes($main_additional_classes) {
        $this->_main_additional_classes = $main_additional_classes;
    }

    public function main_end() {
        global $PAGE;
        $context = new stdClass();
        $output = $this->render_from_template('main_end', $context);
        return $output;
    }

    public function get_global_wrapper_layout_classes() {
        return 'd-flex flex-column container';
    }

    public function footer($showfooter = true) {
        global $PAGE, $CFG;

        $context = new stdClass();
        $context->showfooter = $showfooter;
        $context->main_end = $this->main_end();
        if ($showfooter) {
            $context->page_footer = $this->page_footer();
        } else {
            $context->page_footer = '';
        }
        $context->page = $PAGE;
        $context->performanceinfo = $this->performanceinfo();
        $output = $this->render_from_template('footer', $context);
        return $output;
    }


    public function get_scss() {
        $files = array(
            '/scss/singularity.scss'
        );
        return $files;
    }

    public function nav() {
        global $DB, $CFG, $PAGE;
        $PAGE->get_nav()->fetch_children();
        $this->_navcontext = $PAGE->get_nav()->export_for_template($this);
        $context = $this->_navcontext;
        $context->lang_menu = $this->lang_menu();
        $output = $this->render_from_template('page/nav', $context);
        return $output;
    }


    public function nav_responsive() {
        $context = $this->_navcontext;
        $context->lang_menu = $this->lang_menu();
        $output = $this->render_from_template('page/nav_responsive', $context);
        return $output;
    }

    public function page_header() {
        global $PAGE, $CFG;
        $context = new stdClass();
        $context->nav = $this->nav();
        $context->nav_responsive = $this->nav_responsive();
        $context->lang_menu = $this->lang_menu();
        $context->home_url = $CFG->wwwroot;
        $context->title = $PAGE->title;
        $context->breadcrumb = $this->render_breadcrumb();
        $output = $this->render_from_template('page/header', $context);
        return $output;
    }

    public function render_breadcrumb() {
        global $PAGE;
        $context = new stdClass;
        $context->breadcrumb = $PAGE->get_breadcrumb();
        if (count($context->breadcrumb) == 1) {
            return '';
        }
        return $this->render_from_template('page/breadcrumb', $context);
    }

    public function page_footer() {
        global $PAGE;
        $context = new stdClass();
        $PAGE->get_nav()->fetch_children();
        $context->nav = $PAGE->get_nav()->export_for_template($this);
        $output = $this->render_from_template('page/footer', $context);
        return $output;
    }


    public function pix_url($path, $site = '', $usefileextension = false, $addrevision = true) {
        // raise_warning('Deprecated function pix_url! Please use imgurl instead!');
        return pix_url($path, $site, $usefileextension, $addrevision);
    }

    public function imgurl($path, $site = '', $usefileextension = false, $addrevision = true) {
        return imgurl($path, $site, $usefileextension, $addrevision);
    }

    public function lang_menu() {
        global $CFG, $PAGE;

        if (!empty($this->_lang_menu)) {
            return $this->_lang_menu;
        }
        $url = $PAGE->url;
        if (empty($url)) {
            $url = new surl('/');
        }

        $context = new stdClass;
        $context->languages = array();

        foreach ($CFG->languages as $lang) {
            if ($lang == $CFG->lang) {
                continue;
            }
            $url->add_param('lang', $lang);
            $language = new stdClass;
            $language->code = $lang;
            $language->url = $url->out();
            $context->languages[] = $language;
        }
        $this->_lang_menu = $this->render_from_template('page/lang_menu', $context);
        return $this->_lang_menu;
    }


    public function start_page($shortname = '') {
        /*$html = html_writer::start_tag('div', array('class' => 'col p-3 ' . $shortname));
        $this->pages_started++;
        return $html;*/
        return '';
    }

    public function end_page() {
        /*if ($this->pages_started > 0) {
            $this->pages_started--;
            return html_writer::end_tag('div');
        }*/
        return '';
    }

    public function is_output_started() {
        return self::$_output_started;
    }

    public function render_site_page(\core\site_page $sp) {
        $context = $sp->export_for_template($this);
        return $this->render_from_template('page/site_page', $context);
    }

    public function render_block(\core\block\block $block) {
        return $this->render_from_template($block->get_template_name(), $block->export_for_template($this));
    }

    public function render_news_page($context) {

        return $this->render_from_template('page/news', $context);
    }


    public function render_messagebox($message, $messagetype='info', $jsdismissable = false) {
        $context = new stdClass();
        $context->messagetext = $message;
        $context->messagetype = $messagetype;
        $context->jsdismissable = $jsdismissable;
        $context->notjsdismissable = !$jsdismissable;
        return $this->render_from_template('common/message', $context);
    }

    public function performanceinfo() {
        global $CFG;
        $performanceinfoshow = get_config('performanceinfoshow', false);
        if (is_loggedin() && $performanceinfoshow) {
            $context = new stdClass;
            $context->peakmemory = memory_get_peak_usage();
            $context->peakmemory_human = human_filesize($context->peakmemory);
            $context->totalscripttime = microtime(true) - $CFG->scriptstarttime;
            return $this->render_from_template('performanceinfo', $context);
        }
        return '';
    }
}