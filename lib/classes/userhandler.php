<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 5/27/15
 * Time: 1:14 PM
 */

namespace core;

use security;

defined('INTERNAL') || die();


class userhandler {
    private $_user;

    private static $_instance = null;

    public function __construct($user) {
        $this->_user = $user;
        $this->load_user_roles_and_permissions(true);
    }

    public function login($password) {
        global $DB;

        $password = clean_param($password, PARAM_PASSWORD);

        if (empty($password)) {
            return false;
        }


        if (empty($this->_user->password)) {
            return false;
        }

        if (password_verify($password, $this->_user->password) == true) {
            $this->_user->lastlogin = time();
            $this->_user->lastip = security::get_user_ip();
            $DB->update_record('user', $this->_user);
            unset($this->_user->password);
            $GLOBALS['USER'] = $this->_user;
            security::regenerate_sessionid();
            $_SESSION['loggedin'] = true;
            $_SESSION['userid'] = $this->_user->id;
            add_to_log('user logged in');
            $this->load_user_roles_and_permissions(true);
            return true;
        }


        add_to_log('user tried to log in - wrong password', '', '', '', security::LOG_SEVERITY_WARNING);

        return false;
    }

    public function load_user_roles_and_permissions($forcereload = false) {
        global $DB, $CFG;
        if (empty($this->_user->roles) || $forcereload == true) {
            $this->_user->roles = $DB->get_records('user_role', ['userid' => $this->_user->id], 'role, activeafter, activebefore');
            $this->_user->permissions = [];
            foreach ($this->_user->roles as $userrole) {
                if ($userrole->activeafter > time()) {
                    continue;
                }
                if ($userrole->activebefore > 0 && $userrole->activebefore < time()) {
                    continue;
                }
                if (isset($CFG->roles[$userrole->role])) {
                    $this->_user->permissions = array_merge($this->_user->permissions, $CFG->roles[$userrole->role]['permissions']);
                }
            }
        }
    }

    public function has_role($rolename) {
        if (!empty($this->_user->roles) && isset($this->_user->roles[$rolename])) {
            return true;
        }
        return false;
    }

    public function has_permission($permission) {
        global $CFG;
        if (!isset($CFG->permissions[$permission])) {
            raise_exception('No such permission exists - ' . $permission);
        }
        if (!empty($this->_user->permissions) && in_array($permission, $this->_user->permissions)) {
            return true;
        }
        return false;
    }

    public function logout() {
        if (!isloggedin()) {
            return ;
        }
        unset($_SESSION['sessionid']);
        unset($_SESSION['loggedin']);
        unset($_SESSION['userid']);
        session_destroy();
    }

    public function change_password($oldpass, $newpass, $newpass2) {
        global $DB;
        if (!isloggedin()) {
            return false;
        }

        if (empty($oldpass) || empty($newpass)) {

            return false;
        }

        $tempuser = $DB->get_record('user', array('id' => $this->_user->id), 'id, password');


        if (strcmp($newpass, $newpass2) !== 0) {

            return false;
        }

        if (password_verify($oldpass, $tempuser->password) == true) {
            $tempuser->password = security::encrypt_password($newpass);

            $DB->update_record('user', $tempuser);
            return true;
        }

        return false;
    }

    public static function instance_from_username($username) {
        global $DB;

        $username = clean_param($username, PARAM_USERNAME);

        $user = $DB->get_record('user', array('username' => $username));

        if (empty($user)) {
            return false;
        }
        return new self($user);
    }

    public static function instance_from_USER() {
        global $USER;
        if (empty(self::$_instance)) {
            self::$_instance = new self($USER);
        }
        return self::$_instance;
    }

    public static function try_login($username, $password) {
        $userhandler = self::instance_from_username($username);

        if (empty($userhandler)) {
            return false;
        }
        return $userhandler->login($password);
    }

    public static function preload_roles_and_permissions() {
        global $CFG;
        $roles = [];
        $permissions = [];
        require($CFG->libroot . '/permissions.php');
        require($CFG->libroot . '/roles.php');
        $CFG->roles = $roles;
        foreach ($permissions as $permission) {
            $CFG->permissions[$permission['name']] = $permission;
        }

        // TODO implement cache for roles and permissions
        if (file_exists($CFG->dirroot . '/s/lib/permissions.php')) {
            require($CFG->dirroot . '/s/lib/permissions.php');
            foreach ($permissions as $permission) {
                $CFG->permissions[$permission['name']] = $permission;
            }
        }

        if (file_exists($CFG->dirroot . '/s/lib/roles.php')) {
            require($CFG->dirroot . '/s/lib/roles.php');
            $CFG->roles += $roles;
        }

    }
}
