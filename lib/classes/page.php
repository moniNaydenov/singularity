<?php

namespace core;
use \html_writer;
defined('INTERNAL') || die();

/**
 * Class page
 */
class page {
    /**
     * @var string
     */
    private $_title          = '';
    /**
     * @var string
     */
    private $_heading          = '';
    /**
     * @var array
     */
    private $_css            = array();
    /**
     * @var array
     */
    private $_js             = array();
    /**
     * @var array
     */
    private $_inlinejs             = array();
    /**
     * @var array
     */
    private $_meta           = array();
    /**
     * @var array
     */
    private $_breadcrumb     = array();
    /**
     * @var null
     */
    private $_lastbreadcrumb = null;

    private $_webmanifestpath = null;
    /**
     * @var null
     */
    private $_url                    = null;
    private $_bodyclasses            = array();
    private $_nav;
    private $_output;
    private $_pagetype;
    private $_pagetypes              = array('standard', 'admin', 'login');
    private $_metadescription        = '';
    private $_js_sent                = false;


    /**
     * page constructor.
     */
    public function __construct() {
        global $CFG;

        $this->add_breadcrumb(get_string('home'), $CFG->wwwroot . '/index.php');

        if (!empty($CFG->externalcss)) {
            foreach ($CFG->externalcss as $externalcss) {
                $this->add_external_css($externalcss);
            }
        }

        if (!empty($CFG->externaljs)) {
            foreach ($CFG->externaljs as $externaljs) {
                $this->add_external_js($externaljs);
            }
        }

        if (!empty($CFG->additionalcss)) {
            foreach ($CFG->additionalcss as $css) {
                $this->add_css('/' . $css);
            }
        }

        if (!empty($CFG->additionaljs)) {
            foreach ($CFG->additionaljs as $js) {
                $this->add_js($js);
            }
        }
        $this->_url = $CFG->wwwroot_surl;
        $this->_pagetype = reset($this->_pagetypes);
        //$this->add_standard_bodyclasses();

    }

    public function __get($name) {
        $allowed = array('heading', 'url', 'title', 'pagetype', 'metadescription', 'js_sent');
        if (in_array($name, $allowed)) {
            return $this->{'_' . $name};
        }
        raise_warning('Invalid attribute for $PAGE');
        return null;
    }

    private function magic_init_output($pagetype = '', $generic = false) {
        global $CFG;
        $outputclasses = array(
            'standard'  => 'core',
            'admin'     => 'admin',
            'login'     => 'core'
        );
        if (empty($pagetype)) {
            $pagetype = $this->_pagetype;
        }
        if (!isset($outputclasses[$pagetype])) {
            $pagetype = 'standard';
            //raise_exception('Unknown pagetype: ' . $pagetype);
        }
        $outputclass = '\\output\\' . $outputclasses[$pagetype];

        $fullclass = '\\site' . $outputclass;
        if (class_exists($fullclass)) {
            $output = new $fullclass();
            if (!$generic) {
                $this->_output = $output;
            }
            return $output;
        }

        $fullclass = '\\core' . $outputclass;
        $output = new $fullclass();
        if (!$generic) {
            $this->_output = $output;
        }
        return $output;
    }

    private function magic_init_nav($pagetype = '', $generic = false) {
        global $CFG;

        $navclasses = array(
            'standard'  => 'global_nav',
            'admin'     => 'admin_nav',
            'login'     => 'global_nav'
        );

        if (empty($pagetype)) {
            $pagetype = $this->_pagetype;
        }
        if (!isset($navclasses[$pagetype])) {
            $pagetype = 'standard';
            //raise_exception('Unknown pagetype: ' . $pagetype);
        }

        $navclass = '\\navigation\\' . $navclasses[$pagetype];

        $fullclass = '\\site' . $navclass;

        if (class_exists($fullclass)) {
            $nav = new $fullclass();
            if (!$generic) {
                $this->_nav = $nav;
            }
            return $nav;
        }

        $fullclass = '\\core' . $navclass;
        $nav = new $fullclass();
        if (!$generic) {
            $this->_nav = $nav;
        }
        return $nav;
    }

    private function add_standard_bodyclasses() {

        $this->add_bodyclass('singularity');
        $this->add_bodyclass('pagelayout-' . $this->_pagetype);
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $this->add_bodyclass(strtolower(get_browser_name($_SERVER['HTTP_USER_AGENT'])));
        }
    }


    /**
     *
     */
    public function get_bodyclasses() {
        $this->add_standard_bodyclasses();
        return array_keys($this->_bodyclasses);
    }

    public function add_bodyclass($classname) {
        $this->_bodyclasses[$classname] = 1;
    }


    /**
     * @return \core\navigation\nav
     */
    public function get_nav($pagetype = '', $generic = false) {
        if (empty($this->_nav) || $generic) {
            return $this->magic_init_nav($pagetype, $generic);
        }
        return $this->_nav;
    }

    /**
     * @return \core\output\core
     */
    public function get_output($pagetype = '', $generic = false) {
        if (empty($this->_output) || $generic) {
            return $this->magic_init_output($pagetype, $generic);
        }
        return $this->_output;
    }


    public function set_pagetype($pagetype) {
        //if (in_array($pagetype, $this->_pagetypes)) { //TODO add function for site to extend page types ;)
            $this->_pagetype = $pagetype;
       // }
    }

    public function get_pagetype() {
        return $this->_pagetype;
    }

    /**
     * @param $csspath
     */
    public function add_css($csspath) {
        global $CFG;
        $cssurl = $CFG->wwwroot .'/css.php?p=' . $csspath . '&rev=' . $CFG->revision;
        if (!in_array($cssurl, $this->_css)) {
            $this->_css[] = $cssurl;
        }
    }

    /**
     * Add a javascript that will be loaded with requirejs
     * you can add "site/" that will point to the current site/js
     * otherwise all javascript relative to $CFG->wwwroot
     *
     * @param $jspath
     */
    public function add_js($jspath) {
        global $CFG;
        $this->_js[] = $jspath;
    }

    /**
     * @param $cssurl
     */
    public function add_external_css($cssurl) {
        $this->_css[] = $cssurl;
    }

    /**
     * @param $jsurl
     */
    public function add_external_js($jsurl) {
        $this->_js[] = $jsurl;
    }


    /**
     * @param $cssurl
     */
    public function add_site_css($cssurl) {
        global $SITE;
        $this->_css[] = $cssurl;
    }

    /**
     * @param $jsurl
     */
    public function add_site_js($jsurl) {
        global $SITE;
        $this->_js[] = $jsurl;
    }

    /**
     *
     */
    public function add_inline_js($requires = array(), $js = '', $arguments = '') {
        $this->_inlinejs[] = array(
            'id' => 'inlinejs_' . strval(rand(1000,9999)),
            'requires' => $requires,
            'js' => $js,
            'args' => $arguments
        );
    }

    /**
     * @param $text
     * @param null $url
     */
    public function add_breadcrumb($titleorobj, $url = null) {
        if (is_object($titleorobj) && isset($titleorobj->title)) {
            $this->_breadcrumb[] = $titleorobj;
        } else {
            $breadcrumb = new \stdClass;
            $breadcrumb->title = $titleorobj;
            $breadcrumb->url = $url;
            $this->_breadcrumb[] = $breadcrumb;
        }
    }

    /**
     * @param $httpequiv
     * @param $content
     */
    public function add_meta_tag_httpequiv($httpequiv, $content) {
        $this->_meta[] = '    <meta http-equiv="' . $httpequiv .'" content="' . htmlspecialchars($content) . '">' . PHP_EOL;
    }

    /**
     * @param $name
     * @param $content
     */
    public function add_meta_tag_name($name, $content) {
        $this->_meta[] = '    <meta name="' . $name .'" content="' . htmlspecialchars($content) . '">' . PHP_EOL;
    }


    /**
     * @param $property
     * @param $content
     */
    public function add_meta_tag_property($property, $content) {
        $this->_meta[] = '    <meta property="' . $property .'" content="' . htmlspecialchars($content) . '">' . PHP_EOL;
    }

    /**
     * @param $url
     */
    public function set_url($url) {
        global $CFG;
        if (!($url instanceof \surl)) {
            $url = new \surl($url);
        }
        if ($url->is_suburlof($CFG->wwwroot_surl) == false) {
            raise_exception('Invalid $PAGE->url set ' . $url);
        }
        $this->_url = $url;

        $path = $url->get_relative_path($CFG->wwwroot_surl);
        if ($path == '/') {
            $this->add_bodyclass('home');
            $this->add_bodyclass('path-home');
        } else {
            $parts = explode('/', $path);
            $leveled_classes = array();
            foreach ($parts as $part) {
                if (empty($part)) {
                    continue;
                }
                if (strpos(strrev($part), 'php.') === 0) {
                    $part = substr($part, 0, -4);
                }
                $leveled_classes[] = $part;
                $this->add_bodyclass('path-' . implode('-', $leveled_classes));
            }
        }
    }

    /**
     * @param $title
     */
    public function set_title($title) {
        $this->_title = $title;
    }

    /**
     * @return string
     */
    public function get_title() {
        return $this->_title;
    }

    /**
     * @param $heading
     */
    public function set_heading($heading) {
        $this->_heading = $heading;
    }

    /**
     * @return string
     */
    public function get_heading() {
        return $this->_heading;
    }

    public function set_metadescription($description) {
        $this->_metadescription = $description;
    }

    public function set_meta_opengraph_basic_tags($title, $description = null, $type = 'website', \surl $pageurl = null, $sitename = null) {
        global $CFG;
        $this->add_meta_tag_property('og:title', $title);
        $this->add_meta_tag_property('og:type', $type);
        if (!empty($description)) {
            $this->add_meta_tag_property('og:description', $description);
        }
        if (empty($pageurl)) {
            $pageurl = $this->_url;
        }
        $this->add_meta_tag_property('og:url', $pageurl->out());
        if (empty($sitename)) {
            if ($CFG->stringmanager->has_string('sitename', 'site')) {
                $sitename = get_string('sitename', 'site');
            } else {
                $sitename = $CFG->title;
            }

        }
        $this->add_meta_tag_property('og:site_name', $sitename);
        $this->add_meta_tag_property('og:updated_time', $CFG->revision);
    }

    public function add_meta_opengraph_image_tag($imageurl, $alt = null, $mimetype = null, $width = null, $height = null) {
        $this->add_meta_tag_property('og:image', $imageurl);
        if (!empty($alt)) {
            $this->add_meta_tag_property('og:image:alt', $alt);
        }
        if (!empty($mimetype)) {
            $this->add_meta_tag_property('og:image:type', $mimetype);
        }
        if (!empty($width)) {
            $this->add_meta_tag_property('og:image:width', $width);
        }
        if (!empty($height)) {
            $this->add_meta_tag_property('og:image:height', $height);
        }

    }

    public function set_webmanifest($webmanifestpath) {
        $this->_webmanifestpath = $webmanifestpath;
    }

    public function get_webmanifest() {
        if (!empty($this->_webmanifestpath)) {
            return '    <link rel="manifest" href="' . htmlspecialchars($this->_webmanifestpath) . '">';
        }
        return '';
    }

    /**
     * @return \surl
     *
     *     */
    public function get_url() {
        return $this->_url;
    }


    public function get_css() {
        global $CFG;
        if (file_exists($CFG->dirroot . '/css/' . $this->_pagetype . '.css')) {
            $this->add_css('/css/' . $this->_pagetype . '.css');
        }

        if (file_exists('/s/css/' . $this->_pagetype . '.css')) {
            $this->add_css('/s/css/' . $this->_pagetype . '.css');
        }

        $scssfiles = $this->get_output()->get_scss();
        foreach ($scssfiles as $scss) {
            $this->add_css($scss);
        }
        return $this->_css;
    }

    public function get_js_head() {
        global $CFG, $USER;

        $sessionid = sessionid();

        $cfg = "
window.S = {};
window.S.cfg = window.CFG = {
    wwwroot: '{$CFG->wwwroot}',
    sessionid: '{$sessionid}',
    userid: '{$USER->id}',
    version: '{$CFG->revision}',
    rev: '{$CFG->revision}',
    pageurl: '{$this->_url->out()}',
    lang: '{$CFG->lang}'       
};";

        // allow script to call require() before requireJS actually has loaded
        $require = '
window.preRequire = [];
window.require = function() {
    window.preRequire.push(arguments);
};';
        if (!$CFG->debug) { //just a little bit minify
            $pattern = array("\r\n", "\n", '   ');
            $cfg = str_replace($pattern, '', $cfg);
            $require = str_replace($pattern, '', $require);
        }
        return html_writer::script($cfg . $require);
    }

    /**
     * @return string
     */
    public function get_js_footer() {
        global $CFG, $USER;
        if ($this->_js_sent) {
            raise_warning('Javascript already sent to server!');
        }

        $this->_js_sent = true;

        $revision = $CFG->revision;
        if ($CFG->debug) {
            $revision = $CFG->revision . rand(10, 99999);
        }


        $config = '
window.require = {
    baseUrl: "' . $CFG->wwwroot . '",
    urlArgs: "rev=' . $revision . '",
    map: {
        bootstrap: {
            "popper.js": "lib/popper.js/popper.min"
        }
    },
    paths: {
        admin: "js/admin",
        site: "s/js",
        ace: "lib/ace/src-min-noconflict",
        beautify: "lib/js-beautify/build/beautify.min",
        "beautify-css": "lib/js-beautify/build/beautify-css.min",
        "beautify-html": "lib/js-beautify/build/beautify-html.min",
        block: "js/block",   
        bootstrap: "lib/bootstrap4/bootstrap.min",     
        core: "js/js.php?a=1",
        ckeditor: "js/ckeditorloader",
        ckeditormain: "lib/ckeditor/ckeditor",
        ckeditoradapter: "lib/ckeditor/adapters/jquery",
        form: "js/form",
        jquery: "lib/jquery/jquery-3.3.1.min",
        hammerjs: "lib/hammerjs/hammer.min",
        mediaelement: "lib/mediaelement/build/mediaelement-and-player.min"
    },
    shim: {
        beautify: [ "jquery" ],
        "beautify-css": [ "beautify" ], 
        "beautify-html": [ "beautify"], 
        ckeditoradapter: [ "jquery", "ckeditormain" ],
        mediaelement: {deps: ["require"], exports: "MediaElementPlayer"}        
    },
};' . PHP_EOL;




        $prefix = 'require(["core"], function() {' . PHP_EOL;
        $suffix = '});' . PHP_EOL;
        
        $requirejs = "\t\t" . '<script src="' . $CFG->wwwroot . '/lib/require.js/require.js"></script>' . PHP_EOL;

        $inline = array();
        foreach ($this->_inlinejs as $inlinejs) {
            if (!empty($inlinejs['requires'])) {
                foreach ($inlinejs['requires'] as $i => $require) {
                    $inlinejs['requires'][$i] = "'$require'";
                }
                $requires = implode(', ', $inlinejs['requires']); //TODO
                $inline[] = '
            require([' . $requires . '], function(' . $inlinejs['args'] . ') {
            ' . $inlinejs['js']  . '
            });';
            } else {
                $inline[] = $inlinejs['js'];
            }
        }

        $inline = implode(PHP_EOL, $inline);


        if (!empty($this->_js)) {
            $scripts = array();

            foreach ($this->_js as $path) {
                $scripts[] = "'$path'";
            }
            $scripts = implode(', ', $scripts);

            $scripts = 'requirejs([' . $scripts . '], function() { ' . $inline . ' });' . PHP_EOL;
        } else {
            $scripts = $inline;
        }

        if (!$CFG->debug) { //just a little bit minify
            $pattern = array("\r\n", "\n", '   ');
            $config = str_replace($pattern, '', $config);
        }

        $js = html_writer::script($config) . $requirejs . html_writer::script($prefix .$scripts . $suffix);

        return $js;
    }

    /**
     * @return string
     */
    public function get_meta() {
        return implode('', $this->_meta);
    }

    /**
     * @return array
     */
    public function get_breadcrumb() {
        $last = end($this->_breadcrumb);
        //$last->active = true;
        return $this->_breadcrumb;
    }

    public function get_session_messages() {
        if (isset($_SESSION['messages']) && !empty($_SESSION['messages'])) {
            $messages = $_SESSION['messages'];
            unset($_SESSION['messages']);
            return $messages;
        }
        return array();
    }

    public function export_for_template($output) {
        $context = new \stdClass();
        $context->js = array();
        $context->css = $this->get_css();
    }
}