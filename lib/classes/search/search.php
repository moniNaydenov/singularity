<?php


namespace core\search;

use \surl;
use \stdClass;

class search {
    private $_result = [];
    private $_table = 'search_index';

    public function index_page(surl $url, $title, $lang, $content) {
        global $DB;
        $url = $url->out(false, true);
        $index = $DB->get_record($this->_table, ['url' => $url, 'lang' => $lang]);
        if (!$index) {
            $index = new stdClass;
            $index->url = $url;
            $index->lang = $lang;
        }
        $index->title = $title;
        $index->content = $content;

        if (isset($index->id)) {
            $DB->update_record($this->_table, $index);
        } else {
            $DB->insert_record($this->_table, $index);
        }
    }

    public function search($what) {
        global $DB;

        $what = preg_replace('/\s+/', ' ', trim($what));
        $terms = explode(' ', $what);
        $wheresql = [];
        foreach ($terms as $id => $term) {
            if (mb_strlen($term) < 3) { //TODO: check if not too strict..
                unset($terms[$id]);
                continue;
            }
            $wheresql[] = 'content LIKE "%' . $DB->escape_value($term) . '%"';
            $wheresql[] = 'title LIKE "%' . $DB->escape_value($term) . '%"';
        }

        if (empty($wheresql)) {
            return [];
        }

        $wheresql = implode(' OR ', $wheresql);
        $sql = <<<SQL
SELECT
    *
FROM
    {$this->_table}
WHERE
    $wheresql
SQL;
        $firstresults = $DB->get_records_sql($sql);

        foreach ($firstresults as $result) {
            $result->score = 0;
            $content = mb_strtolower($result->content);
            $title = mb_strtolower($result->title);
            foreach ($terms as $term) {
                $term = mb_strtolower($term);
                $termcount = mb_substr_count($content, $term);
                if ($termcount > 0) {
                    $result->score += 100 + $termcount;
                }
                $termcount = mb_substr_count($title, $term);
                if ($termcount > 0) {
                    $result->score += 200 + $termcount;
                }
                $result->content = preg_replace('/(' . $term . ')/iu', '<strong>$1</strong>', $result->content);
                $result->trimmedcontent = mb_substr($result->content, mb_strpos($result->content, '<strong>'), 256); //TODO: improve!
                $result->trimmedcontent = '...' . mb_substr($result->trimmedcontent, 0, mb_strrpos($result->trimmedcontent, ' ')) . '...';
            }
        }
        usort($firstresults, function($a, $b) {
            if ($a->score < $b->score) {
                return 1;
            }
            if ($a->score > $b->score) {
                return -1;
            }
            return 0;
        });
        $i = 1;
        foreach ($firstresults as $result) {
            $result->id = $i;
            $i++;
        }
        $this->_result = array_values($firstresults);
        return $this->_result;
    }

    public function export_for_template(\core\output\core $output) {
        $context = new stdClass;
        $context->results = array_values($this->_result);
        $context->empty = empty($this->_result);
        $context->total = count($this->_result);
        return $context;
    }

    public static function instance() {
        return new self();
    }
}