<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

class htmleditor extends form_field {
    private $width;
    private $height;
    private $resize;

    public function __construct($name, $defaultvalue, $label, $width = 420, $height = 300, $fieldtype=PARAM_RAW) {
        parent::__construct($name, $name, $defaultvalue, $label, $fieldtype);
        $this->width = $width;
        $this->height = $height;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        return $output->render_from_template('form/field/htmleditor', $context);
    }
}

