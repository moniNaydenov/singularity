<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

defined('INTERNAL') || die;


class textarea extends form_field {
    private $_rows;
    private $_cols;
    public function __construct($name, $defaultvalue, $label, $rows = 6, $cols = 40, $fieldtype=PARAM_TEXT) {
        parent::__construct($name, $name, $defaultvalue, $label, $fieldtype);
        $this->_rows = $rows;
        $this->_cols = $cols;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->rows = $this->_rows;
        $context->cols = $this->_cols;

        return $output->render_from_template('form/field/textarea', $context);
    }
}

