<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */
namespace core\form_field;

defined('INTERNAL') || die;

class checkbox extends form_field {
    private $_label_after;
    private $_checked;
    private $_ingroup;

    public function __construct($name, $defaultvalue, $label_before, $label_after, $checked, $fieldtype, $ingroup = false) {
        if ($ingroup) {
            $id = $name . '_' . strval(rand(1000, 9999));
            $name = $name . '[]';
        } else {
            $id = $name;
        }
        parent::__construct($name, $id, $defaultvalue, $label_before, $fieldtype);
        $this->_label_after = $label_after;
        $this->_checked = $checked;
        $this->_ingroup = $ingroup;
    }

    public function set_value($value) {
        $value = clean_param($value, $this->fieldtype);
        if ($value == $this->value) {
            $this->_checked = true;
        } else {
            $this->_checked = false;
        }
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->checked = $this->_checked;
        $context->label_after = $this->_label_after;
        $context->ingroup = $this->_ingroup;

        return $output->render_from_template('form/field/checkbox', $context);
    }
}