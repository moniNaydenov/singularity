<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;

defined('INTERNAL') || die;

class codeeditor extends form_field {
    private $_language;

    public function __construct($name, $defaultvalue, $label, $language = 'javascript') {
        parent::__construct($name, $name, $defaultvalue, $label, PARAM_RAW);
        $this->_language = $language;
    }


    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->language = $this->_language;

        return $output->render_from_template('form/field/codeeditor', $context);
    }
}

