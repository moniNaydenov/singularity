<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

class fileupload extends form_field {
    private $_allowedfiletypes = '*';
    public function __construct($name, $label, $allowedfiletypes = '*') {
        parent::__construct($name, $name, null, $label, PARAM_RAW);
        $this->_allowedfiletypes = $allowedfiletypes;
    }


    public function get_data() {

        if (isset($_FILES[$this->name])) {
            if ($_FILES[$this->name]['error'] == UPLOAD_ERR_OK) {
                $r = new stdClass;
                $validfiletype = true;
                if ($this->_allowedfiletypes != '*')  {
                    $filetypes = explode(',', $this->_allowedfiletypes);
                    $pathinfo = pathinfo($_FILES[$this->name]['name']);
                    $validfiletype = false;
                    foreach ($filetypes as $filetype) {
                        $filetype = trim($filetype);
                        $filetype = str_replace('.', '', $filetype);
                        if ($pathinfo['extension'] == $filetype) {
                            $validfiletype = true;
                            break;
                        }
                    }
                }
                $r->errorcode = $_FILES[$this->name]['error'];
                if ($validfiletype) {
                    $r->error = false;
                    $r->tmpfilename = $_FILES[$this->name]['tmp_name'];
                    $r->filename = $_FILES[$this->name]['name'];
                    $r->size = $_FILES[$this->name]['size'];
                    $r->success = true;
                } else {
                    $r->error = 'Invalid file type';
                    $r->success = false;
                    unlink($_FILES[$this->name]['tmp_name']);
                }
            } else {
                $r = new stdClass;
                switch ($_FILES[$this->name]['error']) {
                    case UPLOAD_ERR_INI_SIZE:
                        $r->error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
                        break;
                    case UPLOAD_ERR_FORM_SIZE:
                        $r->error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
                        break;
                    case UPLOAD_ERR_PARTIAL:
                        $r->error = 'The uploaded file was only partially uploaded.';
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        $r->error = 'No file was uploaded.';
                        break;
                    case UPLOAD_ERR_NO_TMP_DIR:
                        $r->error = 'Missing a temporary folder.';
                        break;
                    case UPLOAD_ERR_CANT_WRITE:
                        $r->error = 'Failed to write file to disk.';
                        break;
                    case UPLOAD_ERR_EXTENSION:
                        $r->error = 'A PHP extension stopped the file upload.';
                        break;
                    default:
                        $r->error = 'Unknown error occurred!';
                }
                $r->errorcode = $_FILES[$this->name]['error'];
                $r->success = false;
            }
            return $r;
        }
        return false;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->allowedfiletypes = $this->_allowedfiletypes;

        return $output->render_from_template('form/field/fileupload', $context);
    }
}