<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

class hidden extends form_field {
    public function __construct($name, $value, $fieldtype) {
        parent::__construct($name, $name, $value, null, $fieldtype);
    }

    public function hidden_render() {
        return html_writer::empty_tag('input', array(
            'type' => 'hidden',
            'id' => $this->id,
            'name' => $this->name,
            'value' => $this->value
        ));
    }

    public function is_hidden() {
        return true;
    }
}
