<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

defined('INTERNAL') || die;

class datetime extends form_field {
    private $_type;
    const DATEONLY = 1;
    const TIMEONLY = 2;
    const DATEANDTIME = 3;
    public function __construct($name, $defaultvalue, $label, $type = self::DATEONLY) {
        parent::__construct($name, $name, $defaultvalue, $label, PARAM_INT);
        $this->_type = $type;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);

        $context->years = range(1972, 2030);
        $context->days = range(1, 31);

        $context->months = array();
        for ($i = 1; $i <= 12; $i++) {
            $context->months[] = array('v' => $i, 't' => strftime('%B', (($i-1)*32*24*60*60)));
        }
        $context->hours = range(0, 23);
        $context->minutes = range(0, 59);
        $context->showdate = $this->_type == self::DATEONLY || $this->_type == self::DATEANDTIME;
        $context->showtime = $this->_type == self::TIMEONLY || $this->_type == self::DATEANDTIME;

        if (empty($this->value)) {
            $this->value = time();
        }

        $di = getdate($this->value);
        $context->year = $di['year'];
        $context->month = $di['mon'];
        $context->day = $di['mday'];
        $context->minute = $di['minutes'];
        $context->hour = $di['hours'];

        return $output->render_from_template('form/field/datetime', $context);
    }

    public function get_data() {
        $year = optional_param($this->name . '_year', 0, PARAM_INT); // TODO: fix for time only
        $day = optional_param($this->name . '_day', 0, PARAM_INT);
        $mon = optional_param($this->name . '_month', 0, PARAM_INT);
        $hour = optional_param($this->name . '_hour', date("H"), PARAM_INT);
        $minute = optional_param($this->name . '_minute', date("i"), PARAM_INT);
        $ret = mktime($hour, $minute, 0, $mon, $day, $year);
        return $ret;
    }
}