<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */


namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

require_once(__DIR__ . '/dropdown.php');

class lang extends dropdown {

    public function __construct($name, $defaultvalue, $label) {
        global $CFG;
        $options = array();

        foreach ($CFG->languages as $lang) {
            $options[$lang] = $lang;
        }
        parent::__construct($name, $defaultvalue, $options, $label);
    }
}