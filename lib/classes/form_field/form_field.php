<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

class form_field {
    protected $name;
    protected $id;
    protected $cleanname;
    protected $cleanid;
    protected $value;
    protected $label;
    protected $fieldtype;
    protected $_form;

    public function __construct($name, $id, $defaultvalue, $label, $fieldtype) {
        $this->name = $name;// '__formfield_' . $name;
        $this->id = 'field_' . $id; //'__formfield_' . $id;
        $this->cleanname = $name;
        $this->cleanid = $id;
        $this->value = $defaultvalue;
        $this->label = $label;
        $this->fieldtype = $fieldtype;
    }

    public function render() {
        return false;
    }

    public function export_for_template(\core\output\core $output) {
        $context = new stdClass;
        $context->id = $this->id;
        $context->name = $this->name;
        $context->value = $this->value;
        $context->label = $this->label;
        if (!is_null($this->_form) || true) {
            $context->label_width = $this->_form->get_label_width();
            $context->field_width = $this->_form->get_field_width();
        } else {
            $context->label_width = '3';
            $context->field_width = '9';
        }
        return $context;
    }

    public function render_from_template(\core\output\core $output) {
        return '';
    }

    public function hidden_render() {
        return '';
    }

    public function get_data() {
        $p = optional_param($this->name, null, $this->fieldtype);
        return $p;
    }

    public function set_value($value) {
        $this->value = clean_param($value, $this->fieldtype);
    }

    public function get_label() {
        return $this->label;
    }

    public function get_name() {
        return $this->cleanname;
    }

    public function get_id() {
        return $this->cleanid;
    }

    public function set_form(\core\form $form) {
        $this->_form = $form;
    }
}