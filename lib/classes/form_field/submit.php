<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

defined('INTERNAL') || die;

class submit extends form_field {

    private $submit_label;
    private $submit_name;
    private $cancel_label;
    private $cancel_name;
    private $apply_label;
    private $apply_name;


    public function __construct($name, $submit_label, $cancel_label = null, $apply_label = null) {
        parent::__construct($name, $name, '', null, PARAM_TEXT);
        $this->submit_label = $submit_label;
        $this->cancel_label = $cancel_label;
        $this->apply_label = $apply_label;
        $this->submit_name = '__formsubmit__' . $name;
        $this->cancel_name = '__formcancel__' . $name;
        $this->apply_name = '__formapply__' . $name;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->cancel_label = $this->cancel_label;
        $context->cancel_name = $this->cancel_name;
        $context->submit_label = $this->submit_label;
        $context->submit_name = $this->submit_name;
        $context->apply_label = $this->apply_label;
        $context->apply_name = $this->apply_name;
        return $output->render_from_template('form/field/submit', $context);
    }

    public function set_label_submit($label) {
        $this->submit_label = $label;
    }

    public function set_label_cancel($label) {
        $this->cancel_label = $label;
    }

    public function set_label_apply($label) {
        $this->apply_label = $label;
    }

    public function get_data() {
        $submit = optional_param($this->submit_name, false, PARAM_TEXT);
        $apply = optional_param($this->apply_name, false, PARAM_TEXT);
        $cancel = optional_param($this->cancel_name, false, PARAM_TEXT);

        if ($submit != false) {
            return \core\form::FORM_SUBMITTED;
        }
        if ($apply != false) {
            return \core\form::FORM_APPLIED;
        }
        if ($cancel != false) {
            return \core\form::FORM_CANCELED;
        }
        return null;
    }
}