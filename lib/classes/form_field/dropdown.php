<?php

// Singularity is an intelligent, object-oriented and secure php platform
// Copyright (C) 2016 Simeon Naydenov
//
// Singularity is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Singularity is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Singularity.  If not, see <http://www.gnu.org/licenses/>.

/**
 * form field class
 */

namespace core\form_field;

use html_writer;

defined('INTERNAL') || die;

class dropdown extends form_field {
    protected $_options;
    protected $_multiple;

    public function __construct($name, $defaultvalue, $options, $label, $multiple = false) {
        parent::__construct($name, $name, $defaultvalue, $label, PARAM_RAW);
        $this->_options = $options;
        $this->_multiple = $multiple;
    }

    public function add_option($value, $label) {
        $this->_options[$value] = $label;
    }

    public function remove_option($value) {
        if (isset($this->_options[$value])) {
            unset($this->_options[$value]);
        }
    }


    public function get_data() {
        if ($this->_multiple) {
            $v = optional_param_array($this->name, [], $this->fieldtype);
            $ret = [];
            foreach ($v as $value) {
                if (isset($this->_options[$value])) {
                    $ret[] = $value;
                }
            }
            return $ret;
        } else {
            $value = parent::get_data();
            if (isset($this->_options[$value])) {
                return $value;
            }
        }
        return null;
    }


    public function remove_all_options() {
        $this->_options = array();
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);

        $context->options = array();
        foreach ($this->_options as $value => $label) {
            $selected = $this->value == $value;
            $context->options[] = array('value' => $value, 'label' => $label, 'selected' => $selected);
        }
        $context->multiple = $this->_multiple;
        return $output->render_from_template('form/field/dropdown', $context);
    }
}