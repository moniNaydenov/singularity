<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\form_field;


defined('INTERNAL') || die;

class file extends form_field {
    private $_file;

    public function __construct($name, $label) {
        parent::__construct($name, $name, null, $label, PARAM_RAW);

    }

    public function get_data() {
        $hash = parent::get_data();
        if (!$hash) {
            return null;
        }
        $file = \core\file::create_from_hash($hash);
        if (!$file) {
            return null;
        }
        return $file;
    }

    public function set_value($value) {

        $value = clean_param($value, $this->fieldtype);
        if (is_string($value)) {
            $this->_file = \core\file::create_from_hash($value);
        } else if ($value instanceof \core\file) {
            $this->_file = $value;
        }
        if ($this->_file) {
            $this->value = $this->_file->get_uniquehash();
        }
    }

    public function export_for_template(\core\output\core $output) {
        $context = parent::export_for_template($output);
        if (!empty($this->_file)) {
            $context->filename = $this->_file->get_filename();
            $context->fileurl = $this->_file->get_url();
            $parent = $this->_file->get_parent();
            if ($parent) {
                $context->parent = $parent->get_uniquehash();
            } else {
                $context->parent = false;
            }
        }
        return $context;
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        return $output->render_from_template('form/field/file', $context);
    }


}