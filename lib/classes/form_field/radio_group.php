<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\form_field;


defined('INTERNAL') || die;

class radio_group extends form_field {
    private $_radios;
    private $_values;

    public function __construct($name, $defaultvalues, $label, $checkedid = false) {
        parent::__construct($name, $name, null, $label, PARAM_RAW);
        $this->_radios = array();
        $this->_values = $defaultvalues;

        $i = 0;
        foreach ($defaultvalues as $v => $l) {
            $this->_radios[] = new radio($this->name, $v, '', $l, ($checkedid == $v), $this->fieldtype, true);
            $i++;
        }
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->radios = '';
        foreach ($this->_radios as $radio) {
            $context->radios .= $radio->render_from_template($output);
        }
        return $output->render_from_template('form/field/radio_group', $context);
    }


    public function get_data() {

        $p = optional_param($this->name, null, $this->fieldtype);
        if (isset($this->_values[$p])) {
            return $p;
        }
        return null;
    }

    public function set_form(\core\form $form) {
        parent::set_form($form);
        foreach ($this->_radios as $radio) {
            $radio->set_form($form);
        }
    }

    public function get_values() {
        return $this->_values;
    }

}