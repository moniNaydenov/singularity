<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2018
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\form_field;


class image extends file {
    protected $_viewonly = false;
    public function __construct($name, $label, $viewonly = false) {
        parent::__construct($name, $label);
        $this->_viewonly = $viewonly;
    }

    public function export_for_template(\core\output\core $output) {
        $context = parent::export_for_template($output);
        $context->hasthumbnail = true;
        $context->viewonly = $this->_viewonly;


        return $context;
    }
}