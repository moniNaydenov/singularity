<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\form_field;


defined('INTERNAL') || die;

class checkbox_group extends form_field {
    private $_checkboxes;
    private $_values;

    public function __construct($name, $defaultvalues, $label, $checkedids = []) {
        parent::__construct($name, $name, null, $label, PARAM_RAW);
        $this->_checkboxes = array();
        $this->_values = $defaultvalues;

        $i = 0;
        foreach ($defaultvalues as $v => $l) {
            $this->_checkboxes[$v] = new checkbox($this->name, $v, '', $l, in_array($v, $checkedids), $this->fieldtype, true);
            $i++;
        }
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->checkboxes = '';
        foreach ($this->_checkboxes as $checkbox) {
            $context->checkboxes .= $checkbox->render_from_template($output);
        }
        return $output->render_from_template('form/field/checkbox_group', $context);
    }


    public function set_value($values) {
        if (is_array($values)) {
            foreach ($values as $v) {
                if (isset($this->_checkboxes[$v])) {
                    $this->_checkboxes[$v]->set_value($v);
                }
            }
        }
    }

    public function get_data() {

        $p = optional_param_array($this->name, [], $this->fieldtype);
        $ret = array();
        foreach ($p as $value) {
            if (isset($this->_values[$value])) {
                $ret[] = $value;
            }
        }
        return $ret;
    }

    public function set_form(\core\form $form) {
        parent::set_form($form);
        foreach ($this->_checkboxes as $check) {
            $check->set_form($form);
        }
    }

    public function get_values() {
        return $this->_values;
    }

}