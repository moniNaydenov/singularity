<?php
/**
 *
 * @package       nia-bg
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2019
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\form_field;

use html_writer;
use stdClass;

defined('INTERNAL') || die;

class heading extends html {
    private $_size = 4;
    private static $_count = 0;
    public function __construct($content, $size = 4) {
        self::$_count++;
        $this->_size = $size;
        parent::__construct('__form_heading_' . self::$_count, $content);
    }

    public function render_from_template(\core\output\core $output) {
        $context = $this->export_for_template($output);
        $context->size = 'h' . $this->_size;

        return $output->render_from_template('form/field/heading', $context);
    }
}