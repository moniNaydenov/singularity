<?php
/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core;

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\ValueConverter;
use ScssPhp\ScssPhp\Version;
use ScssPhp\ScssPhp\Formatter\Compressed;
use ScssPhp\ScssPhp\Formatter\Expanded;

class scssserver {
    private $_compiler;
    private $_cachedir;
    private $_compileddir;

    public function __construct() {
        global $CFG;

        $this->_compileddir = $CFG->dataroot .'/cache/css/';

        $this->_compiler = new \ScssPhp\ScssPhp\Compiler();
        $this->_compiler->addImportPath($CFG->dirroot . '/scss/');
        $this->_compiler->addImportPath($CFG->dirroot . '/s/');
        $sitescsspath = $CFG->dirroot . '/s/scss/';
        if (file_exists($sitescsspath)) {
            $this->_compiler->addImportPath($sitescsspath);
        }
        $this->_compiler->addVariables($this->get_scss_variables());
        $this->_compiler->registerFunction('pix_url', function($args) {
            @list($pathargs, $siteargs) = $args;

            if (is_null($pathargs)) {
                return '';
            }

            $path = '';

            if ($pathargs[0] == 'keyword') {
                $path = $pathargs[1];
            } else if ($pathargs[0] == 'string') {
                $path = $pathargs[2][0];
            }

            $site = '';

            if (!is_null($siteargs)) {
                if ($siteargs[0] == 'keyword') {
                    $site = $siteargs[1];
                } else if ($siteargs[0] == 'string') {
                    $site = $siteargs[2][0];
                }
            }

            return ValueConverter::parseValue('url("' . pix_url($path, $site) . '")');
        }, ['pathargs', 'siteargs:site']);
        $this->_compiler->registerFunction('imgurl', function($args) {
            @list($pathargs, $siteargs) = $args;

            if (is_null($pathargs)) {
                return '';
            }

            $path = '';

            if ($pathargs[0] == 'keyword') {
                $path = $pathargs[1];
            } else if ($pathargs[0] == 'string') {
                $path = $pathargs[2][0];
            }

            $site = '';

            if (!is_null($siteargs)) {
                if ($siteargs[0] == 'keyword') {
                    $site = $siteargs[1];
                } else if ($siteargs[0] == 'string') {
                    $site = $siteargs[2][0];
                }
            }

            return ValueConverter::parseValue('url("' . pix_url($path, $site) . '")');
        }, ['pathargs', 'siteargs:site']);
        $this->_compiler->registerFunction('surl', function($args) {
            @list($urlargs) = $args;

            $url = '/';

            if (!is_null($urlargs)) {
                if ($urlargs[0] == 'keyword') {
                    $url = $urlargs[1];
                } else if ($urlargs[0] == 'string') {
                    $url = $urlargs[2][0];
                }
            }
            $surl = new \surl($url);
            return  ValueConverter::parseValue('"' . $surl->out() . '"');
        }, ['args']);
        if ($CFG->debug) {
            $this->_compiler->setOutputStyle(\ScssPhp\ScssPhp\OutputStyle::EXPANDED);
        } else {
            $this->_compiler->setOutputStyle(\ScssPhp\ScssPhp\OutputStyle::COMPRESSED); // tested with Crunched (without comments) - too small difference in produced css size
        }
    }

    public function get_compiled_css($filepath, $force = false) {
        global $CFG;
        $fullpath = $CFG->dirroot . $filepath;
        if (file_exists($fullpath) && is_file($fullpath)) {
            $cachedname = sha1($fullpath . '-rev-' . $CFG->revision);
            $cachedfullpath = $this->_compileddir . $cachedname;
            if (!file_exists($cachedfullpath) || $force) {
                try {
                    $compiled = $this->_compiler->compileString(file_get_contents($fullpath), $fullpath);
                    if (is_writable(file_exists($cachedfullpath) ? $cachedfullpath : dirname($cachedfullpath))) {
                        file_put_contents($cachedfullpath, $compiled->getCss());
                        return $compiled->getCss();
                    } else {
                        raise_exception($fullpath . ' not writable!');
                    }
                } catch (\Exception $e) {
                    raise_exception($e->getMessage());
                }
            } else {
                $compiled = file_get_contents($cachedfullpath);
                return $compiled;
            }
        }
        return false;
    }

    private function get_scss_variables() {
        global $CFG;

        $vars = array(
            'wwwroot' => ValueConverter::parseValue($CFG->wwwroot_surl->out())
        );
        return $vars;
    }
}