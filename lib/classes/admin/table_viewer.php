<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\admin;

use stdClass;

defined('INTERNAL') || die();

class table_viewer {
    protected $_table;
    protected $_fields;
    protected $_visiblefields;
    protected $_filters;
    protected $_sortby;
    protected $_idfieldname;
    protected $_datefields;
    protected $_perpage;
    protected $_page;


    public function __construct($table, $visiblefields, $sortby = 'id ASC', $perpage = 10) {
        global $DB;

        $this->_table = $table;

        $this->_fields = $DB->get_columns($table);

        $this->_visiblefields = array();
        foreach ($visiblefields as $visiblefield) {
            if (isset($this->_fields[$visiblefield])) {
                $this->_visiblefields[] = $visiblefield;
            }
        }

        if (!empty($sortby)) {
            $this->_sortby = $sortby;
        } else {

            $this->_sortby = 'id';
        }

        $this->_filters = array();
        $this->_datefields = array();

        $this->_perpage = $perpage;
        $this->_page = optional_param('page', 0, PARAM_INT);
    }

    public function set_page($page) {
        $this->_page = $page;
    }

    public function get_visible_fields() {
        return $this->_visiblefields;
    }

    public function set_filters($filters) {
        $this->_filters = $filters;
    }

    /**
     * @param array $datefields ['field_name => 'format']
     */
    public function set_datefields($datefields) {
        $this->_datefields = $datefields;
    }


    public function get_records() {
        global $DB;
        return $DB->get_records($this->_table,
            $this->_filters,
            '*',
            $this->_sortby,
            $this->_perpage,
            ($this->_perpage * $this->_page)
        );
    }

    public function get_records_count() {
        global $DB;
        return $DB->get_count($this->_table, 'id', $this->_filters);
    }

    public function export_for_template(\core\output\core $output) {
        global $PAGE;
        $context = new stdClass;
        $records = $this->get_records();
        $context->head = array();
        foreach ($this->_visiblefields as $field) {
            $context->head[] = get_string('admin:field:' . $field, 'admin');
        }

        $context->body = array();
        foreach ($records as $record) {
            $row = array();
            foreach ($this->_visiblefields as $field) {
                if (isset($this->_datefields[$field])) {
                    $row[] = strftime($this->_datefields[$field], $record->{$field});
                } else {
                    $row[] = $record->{$field};
                }
            }
            $context->body[] = $row;
        }

        $context->pages = array();
        $totalcount = $this->get_records_count();
        $p = 0;
        do {
            $page = new stdClass;
            $page->active = ($p == $this->_page);
            $pageurl = new \surl($PAGE->get_url(), array('page' => $p));
            $page->pageurl = $pageurl->out();
            $page->page = ($p + 1);
            $context->pages[] = $page;
            $p++;
        } while (($p * $this->_perpage) < $totalcount);

        return $context;
    }

}