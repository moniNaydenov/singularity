<?php

/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\admin;

defined('INTERNAL') || die();


class table_editor {
    protected $_table;
    protected $_sql;
    protected $_recordsmode          = false;
    protected $_mode                 = self::MODE_TABLE;
    protected $_fields;
    protected $_records              = [];
    protected $_recordscount          = 0;
    protected $_currentpage          = 0;
    protected $_pagescount           = 0;
    protected $_pagesize             = 100;
    protected $_visiblefields;
    protected $_filters;
    protected $_orderby;
    protected $_idfieldname;
    protected $_sortorderfield       = 'sortorder';
    protected $_datefields;
    protected $_recordhandler        = null;
    protected $_title_fields         = [];
    protected $_title_customaction   = null;
    protected $_title_customaction2  = null;
    protected $_title_customaction3  = null;
    protected $_target_customaction  = 'self';
    protected $_target_customaction3 = 'self';
    protected $_target_customaction2 = 'self';
    protected $_title_insertbutton;
    protected $_title_backbutton;
    protected $_title_editbutton;
    protected $_title_deletebutton;
    protected $_show_movebuttons     = false;
    protected $_show_insertbutton    = true;
    protected $_show_backbutton      = false;
    protected $_show_deletebutton    = true;
    protected $_show_editbutton      = true;
    protected $_show_checkboxes      = false;
    protected $_rowactions           = [];
    protected $_globalactions        = [];

    const MODE_TABLE = 1;
    const MODE_SQL = 2;
    const MODE_RECORDS = 3;

    public const ACTION_DELETE = 'delete';
    public const ACTION_EDIT = 'edit';
    public const ACTION_INSERT = 'insert';


    public function __construct($tableorsql, $visiblefields, $orderby = 'id ASC', $sqlmode = false, $idfieldname = 'id') {
        global $DB;

        if ($sqlmode || is_null($tableorsql)) {
            $this->_sql = $tableorsql;
            $this->_visiblefields = $visiblefields;
            $this->_fields = array($idfieldname);
            $this->_idfieldname = $idfieldname;
            if (is_null($tableorsql)) {
                $this->_recordsmode = true;
                $this->_mode = self::MODE_RECORDS;
            } else {
                $this->_mode = self::MODE_SQL;
            }
        } else {
            $this->_table = $tableorsql;

            $this->_fields = $DB->get_columns($this->_table);

            $this->_visiblefields = array();
            foreach ($visiblefields as $visiblefield) {
                if (isset($this->_fields[$visiblefield])) {
                    $this->_visiblefields[] = $visiblefield;
                }
            }

        }

        if (!empty($orderby)) {
            $this->_orderby = $orderby;
        } else {

            $this->_orderby = 'id';
        }

        $this->_filters = array();
        $this->_datefields = array();

    }

    /**
     * @param $show
     */
    public function show_movebuttons($show) {
        $this->_show_movebuttons = $show;
    }

    /**
     * @param $show
     * @deprecated
     */
    public function show_insertbutton($show) {
        raise_warning('<strong>show_insertbutton</strong> is deprected! Please, use <strong>set_insertbutton</strong> instead');
        $this->_show_insertbutton = $show;
    }

    /**
     * @param $title
     * @deprecated
     */
    public function set_insertbutton_title($title) {
        raise_warning('<strong>set_insertbutton_title</strong> is deprected! Please, use <strong>set_insertbutton</strong> instead');
        $this->_title_insertbutton = $title;
    }

    public function set_insertbutton($show, $title = null) {
        $this->_show_insertbutton = $show;
        if (!empty($title)) {
            $this->_title_insertbutton = $title;
        }
    }

    public function show_checkboxes($show) {
        $this->_show_checkboxes = $show;
    }

    /**
     * @param $show
     * @deprecated
     */
    public function show_editbutton($show) {
        raise_warning('<strong>show_editbutton</strong> is deprected! Please, use <strong>set_editbutton</strong> instead');
        $this->_show_editbutton = $show;
    }
    /**
     * @param $title
     * @deprecated
     */
    public function set_editbutton_title($title) {
        raise_warning('<strong>set_editbutton_title</strong> is deprected! Please, use <strong>set_editbutton</strong> instead');
        $this->_title_editbutton = $title;
    }

    public function set_editbutton($show, $title = null) {
        $this->_show_editbutton = $show;
        if (!empty($title)) {
            $this->_title_editbutton = $title;
        }
    }

    /**
     * @param $show
     * @deprecated
     */
    public function show_deletebutton($show) {
        raise_warning('<strong>show_deletebutton</strong> is deprected! Please, use <strong>set_deletebutton</strong> instead');
        $this->_show_deletebutton = $show;
    }

    /**
     * @param $title
     * @deprecated
     */
    public function set_deletebutton_title($title) {
        raise_warning('<strong>set_deletebutton_title</strong> is deprected! Please, use <strong>set_deletebutton</strong> instead');
        $this->_title_deletebutton = $title;
    }

    public function set_deletebutton($show, $title = null) {
        $this->_show_deletebutton = $show;
        if (!empty($title)) {
            $this->_title_deletebutton = $title;
        }
    }

    /**
     * @param $show
     * @deprecated
     */
    public function show_backbutton($show) {
        raise_warning('<strong>show_backbutton</strong> is deprected! Please, use <strong>set_backbutton</strong> instead');
        $this->_show_backbutton = $show;
    }

    /**
     * @param $title
     * @deprecated
     */
    public function set_backbutton_title($title) {
        raise_warning('<strong>set_backbutton_title</strong> is deprected! Please, use <strong>set_backbutton</strong> instead');
        $this->_title_backbutton = $title;
    }

    public function set_backbutton($show, $title = null) {
        $this->_show_backbutton = $show;
        if (!empty($title)) {
            $this->_title_backbutton = $title;
        }
    }



    /**
     * @param $title
     * @deprecated
     */
    public function set_customaction($title, $target = 'self') {
        raise_warning('<strong>set_customaction</strong> is deprected! Please, use <strong>add_row_action</strong> instead');
        $this->_title_customaction = $title;
        $this->_target_customaction = $target;
    }

    /**
     * @param $title
     * @deprecated
     */
    public function set_customaction2($title, $target = 'self') {
        raise_warning('<strong>set_customaction</strong> is deprected! Please, use <strong>add_row_action</strong> instead');
        $this->_title_customaction2 = $title;
        $this->_target_customaction2 = $target;
    }

    /**
     * @param $title
     * @deprecated
     */
    public function set_customaction3($title, $target = 'self') {
        raise_warning('<strong>set_customaction</strong> is deprected! Please, use <strong>add_row_action</strong> instead');
        $this->_title_customaction3 = $title;
        $this->_target_customaction3 = $target;
    }

    public function set_record_handler(callable $handler) {
        $this->_recordhandler = $handler;
    }

    /**
     * @param array $datefields ['field_name => 'format']
     */
    public function set_datefields(array $datefields) {
        $this->_datefields = $datefields;
    }

    /**
     * @param array $fieldstitles ['field_name => 'title']
     */
    public function set_fields_titles(array $fieldstitles) {
        $this->_title_fields = $fieldstitles;
    }

    public function set_sortorderfield($sortorderfield) {
        if (isset($this->_fields[$sortorderfield])) {
            $this->_sortorderfield = $sortorderfield;
        }

    }

    public function add_global_action($name, $title, $target = 'self', $confirm = false) {
        $action = new \stdClass;
        $action->name = $name;
        $action->title = $title;
        $action->target = $target;
        $action->confirm = $confirm;
        $this->_globalactions[$name] = $action;
    }

    public function remove_global_action($name) {
        if (isset($this->_globalactions[$name])) {
            unset($this->_globalactions[$name]);
        }
    }

    public function add_row_action($name, $title, $target = 'self') {
        $action = new \stdClass;
        $action->name = $name;
        $action->title = $title;
        $action->target = $target;
        $this->_rowactions[$name] = $action;
    }

    public function remove_row_action($name) {
        if (isset($this->_rowactions[$name])) {
            unset($this->_rowactions[$name]);
        }
    }

    public function get_row_action() {
        $action = optional_param('action', false, PARAM_RAW);
        $delete = optional_param('delete', false, PARAM_BOOL);
        if ($action && isset($this->_rowactions[$action])) {
            return $action;
        }
        if ($delete) {
            return self::ACTION_DELETE;
        }
        return false;
    }

    public function get_global_action() {
        $action = optional_param('globalaction', false, PARAM_RAW);
        if ($action && isset($this->_globalactions[$action])) {
            return $action;
        }
        foreach ($this->_globalactions as $actionname => $t) {
            $action = optional_param('globalaction_' . $actionname, false, PARAM_RAW);
            if ($action) {
                return $actionname;
            }
        }

        return false;
    }

    public function get_checked_checkboxes() {
        $checkboxes = optional_param_array('tableeditor_checkbox', [], PARAM_INT); //TODO: check if INT is enough (other ID value types possible??)
        return $checkboxes;
    }

    /**
     * @return array
     */
    public function get_visible_fields() {
        return $this->_visiblefields;
    }

    /**
     * @param $filters
     */
    public function set_filters($filters) {
        $this->_filters = $filters;
    }

    /**
     * @param $filters
     */
    public function set_sql_filters($filters) {
        $this->_filters = $filters;
    }

    /*
     * if records set, no DB calls will be made, and the set objects will be used
     */
    public function set_records($records) {
        $this->_records = $records;
    }


    /**
     * @param int $startfrom
     * @param int $count
     *
     * @return array
     */
    public function get_records($startfrom = 0, $count = 10) {
        global $DB;

        if (!empty($this->_records) || $this->_recordsmode) {
            return $this->_records;
        } else if (!empty($this->_sql)) {
            return $DB->get_records_sql($this->_sql,
                $this->_filters,
                $this->_orderby,
                $startfrom
            ) ;
        } else {
            return $DB->get_records($this->_table,
                $this->_filters,
                //implode(', ', $this->_visiblefields),
                '*',
                $this->_orderby,
                $count,
                $startfrom
            ) ;
        }
    }

    public function get_records_count() {
        global $DB;
        if ($this->_recordscount > 0) {
            return $this->_recordscount;
        }
        if (!empty($this->_records)) {
            $this->_recordscount = count($this->_records);
        } else if (!empty($this->_sql)) {
            //TODO:
            $this->get_records();
            $this->_recordscount = count($this->_records);
        } else {
            $this->_recordscount = $DB->count($this->_table, $this->get_idfieldname(), $this->_filters);
        }
        return $this->_recordscount;
    }


    /**
     * @return null
     */
    public function get_idfieldname() {
        if (empty($this->_idfieldname)) {
            $idfield = reset($this->_fields);
            $this->_idfieldname = $idfield['column_name'];
        }
        return $this->_idfieldname;
    }


    /**
     * @param $id
     *
     * @return bool|int
     */
    public function delete_record($id) {
        global $DB;
        $records = $this->get_records(0, 0);
        $idfieldname = $this->get_idfieldname();

        if (isset($records[$id])) {
            return $DB->delete_record($this->_table, array($idfieldname => $id));
        }
        return false;

    }

    /**
     * @param $id
     * @param $directionup
     *
     * @return boolean
     */
    public function move_record($id, $directionup) {
        global $DB;

        if ($this->_mode != self::MODE_TABLE) {
            raise_warning('<strong>move_record</strong> can be used only with MODE_TABLE');
            return false;
        }

        $idfieldname = $this->get_idfieldname();
        $record = $DB->get_record($this->_table, [$idfieldname => $id] + $this->_filters);
        $sortorder = $record->{$this->_sortorderfield};
        $sign = $directionup ? ' < ' : ' > ';
        $orderdirection = $directionup ? ' DESC' : ' ASC';
        $nextrecord = $DB->get_record_where($this->_table,
            "{$this->_sortorderfield} $sign :sortorder",
            ['sortorder' => $sortorder],
            $this->_filters,
            '*',
            $this-> _sortorderfield . $orderdirection);
        if ($nextrecord) {
            var_dump($nextrecord);
            $nextsortorder = $nextrecord->{$this->_sortorderfield};
            $record->{$this->_sortorderfield} = $nextsortorder;
            $nextrecord->{$this->_sortorderfield} = $sortorder;
            $DB->update_record($this->_table, $record);
            $DB->update_record($this->_table, $nextrecord);
            return true;
        }
        return false;
    }

    public function get_pagecount() {
        if ($this->_pagescount > 0) {
            return $this->_pagescount;
        }
        $recordscount = $this->get_records_count();
        $this->_pagescount = ceil($recordscount / $this->_pagesize);
        return $this->_pagescount;
    }

    public function get_currentpage() {
        $this->_currentpage = optional_param('page', 0, PARAM_INT);
        if ($this->_currentpage < 0 || $this->_currentpage > $this->get_pagecount()) {
            $this->_currentpage = 0;
        }
        return $this->_currentpage;
    }

    public function set_currentpage($page) {
        if ($page < 0 || $page > $this->get_pagecount()) {
            return;
        }
        $this->_currentpage = $page;
    }

    public function get_pagesize() {
        return $this->_pagesize;
    }
    public function set_pagesize($pagesize) {
        $this->_pagesize = $pagesize;
    }


    /**
     * @param $record
     *
     * @return \surl
     */
    public function get_editurl($record) {
        global $PAGE;


        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            $ret = new \surl($PAGE->get_url());
            $ret->add_param($idfieldname, $record->{$idfieldname});
            return $ret;
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return $PAGE->get_url();
    }

    /**
     * @param $record
     *
     * @return \surl
     */
    public function get_customurl($record, $number = 1) {
        global $PAGE;


        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            $ret = new \surl($PAGE->get_url());
            $ret->add_sessionid();
            $ret->add_param($idfieldname, $record->{$idfieldname});
            if ($number == 1) {
                $ret->add_param('customaction', 1);
            } else if ($number == 2) {
                $ret->add_param('customaction2', 1);
            } else {
                $ret->add_param('customaction3', 1);
            }

            return $ret;
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return $PAGE->get_url();
    }

    /**
     * @param $record
     *
     * @return \surl
     */
    public function get_rowactionurl($record, $action) {
        global $PAGE;


        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            $ret = new \surl($PAGE->get_url());
            $ret->add_sessionid();
            $ret->add_param($idfieldname, $record->{$idfieldname});
            $ret->add_param('action', $action);

            return $ret;
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return $PAGE->get_url();
    }

    /**
     * @param $record
     *
     * @return \surl
     */
    public function get_globalactionurl($action) {
        global $PAGE;

        $ret = new \surl($PAGE->get_url());
        $ret->add_sessionid();
        $ret->add_param('globalaction', $action);

        return $ret;
    }

    /**
     * @param $record
     *
     * @return \surl
     */
    public function get_deleteurl($record) {
        global $PAGE;

        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            $ret = new \surl($PAGE->get_url());
            $ret->add_sessionid();
            $ret->add_param($idfieldname, $record->{$idfieldname});
            $ret->add_param('delete', 1);
            return $ret;
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return $PAGE->get_url();
    }

    /**
     * @param $record
     * @param $directionup
     *
     * @return \surl
     */
    public function get_moveurl($record, $directionup) {
        global $PAGE;

        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            $ret = new \surl($PAGE->get_url());
            $ret->add_sessionid();
            $ret->add_param($idfieldname, $record->{$idfieldname});
            $ret->add_param('move', 1);
            $ret->add_param('up', ($directionup ? 1 : 0));
            return $ret;
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return $PAGE->get_url();
    }

    /**
     * @param $record
     * @param $directionup
     *
     * @return String
     */
    public function get_checkbox_value($record) {
        global $PAGE;

        $idfieldname = $this->get_idfieldname();

        if (isset($record->{$idfieldname})) {
            return $record->{$idfieldname};
        }
        raise_warning('Missing property {' . $idfieldname . '} of record');
        return false;
    }

    /**
     * @param \core\output\core $output
     *
     * @return \stdClass
     */
    public function export_for_template(\core\output\core $output) {
        global $PAGE, $CFG;
        $visiblefields = $this->get_visible_fields();

        foreach ($visiblefields as $id => $field) {
            if (isset($this->_title_fields[$field])) {
                $visiblefields[$id] = $this->_title_fields[$field];
            } else if ($CFG->stringmanager->has_string($field, 'site')) {
                $visiblefields[$id] = get_string($field, 'site');
            } else if ($CFG->stringmanager->has_string($field, 'admin')) {
                $visiblefields[$id] = get_string($field, 'admin');
            } else {
                $visiblefields[$id] = get_string($field);
            }

        }

        $startfrom = $this->get_currentpage() * $this->get_pagesize();
        $limit = $this->get_pagesize();

        $records = $this->get_records($startfrom, $limit);

        $inserturl = new \surl($PAGE->url, array(
            'id' => -1,
        ));

        $backurl = new \surl($PAGE->url, array(
            'goback' => 1
        ));

        $context = new \stdClass();
        $context->head = $visiblefields;
        $context->inserturl = $inserturl->out();
        $context->backurl = $backurl->out();

        //TODO: deprecate!!
        $context->title_customaction = empty($this->_title_customaction) ? false : $this->_title_customaction;
        $context->title_customaction2 = empty($this->_title_customaction2) ? false : $this->_title_customaction2;
        $context->title_customaction3 = empty($this->_title_customaction3) ? false : $this->_title_customaction3;
        $context->target_customaction = $this->_target_customaction == 'self' ? false : $this->_target_customaction;
        $context->target_customaction2 = $this->_target_customaction2 == 'self' ? false : $this->_target_customaction2;
        $context->target_customaction3 = $this->_target_customaction3 == 'self' ? false : $this->_target_customaction3;


        $context->title_insertbutton = empty($this->_title_insertbutton) ? get_string('addnew') : $this->_title_insertbutton;
        $context->title_editbutton = empty($this->_title_editbutton) ? get_string('edit') : $this->_title_editbutton;
        $context->title_deletebutton = empty($this->_title_deletebutton) ? get_string('delete') : $this->_title_deletebutton;
        $context->title_backbutton = empty($this->_title_backbutton) ? get_string('back') : $this->_title_backbutton;
        $context->show_insertbutton = $this->_show_insertbutton;
        $context->show_editbutton = $this->_show_editbutton;
        $context->show_deletebutton = $this->_show_deletebutton;
        $context->show_backbutton = $this->_show_backbutton;
        $context->show_checkboxes = $this->_show_checkboxes;

        $context->globalactions = [];

        $context->has_confirm = false;
        foreach ($this->_globalactions as $globalaction) {
            $globalaction->actionurl = $this->get_globalactionurl($globalaction->name);
            $context->has_confirm = $context->has_confirm || $globalaction->confirm;
            $context->globalactions[] = $globalaction;
        }

        $context->body = array();

        $recordslastid = count($records) - 1;
        $i = 0;
        foreach ($records as $record) {
            $row = new \stdClass();

            if (isset($record->lastmodified) && !isset($this->_datefields['lastmodified'])) {
                $record->lastmodified = strftime('%H:%M:%S %d-%m-%Y', $record->lastmodified);
            }

            if (!is_null($this->_recordhandler)) {
                $callable = $this->_recordhandler;
                if (is_callable($callable)) {
                    $record = $callable($record);
                }
            }

            $values = array();
            foreach ($this->_visiblefields as $field) {
                if (isset($this->_datefields[$field])) {
                    $values[] = strftime($this->_datefields[$field], $record->{$field});
                } else {
                    $values[] = $record->{$field};
                }
            }

            $row->values    = $values; //array_values((array)$record);
            $row->editurl   = $this->get_editurl($record);
            $row->deleteurl = $this->get_deleteurl($record);

            //TODO: deprecate!
            $row->customurl = $this->get_customurl($record);
            $row->customurl2 = $this->get_customurl($record, 2);
            $row->customurl3 = $this->get_customurl($record, 3);

            $row->moveupurl = $this->get_moveurl($record, true);
            $row->movedownurl = $this->get_moveurl($record, false);
            $row->checkboxvalue = $this->get_checkbox_value($record);
            $row->showmovebuttons = $this->_show_movebuttons;

            $row->actions = [];
            foreach ($this->_rowactions as $rowaction) {
                $action = new \stdClass;
                $action->title = $rowaction->title;
                $action->actionurl = $this->get_rowactionurl($record, $rowaction->name);
                $action->target = $rowaction->target == 'self' ? false : $rowaction->target;
                $row->actions[] = $action;
            }


            if ($i == 0) {
                $row->first = true;
            }
            if ($i == $recordslastid) {
                $row->last = true;
            }
            $i++;
            $context->body[] = $row;
        }

        // Pages
        if ($this->get_pagecount() > 1) {
            $context->showpages = true;
            $pageurl = new \surl($PAGE->get_url());
            $context->pages = [];
            for ($p = 0; $p < $this->get_pagecount(); $p++) {
                $page = new \stdClass();
                $page->number = $p + 1;
                $pageurl->add_param('page', $p);
                $page->url = $pageurl->out();
                $page->current = $p == $this->get_currentpage();
                $context->pages[] = $page;
            }
        }
        return $context;
    }

}
