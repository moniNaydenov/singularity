<?php


namespace core\admin;

defined('INTERNAL') || die();


class nested_table_editor extends table_editor {
    protected $_parentfield;
    public function __construct($table, $parentfield, $visiblefields, $sortby = 'id ASC') {
        parent::__construct($table, $visiblefields, $sortby);
        $this->_parentfield = $parentfield;
        if (!isset($this->_fields[$parentfield])) {
            raise_exception('$parentfield {' . $parentfield . '} not found in table {' . $table . '}');
        }
    }




    public function get_records($startfrom = 0, $count = 10) {
        global $DB;
        $filters = $this->_filters;
        if (!isset($filters[$this->_parentfield])) {
            $filters[$this->_parentfield] = 0;
        }

        $result = $this->get_recursive_records_with_parent(0, 0);

        return $result;
    }

    protected function get_recursive_records_with_parent($parentid, $level) {
        global $DB;
        $filters = $this->_filters;
        $filters[$this->_parentfield] = $parentid;
        $records = $DB->get_records($this->_table, $filters, '*', $this->_orderby); //TODO: implement startform and count
        $result = array();
        $idfieldname = $this->get_idfieldname();
        $firstvisiblecolumn = reset($this->_visiblefields);
        foreach ($records as $record) {
            $record->{$firstvisiblecolumn} = '<div style="padding-left: ' . sprintf('%.1F', $level*2 + 0.5) . 'rem;">' . $record->{$firstvisiblecolumn} . '</div>';
            $result[$record->{$idfieldname}] = $record;
            $children = $this->get_recursive_records_with_parent($record->{$idfieldname}, $level+1);
            $result += $children;
        }
        return $result;
    }

}