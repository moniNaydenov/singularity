<?php





defined('INTERNAL') || die();

/**
 * singlarity url class
 * handles urls "securely"
 *
 */

class surl {
    private $params = array();
    private $scheme;
    private $host;
    private $path = '';
    private $port = 80;
    private $fragment;


    public function __construct($url, $params = array()) {
        global $CFG;

        if ($url instanceof self) {
            $parsed = array(
                'scheme' => $url->get_scheme(),
                'port' => $url->get_port(),
                'path' => $url->get_path(),
                'host' => $url->get_host(),
                'fragment' => $url->get_fragment()
            );
            $this->add_params($url->get_params());

        } else {
            $parsed = parse_url($url);
        }

        if (isset($parsed['scheme'])) {

            if (strpos($url, 'https') !== false) {
                $this->scheme = 'https';
                $this->port = 443;

            } else {
                $this->scheme = $parsed['scheme'];
            }
            $this->host = $parsed['host'];

            if (isset($parsed['path'])) {
                $this->path = $parsed['path'];
            }

            if (isset($parsed['port'])) {
                $this->port = $parsed['port'];
            }


        } else {
            $this->scheme = $CFG->wwwroot_surl->get_scheme();
            $this->host = $CFG->wwwroot_surl->get_host();
            $this->path = $CFG->wwwroot_surl->get_path();
            $this->port = $CFG->wwwroot_surl->get_port();

            if (isset($parsed['path'])) {
                $this->path .= $parsed['path'];
            }
        }

        if (isset($parsed['query'])) {
            parse_str($parsed['query'], $qparams);
            $this->add_params($qparams);
        }
        $this->add_params($params);

        if (isset($parsed['fragment'])) {
            $this->fragment = $parsed['fragment'];
        }

    }

    public function add_param($paramname, $paramvalue, $escape = true) {
        if ($escape) {
            $paramvalue = urlencode($paramvalue);
        }
        $this->params[$paramname] = $paramvalue;
    }

    public function add_params($params, $escape = true) {
        foreach ($params as $paramname => $paramvalue) {
            $this->add_param($paramname, $paramvalue, $escape);
        }
    }

    public function remove_param($paramname) {
        if (isset($this->params[$paramname])) {
            unset($this->params[$paramname]);
        }
    }

    public function remove_params($params) {
        foreach ($params as $param) {
            $this->remove_param($param);
        }
    }

    public function param($paramname, $paramvalue = null, $escape = true) {
        if (is_null($paramvalue)) {
            if (isset($this->params[$paramname])) {
                return $this->params[$paramname];
            }
            return null;
        } else {
            $this->add_param($paramname, $paramvalue, $escape);
        }
    }

    public function set_fragment($fragment = '') {
        $this->fragment = $fragment;
    }

    public function get_fragment() {
        return $this->fragment;
    }

    public function get_scheme() {
        return $this->scheme;
    }


    public function get_host() {
        return $this->host;
    }


    public function get_path() {
        return $this->path;
    }

    public function get_port() {
        return $this->port;
    }

    public function get_relative_path(surl $baseurl) {

        if ($this->is_suburlof($baseurl) == false) {
            return $this->path;
        }
        $basepath = $baseurl->get_path();
        $relativepath = str_replace($basepath, '', $this->path);
        return $relativepath;
    }

    public function is_suburlof(surl $baseurl) {
        $path = empty($this->path) ? '/' : $this->path;
        $basepath = empty($baseurl->path) ? '/' : $baseurl->path;
        if ($baseurl->scheme == $this->scheme &&
            $baseurl->host == $this->host &&
            $baseurl->port == $this->port &&
            strpos($path, $basepath) === 0) {
            return true;
        }
        return false;
    }

    public function get_params() {
        return $this->params;
    }

    public function add_sessionid() {
        $this->add_param('sessionid', sessionid());
    }

    public function out($escape = false, $aslocal = false) {
        global $CFG;
        $port = '';
        if ($this->port != 80 && $this->port != 443) {
            $port = ':' . strval($this->port);
        }

        $url = sprintf('%s://%s%s%s', $this->scheme, $this->host, $port, $this->path);
        if (!empty($this->params)) {
            $params = array();
            foreach ($this->params as $paramname => $paramvalue) {
                $params[] = $paramname . '=' . $paramvalue;
            }
            $params = implode('&', $params);
            $url .= '?' . $params;
        }

        if (!empty($this->fragment)) {
            $url .= '#' .$this->fragment;
        }
        if ($aslocal) {
            $url = str_replace($CFG->wwwroot, '', $url);
        }
        if ($escape) {
            $url = rawurlencode($url);
        }
        return $url;
    }

    public function __toString() {
        return $this->out();
    }

    public function equals($to) {
        if (!($to instanceof surl)) {
            $to = new surl($to);
        }
        if ($this->host != $to->host ||
            $this->scheme != $to->scheme ||
            $this->port != $to->port ||
            $this->path != $to->path) {
            return false;
        }
        if (count($this->params) != count($to->params)) {
            return false;
        }
        foreach ($this->params as $pname => $pvalue) {
            if (!isset($to->params[$pname]) || $pvalue != $to->params[$pname]) {
                return false;
            }
        }
        return true;
    }

}