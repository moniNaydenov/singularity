<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2019
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\gallery;

use \core\persistent;

class gallery extends persistent {
    static $_dbtable = 'gallery';
    protected $_items = [];

    protected static $_types = [];

    public function get_items() {
        global $DB;
        if (empty($this->_items)) {
            $items = $DB->get_records('gallery_item', ['galleryid' => $this->id], '*', 'sortorder ASC');
            foreach ($items as $item) {
                $this->_items[$item->id] = new item($item);
            }
        }
        return $this->_items;
    }

    public function get_max_sortorder() {
        global $DB;
        $sql = 'SELECT MAX(sortorder) AS maxsortorder FROM gallery_item gi WHERE gi.galleryid = :galleryid';
        $maxsortorder = $DB->get_record_sql($sql, ['galleryid' => $this->id]);
        if ($maxsortorder) {
            $maxsortorder = $maxsortorder->maxsortorder + 1;
        } else {
            $maxsortorder = 0;
        }
        return $maxsortorder;
    }

    public function delete_item($itemid) {
        global $DB;
        $items = $this->get_items();
        $found = false;
        $prevsortorder = -1;
        foreach ($items as $id => $item) {
            if ($id == $itemid) {
                $found = true;
                $prevsortorder = $item->sortorder;
                $item->delete_from_db();
            } else if ($found) {
                $tmpsortorder = $item->sortorder;
                $item->sortorder = $prevsortorder;
                $item->update_to_db();
                $prevsortorder = $tmpsortorder;
            }
        }
        return $found;
    }

    public function export_for_template(\core\output\core $output) {
        $context = parent::export_for_template($output);
        $context->{'type:' . $context->type} = true;
        $context->items = [];
        $items = $this->get_items();
        $first = true;
        $i = 0;
        foreach ($items as $item) {
            $itemcontext = $item->export_for_template($output);
            $itemcontext->first = $first;
            $itemcontext->i = strval($i++);
            $first = false;
            $context->items[] = $itemcontext;
        }
        return $context;
    }


    public static function get_all_galleries() {
        global $DB;
        $galleries = $DB->get_records('gallery', [], '*', 'lastmodified DESC');
        return $galleries;
    }

    public static function get_gallery_types() {
        if (empty(self::$_types)) {
            self::$_types = [
                //'grid' => get_string('gallery:type:grid', 'admin'),
                'list_horizontal' => get_string('gallery:type:list_horizontal', 'admin'),
                'click_carousel' => get_string('gallery:type:click_carousel', 'admin')
            ];
        }
        return self::$_types;
    }

}