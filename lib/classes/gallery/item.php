<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2019
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\gallery;
use core\persistent;
use surl;

class item extends persistent{
    public static $_dbtable = 'gallery_item';

    public function export_for_template(\core\output\core $output) {
        $context = parent::export_for_template($output);
        $image = \core\file::create_from_hash($this->image);
        $context->imageurl = $image->get_url();
        if (!empty($context->link)) {
            $context->link = (new surl($context->link))->out();
        }
        return $context;
    }

}