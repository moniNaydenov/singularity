<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 28.04.17
 * Time: 15:21
 */

namespace core\navigation;


class admin_nav extends nav {
    private $_maxsortorder;

    public function __construct() {
        global $CFG;
        $this->_navtype = 'admin-nav';
        $this->_mainnode = new nav_node(0, '', true, null, $CFG->lang, 0);
        $this->build_admin_nav();
    }

    private function build_admin_nav() {
        global $CFG;
        $this->_mainnode->add_child(get_string('adminhome'), true, '/admin/index.php', $CFG->lang, 1);
        $this->_mainnode->add_child(get_string('sitehome'), true, '/index.php', $CFG->lang, 100);
        $generalnode = $this->_mainnode->add_child(get_string('settings:general', 'admin'), true, null, $CFG->lang, 101);
        $generalnode->add_child(get_string('filebrowser'), true, '/admin/filebrowser.php', $CFG->lang, 200);
        $generalnode->add_child(get_string('navitems'), true, '/admin/nav/index.php', $CFG->lang, 300);
        $generalnode->add_child(get_string('sitepages'), true, '/admin/site_pages.php', $CFG->lang, 300);
        $generalnode->add_child(get_string('htmlblocks'), true, '/admin/block/html_block/index.php', $CFG->lang, 300);
        $generalnode->add_child(get_string('news'), true, '/admin/news/index.php', $CFG->lang, 300);
        $generalnode->add_child(get_string('gallery:galleries', 'admin'), true, '/admin/gallery/index.php', $CFG->lang, 400);
        $adminnode = $this->_mainnode->add_child(get_string('settings:admin', 'admin'), true, null, $CFG->lang, 10000);
        $adminnode->add_child(get_string('generalsettings', 'admin'), true, '/admin/general.php', $CFG->lang, 300);
        $adminnode->add_child(get_string('changepassword'), true, '/admin/changepassword.php', $CFG->lang, 500);
        $adminnode->add_child(get_string('log:view', 'admin'), true, '/admin/log.php', $CFG->lang, 501);
        $adminnode->add_child(get_string('purgecaches'), true, '/admin/purgecaches.php?sessionid=' . sessionid(), $CFG->lang, 10000);
        $adminnode->add_child(get_string('purgecaches:updaterevonly'), true, '/admin/purgecaches.php?revonly=true&sessionid=' . sessionid(), $CFG->lang, 10000);
        $this->_maxsortorder = 400;
        $extendfunction = 'site_extend_admin_nav';
        if (function_exists($extendfunction)) {
            $extendfunction($this);
        }
    }

    public function add_child($title, $visible, $url, $lang, $sortorder = 0, $target = null) {
        if ($sortorder == 0) {
            $this->_maxsortorder++;
            $sortorder = $this->_maxsortorder;
        }
        return $this->_mainnode->add_child($title, $visible, $url, $lang, $sortorder, $target);
    }


}