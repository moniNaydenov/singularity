<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 28.04.17
 * Time: 15:23
 */

namespace core\navigation;

class nav {
    protected $_mainnode;
    protected $_navtype = '';

    public function export_for_template($output) {
        $context = $this->_mainnode->export_for_template($output);
        $context->navtype = $this->_navtype;
        return $context;
    }


    public function fetch_children($fetchdepth = 1) {
        return $this->_mainnode->fetch_children($fetchdepth);
    }


    /**
     * @return \core\navigation\nav_node
     */
    public function get_mainnode() {
        return $this->_mainnode;
    }

}