<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 27.04.17
 * Time: 13:12
 */


namespace core\navigation;

class nav_node {
    private $_title;
    private $_url;
    private $_sortorder;
    private $_dbid;
    private $_children;
    private $_visible;
    private $_lang;
    private $_hasactivenode;
    private $_parentnode;
    private $_target;
    private static $ALLOWED = array('url', 'title', 'sortorder', 'visible', 'lang');

    public function __construct($dbid, $title, $visible, $url, $lang, $sortorder, $parentnode = null, $target = null) {
        global $PAGE;
        $this->_dbid = $dbid;
        $this->_title = $title;
        if (is_null($url) || $url === '#') {
            $this->_url = null;
        } else {
            $this->_url = new \surl($url);
        }
        $this->_visible = $visible;
        $this->_sortorder = $sortorder;
        $this->_lang = $lang;
        $this->_parentnode = $parentnode;
        $this->_target = $target;
        $this->_children = array();
        $this->get_active();

    }

    public function __get($key) {
        if (in_array($key, self::$ALLOWED)) {
            return $this->{"_$key"};
        }
        return null;
    }

    public function __set($key, $value) {
        if (in_array($key, self::$ALLOWED)) {
            $this->{"_$key"} = $value;
        }
    }

    public function get_id() {
        return $this->_dbid;
    }

    public function get_lang() {
        return $this->_lang;
    }

    public function get_title() {
        return $this->_title;
    }

    public function get_parentnode() {
        return $this->_parentnode;
    }

    public function get_url() {
        return $this->_url;
    }

    public function get_target() {
        return $this->_url;
    }

    public function get_active() {
        global $PAGE;
        $active = $PAGE->get_url()->equals($this->_url);
        if ($active) {

            $this->set_activenode_rec(true);
        }
        return $active;
    }

    public function has_activenode() {
        return $this->_hasactivenode;
    }

    public function set_activenode_rec($hasactivechild) {
        $this->_hasactivenode = $hasactivechild;
        if (!is_null($this->_parentnode)) {
            $this->_parentnode->set_activenode_rec($hasactivechild);
        }
    }

    public function find_activenode() {
        if ($this->get_active()) {
            return $this;
        }
        if ($this->has_activenode()) {
            foreach ($this->_children as $childnode) {
                if ($childnode->has_activenode()) {
                    return $childnode->find_activenode();
                }
            }
        }
        return null;
    }

    public function find($title) {
        if ($this->get_title() == $title) {
            return $this;
        }
        foreach ($this->_children as $child) {
            $result = $child->find($title);
            if (!is_null($result)) {
                return $result;
                break;
            }
        }
        return null;
    }

    public function export_for_template($output) {
        global $PAGE;
        if ($this->_visible == false) {
            return false;
        }
        $context = new \stdClass();
        $context->title = $this->_title;
        $context->active = false;
        $context->id = $this->_dbid;
        $context->target = $this->_target;
        $context->parentid = null;
        if ($this->_parentnode) {
            $context->parentid = $this->_parentnode->get_id();
        }

        if (is_null($this->_url)) {
            $context->url = false;
        } else {
            $context->active = $this->get_active();
            $context->url = $this->_url->out();
        }

        $children = array();
        foreach ($this->_children as $child) {
            $c = $child->export_for_template($output);
            if ($c) {
                $children[] = $c;
            }
        }
        if (!empty($children)) {
            $context->children = $children;
        }
        $context->haschildren = !empty($children);
        return $context;
    }

    public function fetch_children($fetchdepth = 1) {
        global $DB;
        if ($this->_dbid == -1 || !empty($this->_children)) {
            return $this->_children;
        } else if ($this->_dbid == 0) {
            $children = $DB->get_records('nav_item', array('parent' => 0, 'lang' => $this->_lang), '*', 'sortorder');
        } else {
            $children = $DB->get_records('nav_item', array('parent' => $this->_dbid), '*', 'sortorder');
        }
        foreach ($children as $child) {
            $child = new self($child->id, $child->title, $child->visible, $child->url, $child->lang, $child->sortorder, $this, $child->target);
            if ($fetchdepth != 0) {
                $child->fetch_children($fetchdepth - 1);
            }
            $this->_children[] = $child;
        }
        $this->sort_children();
        return $this->_children;
    }

    private function sort_children() {
        usort($this->_children, function($a, $b) {
            $sa = $a->_sortorder;
            $sb = $b->_sortorder;
            return $sa == $sb ? 0 : ($sa < $sb ? -1 : +1);
        });
    }

    public function add_child($title, $visible, $url, $lang, $sortorder, $target = null) {
        $navnode = new nav_node(-1, $title, $visible, $url, $lang, $sortorder, $this, $target);
        $this->_children[] = $navnode;
        $this->sort_children();
        return $navnode;
    }

    public static function create_from_id($id) {
        global $DB;
        $nav_item = $DB->get_record('nav_item', array('id' => $id));
        if ($nav_item) {
            $nav_node = new self($nav_item->id, $nav_item->title, $nav_item->visible, $nav_item->url, $nav_item->lang, $nav_item->sortorder, null, $nav_item->target);
            return $nav_node;
        }
        return null;
    }
}