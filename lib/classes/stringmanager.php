<?php

namespace core;

defined('INTERNAL') || die;

class stringmanager {
    private $loadedsites;
    private $allowedsites;
    private $strings;
    private $lang;
    private $fallbacklang = 'en';

    public static $LOCALES = array(
        'bg' => 'bg_BG.UTF-8',
        'en' => 'en_US.UTF-8',
        'de' => 'de_AT.UTF-8'
    );

    public function __construct() {
        global $CFG;

        $this->loadedsites = array();
        $this->strings = array();
        $this->lang = $CFG->lang;

        $this->allowedsites[] = 'core';
        $this->allowedsites[] = 'admin';
        $this->allowedsites[] = 'site';

        $this->allowedsites = array_flip($this->allowedsites);


    }

    public function get_string($identifier, $site = '') {
        if (empty($site)) {
            $site = 'site';
        }

        $this->load_site($site);
        $this->load_site($site, $this->fallbacklang);

        if (isset($this->strings[$site][$this->lang][$identifier])) {
            return $this->strings[$site][$this->lang][$identifier];
        }

        if (isset($this->strings[$site][$this->fallbacklang][$identifier])) {
            return $this->strings[$site][$this->fallbacklang][$identifier];
        }

        if ($site != 'core') {
            return $this->get_string($identifier, 'core');
        }

        raise_warning('String not found - <strong>' . $identifier . ' / ' . $site . '</strong>');
        // always fallback to core (just in case)
        /*if ($site != 'core') {
            return $this->get_string($identifier);
        }*/

        return '[[' . $identifier . ' / ' . $site . ']]';
    }

    public function has_string($identifier, $site = '') {
        if (empty($site)) {
            $site = 'site';
        }

        $this->load_site($site);
        $this->load_site($site, $this->fallbacklang);

        if (isset($this->strings[$site][$this->lang][$identifier])) {
            return true;
        }

        if (isset($this->strings[$site][$this->fallbacklang][$identifier])) {
            return true;
        }
        return false;

    }

    private function load_site($site, $lang = '') {
        global $CFG;

        if (!isset($this->allowedsites[$site])) {
            return false;
        }
        if (empty($lang)) {
            $lang = $this->lang;
        }
        if (isset($this->strings[$site][$lang])) {
            return true;
        }

        if ($site == 'core') {
            $candidate = $CFG->dirroot . '/lang/core/' . $lang . '.php';
        } else if ($site == 'admin') {
            $candidate = $CFG->dirroot . '/lang/admin/' . $lang . '.php';
        } else {
            $candidate = $CFG->dirroot . '/s/lang/' . $lang . '.php';
        }

        $string = array();
        if (file_exists($candidate)) {

            require_once($candidate);

            if (!isset($this->strings[$site])) {
                $this->strings[$site] = array();
            }

            $this->strings[$site][$lang] = $string;
            return true;
        }
        return false;
    }

    public function clean_for_htmlid($string) {
        $search = array(
            ':',
            '?',
            '!',
            '.',
            ',',
            '#',
            '/',
            '\\',
            '$',
            '<',
            '>'
        );

        $string = str_replace(' ', '-', $string);
        $string = str_replace($search, '', $string);
        return $string;
    }

    public function convert_bg_to_lat($string) {
        raise_warning('stringmanager::convert_bg_to_lat is deprectated! use stringmanager::<strong>seo_escape</strong> instead!');
        return $this->seo_escape($string);
    }
    public function seo_escape($string) {
        static $search;
        static $replace;

        $string = preg_replace('/( +)/', ' ', $string);

        if (empty($search)) {
            $map = array(
                'онлайн' => 'online',
                'Онлайн' => 'online',
                'Кеймбридж' => 'cambridge',
                'а' => 'a',
                'б' => 'b',
                'в' => 'v',
                'г' => 'g',
                'д' => 'd',
                'е' => 'e',
                'ж' => 'zh',
                'з' => 'z',
                'и' => 'i',
                'й' => 'y',
                'к' => 'k',
                'л' => 'l',
                'м' => 'm',
                'н' => 'n',
                'о' => 'o',
                'п' => 'p',
                'р' => 'r',
                'с' => 's',
                'т' => 't',
                'у' => 'u',
                'ф' => 'f',
                'х' => 'h',
                'ц' => 'c',
                'ч' => 'ch',
                'ш' => 'sh',
                'щ' => 'sht',
                'ъ' => 'a',
                'ь' => 'y',
                'ю' => 'yu',
                'я' => 'ya',
                'ѝ' => 'y',
                'ä' => 'ae',
                'ö' => 'oe',
                'ü' => 'ue',
                'ß' => 'ss',
                'А' => 'a',
                'Б' => 'b',
                'В' => 'v',
                'Г' => 'g',
                'Д' => 'd',
                'Е' => 'e',
                'Ж' => 'zh',
                'З' => 'z',
                'И' => 'i',
                'Й' => 'y',
                'К' => 'k',
                'Л' => 'l',
                'М' => 'm',
                'Н' => 'n',
                'О' => 'o',
                'П' => 'p',
                'Р' => 'r',
                'С' => 's',
                'Т' => 't',
                'У' => 'u',
                'Ф' => 'f',
                'Х' => 'h',
                'Ц' => 'c',
                'Ч' => 'ch',
                'Ш' => 'sh',
                'Щ' => 'sht',
                'Ъ' => 'a',
                'Ю' => 'yu',
                'Я' => 'ya',
                'Ä' => 'ae',
                'Ö' => 'oe',
                'Ü' => 'ue',
                '%' => '%25',
                ' ' => '-',
                '"' => '_%{}%_',
                '\'' => '_%{}%_',
                '+' => '_%{}%_',
                ',' => '_%{}%_',
                '.' => '_%{}%_',
                '!' => '_%{}%_',
                '?' => '_%{}%_'
            );

            $search = array_keys($map);
            $replace = array_values($map);
        }
        $escaped = str_replace($search, $replace, $string);
        $escaped = str_replace('_%{}%_', '', $escaped);

        $escaped = preg_replace('/(-+)/', '-', $escaped);
        $escaped = preg_replace('/(_+)/', '_', $escaped);
        $escaped = str_replace('-_', '-', $escaped);
        $escaped = str_replace('_-', '-', $escaped);
        return $escaped;
    }

}