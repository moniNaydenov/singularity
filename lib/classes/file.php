<?php

/**
 *
 * @package       sando
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2015
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace core;

use stdClass;
use security;
defined('INTERNAL') || die;

class file {
    private $fileobj;
    private $fs_filename = false;
    private $children = false;
    private $parent = false;
    private $filesize = null;
    private static $maxdepth = 100;

    public function __construct($idoruniquehashordbobj ) {
        global $DB;
        if (is_int($idoruniquehashordbobj)) {
            $this->fileobj = $DB->get_record('file', array('id' => $idoruniquehashordbobj));
        } else if (is_string($idoruniquehashordbobj)) {
            $this->fileobj = $DB->get_record('file', array('uniquehash' => $idoruniquehashordbobj));
        } else {
            $this->fileobj = $idoruniquehashordbobj;
        }
        if ($this->fileobj == false) {
            raise_warning(sprintf('Invalid id/uniquehash supplied - %s!', $idoruniquehashordbobj));
        }

        $this->update_uniquehash();

    }

    private function update_uniquehash() {
        if (empty($this->fileobj->uniquehash) || $this->fileobj->uniquehash == 'UPDATE_ME') {
            do {
                $this->fileobj->uniquehash = $this->calculate_uniquehash();
            } while ($tmp = self::create_from_hash($this->fileobj->uniquehash));
            $this->update_db_fileobj();
        }
        return;
    }

    private function update_db_fileobj() {
        global $DB;
        if ($this->deleted()) {
            return false;
        }
        $DB->update_record('file', $this->fileobj);
        return true;
    }

    private function calculate_uniquehash() {
        $hash = $this->fileobj->id;
        for ($i = 0; $i < 16; $i++) { // hash the id 16 times to make it hard to trace but still unique
            $hash = hash('sha1', $hash, true);
        }
        $hash = security::base64_encode_urlsafe($hash);
        return $hash;
    }

    public function success() {
        return !$this->deleted();
    }

    public function deleted() {
        return $this->fileobj == false;
    }


    public function is_file() {
        if ($this->deleted()) {
            return false;
        }
        return $this->fileobj->type == 0;
    }

    public function is_dir() {
        if ($this->deleted()) {
            return false;
        }
        return $this->fileobj->type == 1;
    }

    public function get_mimetype() {
        if ($this->deleted()) {
            return false;
        }
        return $this->fileobj->mimetype;
    }

    public function get_extension() {
        if ($this->deleted()) {
            return false;
        }
        $lastdot = stripos($this->fileobj->filename, '.');
        if ($lastdot !== false) {
            return substr($this->fileobj->filename, $lastdot + 1);
        }
        return '';
    }

    public function get_filename() {
        if ($this->deleted()) {
            return false;
        }
        return $this->fileobj->filename;
    }

    public function get_filenameshort() {
        $fullname = $this->get_filename();
        if (!$fullname) {
            return false;
        }
        if (mb_strlen($fullname) > 15) {
            $fullname = mb_substr($fullname, 0, 20)  . '...';
        }

        return $fullname;
    }

    public function get_path_as_string() {
        $parents = array();
        $depthcounter = 0;
        $current = $this;
        while ($current != false) { //traverse through parents in reverse order
            $parents[] = $current->get_filename();
            $current = $current->get_parent();
            if ($depthcounter > self::$maxdepth) {
                break;
            }
            $depthcounter++;
        }
        array_pop($parents); // get '/' out of the path
        $parents = array_reverse($parents); // reverse parent list - bring path dirs in normal order
        return '/' . implode('/', $parents); // add '/' as root


    }

    public function get_filesize() {
        if (!empty($this->filesize)) {
            return $this->filesize;
        }
        if ($this->deleted() || $this->is_dir() || !$this->fs_fileexists()) {
            return false;
        }
        $this->filesize = filesize($this->fs_getfilename());
        return $this->filesize;
    }

    public function get_id() {
        if ($this->deleted()) {
            return false;
        }
        return $this->fileobj->id;
    }

    public function get_url() {
        global $CFG;
        if ($this->deleted() || $this->is_dir()) {
            return false;
        }

        return $CFG->wwwroot . '/file.php?id=' . $this->fileobj->uniquehash;
    }

    public function get_uniquehash() {
        return $this->fileobj->uniquehash;
    }

    public function create_subdir($dirname) {
        global $DB, $USER;
        $dirname = clean_param($dirname, PARAM_FILENAME);
        if (empty($dirname) || $this->deleted() || $this->is_file()) {
            return false;
        }
        if ($DB->record_exists('file', array('parent' => $this->fileobj->id, 'filename' => $dirname))) {
            return false;
        }
        $fileobj = new stdClass;
        $fileobj->filename = $dirname;
        $fileobj->type = 1;
        $fileobj->filesize = 0;
        $fileobj->parent = $this->fileobj->id;
        $fileobj->mimetype = '';
        $fileobj->checksum = '';
        $fileobj->lastmodified = time();
        $fileobj->uniquehash = 'UPDATE_ME';
        $fileobj->modifiedby = $USER->id;
        $fileobj->id = $DB->insert_record('file', $fileobj);
        return new self($fileobj);
    }



    public function fs_fileexists() {
        if ($this->deleted()) {
            return false;
        }
        if ($this->is_dir()) {
            return true;
        }
        return file_exists($this->fs_getfilename());
    }

    public function delete() {
        global $DB;
        if ($this->deleted()) {
            return false;
        }
        if ($this->is_file()) {
            if ($this->fs_fileexists()) {
                if (count($DB->get_records('file', array('checksum' => $this->fileobj->checksum))) == 1) {
                    unlink($this->fs_getfilename());
                }
            }
        } else {
            if ($DB->record_exists('file', array('parent' => $this->fileobj->id))) {
                return false;
            }
        }
        $DB->delete_record('file', array('id' => $this->fileobj->id));
        $this->fileobj = false;
        return true;
    }

    public function rename($newname) {

        if ($this->deleted()) {
            return false;
        }
        $cleanname = clean_param($newname, PARAM_FILENAME);

        if (empty($cleanname)) {
            return false;
        }

        $this->fileobj->filename = $newname;
        $this->update_db_fileobj();
        return true;
    }

    public function fs_getfilename() {
        if ($this->fs_filename !== false) {
            return $this->fs_filename;
        }
        if ($this->deleted() || $this->is_dir() || empty($this->fileobj->checksum)) {
            return false;
        }
        $this->fs_filename = self::get_path_from_checksum($this->fileobj->checksum);
        return $this->fs_filename;
    }

    public static function get_path_from_checksum($checksum) {
        global $CFG;
        $checksum = clean_param($checksum, PARAM_HASH);
        if (strlen($checksum) != 40) {
            return false;
        }

        $dirs = array(
            substr($checksum, 0, 2),
            substr($checksum, 2, 2)
        );

        return sprintf('%s/files/%s/%s/%s', $CFG->dataroot, $dirs[0], $dirs[1], $checksum);
    }

    public function move_uploaded_file_to_dir($filename, $tmpfilename) {
        if ($this->fileobj === false) {
            return false;
        }
        return self::create_new_file_from_upload($filename, $tmpfilename, $this);
    }

    public function get_parent() {
        global $DB;
        if ($this->fileobj->parent == -1) {
            return false;
        }
        if ($this->parent == false) {
            $parentobj = $DB->get_record('file', array('id' => $this->fileobj->parent));
            if ($parentobj) {
                $this->parent = new self($parentobj);
            }
        }
        return $this->parent;
    }

    public function get_children() {
        global $DB;
        if ($this->children !== false) {
            return $this->children;
        }
        if ($this->deleted() || !$this->is_dir()) {
            return false;
        }
        $children = $DB->get_records('file', array('parent' => $this->fileobj->id), '*', 'type DESC, filename ASC');
        $this->children = array();
        foreach ($children as $child) {
            $this->children[] = new self($child);
        }
        return $this->children;
    }

    public function export_for_template(\core\output\core $output) {
        global $PAGE;
        $context = new stdClass();
        if ($this->is_dir()) {
            $context->url = new \surl($PAGE->get_url(), array('root' => $this->get_uniquehash()));
        } else {
            $context->url = $this->get_url();
        }
        $context->icon = $this->get_fa_icon_based_on_mime();
        $context->uniquehash = $this->get_uniquehash();
        $context->filename = $this->get_filename();
        $context->filenameshort = $this->get_filenameshort();
        $context->previewable = ($context->icon == 'file-image');
        $context->blank = $this->is_file();
        $context->type = $this->is_file() ? 'file' : 'dir';
        $parent = $this->get_parent();
        if ($parent) {
            $context->parenthash = $parent->get_uniquehash();
        } else {
            $context->parenthash = false;
        }
        return $context;
    }

    public function get_fa_icon_based_on_mime() {
        if ($this->is_dir()) {
            return 'folder';
        }

        $mime = $this->get_mimetype();
        switch ($mime) {
            case 'application/pdf':
                return 'file-pdf';
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            case 'application/msword':
                return 'file-word';
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'file-excel';
            case 'application/vnd.ms-powerpoint':
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return 'file-powerpoint';
            case 'image/jpeg':
            case 'image/x-citrix-jpeg':
            case 'image/png':
            case 'image/x-citrix-png':
            case 'image/x-png':
            case 'image/bmp':
            case 'image/gif':
                return 'file-image';
            case 'audio/mpeg':
                return 'file-audio';
        }
        return 'file';
    }

    public static function create_new_file_from_upload($filename, $tmpfilename, self $parent) {
        global $DB, $USER;

        if (!is_uploaded_file($tmpfilename)) {
            return false;
        }

        if ($parent == false || $parent->is_dir() == false) {
            return false;
        }

        $fileobj = new stdClass;
        $fileobj->filename = clean_param($filename, PARAM_FILENAME);
        $fileobj->type = 0;
        $fileobj->parent = $parent->get_id();

        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        $fileobj->mimetype = finfo_file($finfo, $tmpfilename);
        $fileobj->filesize = filesize($tmpfilename);

        $fileobj->uniquehash = 'UPDATE_ME';

        $fileobj->checksum = sha1_file($tmpfilename);
        $fileobj->lastmodified = time();
        $fileobj->modifiedby = $USER->id;
        $fs_filename = self::get_path_from_checksum($fileobj->checksum);
        if (!file_exists(dirname(dirname($fs_filename)))) {
            mkdir(dirname(dirname($fs_filename)));
        }

        if (!file_exists(dirname($fs_filename))) {
            mkdir(dirname($fs_filename));
        }

        if (file_exists($fs_filename) || move_uploaded_file($tmpfilename, $fs_filename)) {
            $fileobj->id = $DB->insert_record('file', $fileobj);
            return new self($fileobj);
        }

        return false;
    }

    private static function fs_readfie_buffered($file, $start, $end) {
        global $CFG;
        $buffersize = 1*(1024*1024);

        while(@ob_get_level()) {
            if (!@ob_end_flush()) {
                break;
            }
        }

        while ($start < $end) {
            fseek($file, $start, SEEK_SET);
            if ($start + $buffersize > $end) {
                $buffersize = $end - $start + 1;
            }
            echo fread($file, $buffersize);
            flush();
            $start += $buffersize;
        }

    }

    public function fs_readfile_full() {
        if ($this->deleted() || $this->is_dir() || !$this->fs_fileexists()) {
            return false;
        }
        return readfile($this->fs_getfilename());
    }

    public function fs_readfile_ranged($ranges) {
        if ($this->deleted() || $this->is_dir() || !$this->fs_fileexists()) {
            return false;
        }

        ini_set('zlib.output_compression', 'Off');

        $singlerange = count($ranges) == 1;
        $first = true;
        $contenttypeheader = 'Content-Type: ' . $this->get_mimetype();
        $boundary = get_http_boundary();

        if (!$singlerange) {
            header('Content-Type: multipart/byteranges; boundary=' . $boundary );
        }
        $file = fopen($this->fs_getfilename(), 'rb');
        foreach ($ranges as $range) {
            if (!$singlerange) {
                if (!$first) {
                    echo "\r\n";
                }
                echo '--' . $boundary . "\r\n";
                echo $contenttypeheader . "\r\n";
                echo 'Content-Range: bytes ' . $range->start . '-' . $range->end . '/' . $this->get_filesize() . "\r\n\r\n";
            } else {
                header($contenttypeheader);
                header('Content-Range: bytes ' . $range->start . '-' . $range->end . '/' . $this->get_filesize());
            }
            $first = false;
            if ($file) {
                self::fs_readfie_buffered($file, $range->start, $range->end);
            }
        }
        if (!$singlerange) {
            echo "\r\n--" . $boundary . "--\r\n";
        }
        if ($file) {
            fclose($file);
        }
        return true;

    }

    public function serve($forcedownload = false) {
        @session_write_close(); // close session (prevent browser from hanging when serving files...)
        @session_abort();

        $range = isset($_SERVER['HTTP_RANGE']) ? $_SERVER['HTTP_RANGE'] : null;
        if ($range) {
            $allowedrangeheader = 'Content-Range: bytes */' . $this->get_filesize();

            if (!preg_match('/^bytes= *\d*-\d*(, *\d*-\d*)*$/', $range)) {
                security::die_416('', [$allowedrangeheader]);
            }
            $range = substr($range, 6); // remove bytes=
            $ranges = explode(',', $range);

            $parsedranges = [];

            $filesize = $this->get_filesize() - 1;

            $totallength = 0;
            foreach ($ranges as $range) {
                $range = trim($range);

                list($start, $end) = explode('-', $range);

                $start = intval($start);

                $parsed = new stdClass;

                if ($start > $filesize) {
                    security::die_416('', [$allowedrangeheader]);
                }

                $parsed->start = $start;
                if (!empty($end)) {
                    $end = intval($end);
                    if ($end > $filesize || $end < $start) {
                        security::die_416('', [$allowedrangeheader]);
                    }
                    $parsed->end = $end;
                    $totallength += $end - $start + 1;
                } else {
                    $parsed->end = $filesize;
                    $totallength += $filesize - $start + 1;
                }
                $parsedranges[] = $parsed;
            }

            if (count($parsedranges) > 0) {
                header('HTTP/1.1 206 Partial Content');
                header('Content-Length: ' . $totallength);

                header('Accept-Ranges: bytes');
                header_remove('Cache-Control');
                header_remove('Pragma');
                header_remove('Expires');
                $this->fs_readfile_ranged($parsedranges);
                die;
            }
        }

        header('Content-Type: ' . $this->get_mimetype());
        if ($forcedownload) {
            header('Content-Disposition: attachment; filename="' . $this->get_filename() . '"');
        } else {
            header('Content-Disposition: inline; filename="' . $this->get_filename() . '"');
        }
        header('Content-Length: ' . $this->get_filesize());
        header('Accept-Ranges: bytes');
        $this->fs_readfile_full();
        die;
    }

    public static function create_from_hash($hash) {
        global $DB;
        $fileobj = $DB->get_record('file', array('uniquehash' => $hash));

        if ($fileobj) {
            return new self($fileobj);
        }
        return false;
    }

    public static function create_from_url($url) {
        global $CFG;
        $urlbase = $CFG->wwwroot . '/file.php?id=';
        $hash = str_replace($urlbase, '', $url);
        return self::create_from_hash($hash);
    }

    public static function get_root() {
        global $DB;
        $fileobj = $DB->get_record('file', array('id' => 1));

        if ($fileobj) {
            return new self($fileobj);
        }
        return false;
    }

    //TODO: convert file urls to relative urls in text
    //public static function
}