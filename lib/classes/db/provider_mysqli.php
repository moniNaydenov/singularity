<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 1:11 PM
 */

namespace core\db;

const IGNORE_MISSING = 1;
const MUST_EXIST = 2;

defined('INTERNAL') || die();

class provider_mysqli {
    private $mysqli;

    private $cache;

    private $dbname;

    public $debug = false;

    public static $FIELDTYPE_INT = 'INT';

    public static $ALLOWEDTYPES = array(
        self::FIELDTYPE_VARCHAR => 1,
        self::FIELDTYPE_INT => 1,
        self::FIELDTYPE_FLOAT => 1,
        self::FIELDTYPE_TEXT => 1
    );

    const FIELDTYPE_VARCHAR = 'VARCHAR';
    const FIELDTYPE_INT = 'INT';
    const FIELDTYPE_TEXT = 'TEXT';
    const FIELDTYPE_FLOAT = 'FLOAT';

    const DEFAULT_NULL = 0;
    const DEFAULT_NONE = 1;
    const DEFAULT_CUSTOM = 2;
    const DEFAULT_NOTNULL = 3;

    public function __construct() {
        global $CFG;
        if (!empty($this->mysqli)) {
            return;
        }

        $this->mysqli = new \mysqli($CFG->dbhost, $CFG->dbusername, $CFG->dbpassword, $CFG->dbname, $CFG->dbport);
        if ($this->mysqli->connect_errno) {
            raise_exception('Failed to connect to MySQL: (' . $this->mysqli->connect_errno . ') '. $this->mysqli->connect_error);
        }
        $this->mysqli->query('SET CHARACTER SET "utf8"');
        $this->cache = [];
        $this->dbname = $CFG->dbname;

        unset($CFG->dbhost);
        unset($CFG->dbport);
        unset($CFG->dbusername);
        unset($CFG->dbpassword);
        unset($CFG->dbname);
    }

    public function execute($sql) {
        $starttime = microtime(true);
        if ($this->debug) {
            echo $sql;

        }
        $result = $this->mysqli->query($sql);
        $endtime = microtime(true);
        if ($result == false) {
            raise_exception('DB error: ' . $this->mysqli->error);
        }
        if ($this->debug) {
            echo PHP_EOL . 'Execution time: ' . ($endtime - $starttime) . ' sec' . PHP_EOL;
        }
        return $result;
    }

    public function multi_execute($sql) {
        $starttime = microtime(true);
        if ($this->debug) {
            echo $sql;

        }
        $result = $this->mysqli->multi_query($sql);
        $endtime = microtime(true);
        if ($result == false) {
            raise_exception('DB error: ' . $this->mysqli->error);
        }
        do {
            $res = $this->mysqli->use_result();
            if ($res) {
                $res->close();
            }
        } while ($this->mysqli->next_result());
        if ($this->debug) {
            echo PHP_EOL . 'Execution time: ' . ($endtime - $starttime) . ' sec' . PHP_EOL;
        }
        return $result;
    }

    public function get_records($table, $filter = [], $fields = '*', $orderby = '', $rowcount = 0, $startfrom = 0) {
        $sql = "SELECT $fields FROM $table ";
        if (!empty($filter)) {
            $sql .= 'WHERE ' . $this->array_to_filter($filter);
        }
        if (!empty($orderby)) {
            $sql .= " ORDER BY $orderby";
        }
        if ($rowcount > 0) {
            $startfrom = intval($startfrom);
            $rowcount = intval($rowcount);
            $sql .= " LIMIT $startfrom, $rowcount";
        }

        $res = $this->execute($sql);
        return $this->res_to_objarray($res);
    }

    public function get_record($table, $filter = [], $fields = '*', $orderby = null, $strictness = IGNORE_MISSING) {
        $ret = $this->get_records($table, $filter, $fields, $orderby, 1);
        if (count($ret) == 0) {
            if ($strictness == MUST_EXIST) {
                raise_exception(get_string('recordnotfound'));
            }
            return null;
        }
        return reset($ret);
    }

    public function get_records_sql($sql,  $filter = [], $orderby = '', $rowcount = 0, $startfrom = 0) {

        $sql = $this->array_to_filter($filter, $sql);
        if (!empty($orderby)) {
            $sql .= " ORDER BY $orderby";
        }
        if ($rowcount > 0) {
            $startfrom = intval($startfrom);
            $rowcount = intval($rowcount);
            $sql .= " LIMIT $startfrom, $rowcount";
        }
        $res = $this->execute($sql);
        return $this->res_to_objarray($res);
    }

    public function get_record_sql($sql,  $filter = [], $orderby = null, $strictness = IGNORE_MISSING) {
        $ret = $this->get_records_sql($sql, $filter, $orderby);
        if (count($ret) == 0) {
            if ($strictness == MUST_EXIST) {
                raise_exception(get_string('recordnotfound'));
            }
            return null;
        }
        return reset($ret);
    }

    public function get_records_in($table, $field_in, $values_in, $filter = [], $fields = '*', $orderby = null, $rowcount = 0, $startfrom = 0) {
        $sql = "
            SELECT
                $fields
            FROM
                $table
            WHERE " . $this->compute_in($field_in, $values_in);
        if (!empty($filter)) {
            $sql .= 'AND ' . $this->array_to_filter($filter);
        }
        if (!empty($orderby)) {
            $sql .= " ORDER BY $orderby";
        }

        if ($rowcount > 0) {
            $startfrom = intval($startfrom);
            $rowcount = intval($rowcount);
            $sql .= " LIMIT $startfrom, $rowcount";
        }

        $res = $this->execute($sql);
        return $this->res_to_objarray($res);
    }

    public function get_records_notin($table, $field_in, $values_in, $filter = [], $fields = '*', $orderby = null, $rowcount = 0, $startfrom = 0) {
        $sql = "
            SELECT
                $fields
            FROM
                $table
            WHERE " . $this->compute_notin($field_in, $values_in);
        if (!empty($filter)) {
            $sql .= 'AND ' . $this->array_to_filter($filter);
        }
        if (!empty($orderby)) {
            $sql .= " ORDER BY $orderby";
        }

        if ($rowcount > 0) {
            $startfrom = intval($startfrom);
            $rowcount = intval($rowcount);
            $sql .= " LIMIT $startfrom, $rowcount";
        }

        $res = $this->execute($sql);
        return $this->res_to_objarray($res);
    }

    public function get_records_menu($table, $valuefield, $labelfield, $filter = [], $fields = '*', $orderby = null, $rowcount = 0, $startfrom = 0) {
        $records = $this->get_records($table, $filter, $fields, $orderby, $rowcount, $startfrom);
        $ret = [];
        foreach ($records as $id => $record) {
            if (!isset($record->{$valuefield}) || !isset($record->{$labelfield})) {
                raise_exception('DB error: missing value or label field in result');
            }
            $ret[$record->{$valuefield}] = $record->{$labelfield};
        }
        return $ret;
    }

    public function get_records_where($table, $wheresql, $wherefilter = [], $filter = [], $fields = '*', $orderby = null, $rowcount = 0, $startfrom = 0) {
        $filter1 = $this->array_to_filter($wherefilter, $wheresql);
        $filter2 = $this->array_to_filter($filter);
        $where = '';
        if (!empty($filter1)) {
            $where = ' WHERE ' . $filter1;
            if (!empty($filter2)) {
                $where .= ' AND ' .$filter2;
            }
        } else if (!empty($filter2)) {
            $where = ' WHERE ' . $filter2;
        }
        $sql = "
            SELECT
                $fields
            FROM
                $table
            $where ";
        if (!empty($orderby)) {
            $sql .= " ORDER BY $orderby";
        }

        if ($rowcount > 0) {
            $startfrom = intval($startfrom);
            $rowcount = intval($rowcount);
            $sql .= " LIMIT $startfrom, $rowcount";
        }

        $res = $this->execute($sql);
        return $this->res_to_objarray($res);
    }


    public function get_record_where($table, $wheresql, $wherefilter = [], $filter = [], $fields = '*', $orderby = null, $strictness = IGNORE_MISSING) {
        $ret = $this->get_records_where($table, $wheresql, $wherefilter, $filter, $fields, $orderby, 1);
        if (count($ret) == 0) {
            if ($strictness == MUST_EXIST) {
                raise_exception(get_string('recordnotfound'));
            }
            return null;
        }
        return reset($ret);
    }


    public function count($table, $field, $filter = []) { //legacy
        return $this->get_count($table, $field, $filter);
    }

    public function get_count($table, $field, $filter = []) {
        $sql = "
            SELECT
                '0',
                COUNT($field) AS count_records
            FROM
                $table ";
        if (!empty($filter)) {
            $sql .= 'WHERE ' . $this->array_to_filter($filter);
        }


        $res = $this->execute($sql);
        $counts = $this->res_to_objarray($res);
        return intval($counts[0]->count_records);
    }

    public function compute_in($field_in, $values_in, $notin = false) {

        $values_in_comp = '-1';
        if (!empty($values_in)) {
            $values_in_comp = [];
            foreach ($values_in as $val) {
                $values_in_comp[] = '"' . $this->mysqli->real_escape_string($val) . '"';
            }
            $values_in_comp = implode(',', $values_in_comp);
        }
        $ret = $field_in . ($notin ? ' NOT' : '') . ' IN (' . $values_in_comp . ')';
        return $ret;
    }

    public function compute_notin($field_in, $values_in) {
        return $this->compute_in($field_in, $values_in, true);
    }

    public function escape_value($value) {
        return $this->mysqli->real_escape_string($value);
    }

    public function update_records($table, $new = [], $filter = []) {
        $sql = "UPDATE $table SET ";

        if (!is_array($new)) {
            $new = (array)$new;
        }
        foreach ($new as $field => $value) {

            if (is_null($value)) {
                $new[$field] = $field . '=NULL';
            } else {
                $new[$field] = $field . '="' . $this->mysqli->real_escape_string($value) . '"';
            }
        }
        $sql .= implode(',', $new);
        if (!empty($filter)) {
            $sql .= 'WHERE ' . $this->array_to_filter($filter);
        }

        $res = $this->execute($sql);
        return $this->mysqli->affected_rows;
    }

    public function update_record($table, $record) {

        $params = (array)$record;
        if (!isset($params['id'])) {
            raise_exception('DB error: update record id not set');
        }
        $id = $params['id'];
        unset($params['id']);

        $columns = $this->get_columns($table);
        $clean = [];
        foreach ($params as $k => $v) {
            if (!isset($columns[$k])) {
                continue;
            }
            $clean[$k] = $v;
        }

        if (empty($clean)) {
            raise_exception('DB error: update record empty object given');
        }

        $sets = [];

        foreach ($clean as $field => $value) {
            if (is_null($value)) {
                $sets[] = $field . '=NULL';
            } else {
                $sets[] = $field . '="' . $this->mysqli->real_escape_string($value) . '"';
            }
        }
        $id = $this->mysqli->real_escape_string(strval($id));
        $sets = implode(',', $sets);
        $sql = 'UPDATE ' . $table . ' SET ' . $sets . ' WHERE id="' . $id . '"';

        $this->execute($sql);
        return true;
    }

    public function update_record_set($table, $set, $filter = []) {
        $sql = "UPDATE $table SET $set ";
        if (!empty($filter)) {
            $sql .= 'WHERE ' . $this->array_to_filter($filter);
        }

        if ($this->debug) {
            echo $sql;
        }
        if ($this->mysqli->query($sql) == false) {
            raise_exception('DB error in query: ' . $this->mysqli->error);
        }
        return $this->mysqli->affected_rows;
    }

    public function insert_record($table, $record, $returnid = true) {

        $params = (array)$record;

        $columns = $this->get_columns($table);
        $clean = [];
        foreach ($params as $k => $v) {
            if (!isset($columns[$k])) {
                continue;
            }
            $clean[$k] = $v;
        }

        if (empty($clean)) {
            raise_exception('DB error: insert record empty object given');
        }
        $fields = [];
        $values = [];

        foreach ($clean as $field => $value) {
            if (is_null($value)) {
                $values[] = 'NULL';
            } else {
                $values[] = '"' . $this->mysqli->real_escape_string($value) . '"';
            }
            $fields[] = $field;
        }
        $fields = implode(',', $fields);
        $values = implode(',', $values);
        $sql = "INSERT INTO $table ($fields) VALUES ($values)";
        $this->execute($sql);
        $id = @$this->mysqli->insert_id;

        if ($returnid) {
            return (int)$id;
        } else {
            return true;
        }
    }

    public function delete_record($table, $filter = []) {
        $sql = "DELETE FROM $table ";
        if (!empty($filter)) {
            $sql .= 'WHERE ' . $this->array_to_filter($filter);
        }
        $this->execute($sql);
        return $this->mysqli->affected_rows;

    }

    public function delete_record_where($table, $where, $filter = []) {
        $sql = sprintf('DELETE FROM %s WHERE %s', $table, $where);
        $sql = $this->array_to_filter($filter, $sql);
        $this->execute($sql);
        return $this->mysqli->affected_rows;
    }

    public function record_exists($table, $filter) {
        $records = $this->get_records($table, $filter);
        return !empty($records);
    }

    public function record_exists_sql($sql, $filter) {
        $records = $this->get_records_sql($table, $filter);
        return !empty($records);
    }

    //TODO: get_records_sql_menu

    public function get_columns($table) {
        global $CFG;
        if (isset($this->cache['structure']) && !empty($this->cache['structure'][$table])) {
            return $this->cache['structure'][$table];
        }
        $sql = "SELECT column_name, data_type, character_maximum_length, numeric_precision,
                       numeric_scale, is_nullable, column_type, column_default, column_key, extra
                  FROM information_schema.columns
                 WHERE table_name = '" . $table . "'
                       AND table_schema = '" . $this->dbname . "'
              ORDER BY ordinal_position";
        if ($this->debug) {
            echo $sql;
        }
        $result = $this->mysqli->query($sql);
        if ($result == false) {
            raise_exception('DB error: table ' . $table . ' not existing');
        }
        $columns = [];
        while ($row = $result->fetch_assoc()) {
            $columns[$row['column_name']] = array(
                'column_name' => $row['column_name'],
                'column_type' => $row['column_type']
            );
        }
        if (!isset($this->cache['structure'])) {
            $this->cache['structure'] = [];
        }
        $this->cache['structure'][$table] = $columns;
        return $columns;
    }

    public function array_to_filter($filter = [], $sql = null) {

        $newfilters = [];
        if (!empty($filter)) {
            foreach ($filter as $k => $v) {
                if (is_null($sql)) {
                    $newfilters[] = $k . '="' . $this->mysqli->real_escape_string($v) . '"';
                } else {
                    $k = ":$k";
                    $v = '"' . $this->mysqli->real_escape_string($v) . '"';
                    $sql = str_replace($k, $v, $sql);
                }

            }
        }

        $newfilters = implode(' AND ' , $newfilters);
        if (is_null($sql)) {
            $sql = $newfilters;
        }
        return $sql;
    }

    private function res_to_objarray($res) {
        $ret = [];
        while ($row = $res->fetch_assoc()) {
            $key = array_keys($row)[0];
            if (isset($ret[$row[$key]])) {
                raise_warning(sprintf('Warning! Non-unique value %s found', $row[$key]));
            }
            $ret[$row[$key]] = (object) $row;
        }
        return $ret;
    }

    public function lock_table($table, $write = true) {
        $mode = $write ? 'WRITE' : 'READ';
        $sql = sprintf('LOCK TABLES %s %s;', $table, $mode);
        $this->execute($sql);
    }

    public function unlock_tables() {
        $sql = 'UNLOCK TABLES;';
        $this->execute($sql);
    }

    public function start_transaction() {
        $sql = 'BEGIN';

        $this->execute($sql);
    }

    public function commit_transaction() {
        $sql = 'COMMIT';
        $this->execute($sql);
    }

    public function truncate_table($table) {
        $sql = sprintf('TRUNCATE %s', $table);
        $this->execute($sql);
        return true;
    }

    public function add_table_column($table, $columnname, $type, $length, $afterfield, $default, $defaultvalue = null, $autoincrement = false) {
        if (!isset(self::$ALLOWEDTYPES[$type])) {
            raise_exception('DB field type "' . $type . '" does not exist!');
        }
        if (is_null($afterfield)) {
            $afterfield = 'FIRST';
        } else {
            $afterfield = "AFTER `$afterfield`";
        }

        if ($default == self::DEFAULT_NONE) {
            $default = '';
        } else if ($default == self::DEFAULT_NULL) {
            $default = 'DEFAULT NULL';
        } else if ($default == self::DEFAULT_CUSTOM)  {
            $default = "DEFAULT '$defaultvalue'";
        } else if ($default == self::DEFAULT_NOTNULL)  {
            $default = 'NOT NULL';
        }

        if (is_null($length)) {
            $length = '';
        } else {
            $length = "($length)";
        }

        $sql = "ALTER TABLE `$table` ADD `$columnname` $type$length $default $afterfield";
        $this->execute($sql);;
    }

    public function drop_table_column($table, $columnname) {
        $sql = "ALTER TABLE `$table` DROP `$columnname`" ;
        $this->execute($sql);

    }

    public function create_table($table, $charset = 'utf8mb4', $collation = 'utf8mb4_general_ci', $engine = 'InnoDB') {
        $sql = "CREATE TABLE `$table` (`id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = $engine CHARSET=$charset COLLATE $collation;";
        $this->execute($sql);
    }

    //TODO
    public function add_primary_key($table, $columnname) {
        $sql = "ALTER TABLE `$table` ADD PRIMARY KEY (`$columnname`)";
        $this->execute($sql);
    }

    public function add_index($table, $columnname) {
        $indexname = 'idx' . $columnname;
        $sql = "ALTER TABLE `$table` ADD INDEX `$indexname` (`$columnname`)";
        $this->execute($sql);
    }

    public function set_debug($debug = false) {
        $this->debug = $debug == true;
    }
}

