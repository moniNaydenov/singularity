<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2018
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core;


/**
 * Class persistent
 * @package core
 */
abstract class persistent {
    /**
     * @var
     */
    protected $_obj;

    /**
     * @var
     */
    static $_dbtable;

    /**
     * persistent constructor.
     *
     * @param $obj
     */
    public function __construct($obj) {
        $this->_obj = $obj;
    }

    /**
     * @param $name
     *
     * @return null
     */
    public function __get($name) {
        if (property_exists($this->_obj, $name)) {
            return $this->_obj->{$name};
        }
        raise_warning('Object of type ' . static::class . ' does not have property ' . $name);
        return null;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value) {
        if (property_exists($this->_obj, $name)) {
            $this->_obj->{$name} = $value;
        } else {
            raise_warning('Object of type ' . static::class . ' does not have property ' . $name);
        }
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name) {
        return isset($this->_obj->{$name});
    }

    /**
     * @param $data
     */
    public function update_from_form($data) {
        foreach ((array)$data as $key => $value) {
            if ($key == 'id') {
                continue;
            }
            if (property_exists($this->_obj, $key)) {
                $this->_obj->{$key} = $value;
            }
        }
    }

    public function get_obj() {
        return $this->_obj;
    }

    public function delete_from_db() {
        global $DB;
        return $DB->delete_record(static::$_dbtable, array('id' => $this->_obj->id));
    }

    /**
     *
     */
    public function update_to_db() {
        global $DB;
        $DB->update_record(static::$_dbtable, $this->_obj);
    }

    public function save_to_db() {
        global $DB;
        $DB->update_record(static::$_dbtable, $this->_obj);
    }

    /**
     *
     */
    public function update_from_db() {
        global $DB;
        $this->_obj = $DB->get_record(static::$_dbtable, array('id' => $this->_obj->id));
    }

    public function export_for_template(\core\output\core $output) {
        return $this->_obj;
    }


    /**
     * @param $id
     *
     * @return null|static
     */
    public static function instance_from_dbid($id) {
        global $DB;
        $obj = $DB->get_record(static::$_dbtable, array('id' => $id));
        if ($obj) {
            return new static($obj);
        }
        return null;
    }

    /**
     * @param $obj
     *
     * @return null|static
     */
    public static function create_from_obj($obj) {
        global $DB;
        if (isset($obj->id)) {
            unset($obj->id);
        }
        $obj->id = $DB->insert_record(static::$_dbtable, $obj);
        if ($obj->id != 0) {
            return new static($obj);
        }
        return null;
    }

}