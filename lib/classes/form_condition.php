<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2018
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core;


use core\form_field\form_field;

class form_condition {
    private $_targetfield;
    private $_controlfield;
    private $_conrolvalue;
    private $_hide;

    private $_comparator;

    private $_allowedcomparators = array(
        'eq', 'neq', 'lt', 'lte', 'gt', 'gte', 'contains', 'containsnot', 'in', 'notin'
    );

    const VISIBLE_IF_EQUAL = 1;
    const HIDDEN_IF_EQUAL = 2;
    const VISIBLE_IF_DIFFERENT = 3;
    const HIDDEN_IF_DIFFERENT = 4;

    /**
     * @param $targetfield form_field that will be hidden
     * @param $controlfield form_field that is used for comparison
     * @param $comparator string comparison operation, can be 'eq', 'neq', 'lt', 'lte', 'gt', 'gte', 'contains', 'containsnot', 'in', 'notin'
     * @param $controlvalue string|int|float|array value to compare against
     * @param false $hide
     */
    public function __construct($targetfield, $controlfield, $comparator, $controlvalue, $hide = false) {
        $this->_targetfield = $targetfield;
        $this->_controlfield = $controlfield;
        $this->_conrolvalue = $controlvalue;
        if (!in_array($comparator, $this->_allowedcomparators)) {
            raise_exception("Comparator \"$comparator\" not allowed!");
        }
        $this->_comparator = $comparator;
        $this->_hide = $hide;
    }

    public function export_for_template(\core\output\core $output) {
        $data = new \stdClass;
        $data->targetfield = $this->_targetfield;
        $data->controlfield = $this->_controlfield;
        $data->controlvalue = $this->_conrolvalue;
        $data->comparator = $this->_comparator;
        $data->hide = $this->_hide;
        $context = new \stdClass;
        $context->data = json_encode($data, JSON_UNESCAPED_UNICODE);

        return $context;
    }

    public function __get($name) {
        if (isset($this->{'_' . $name})) {
            return $this->{'_' . $name};
        }
        raise_warning(get_class($this) . ' doesn\'t have property ' . $name);
        return '';
    }
}