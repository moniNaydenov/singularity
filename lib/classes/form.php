<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 9:16 PM
 */

namespace core;

use stdClass;
use \core\form_field;

defined('INTERNAL') || die();



class form {
    protected $_fields       = array();
    private   $_formfieldname;
    private   $_formdeffield;
    private   $_horizontal;
    private   $_inline;
    private   $_datasubmitted = false;
    private   $_submitted    = false;
    private   $_applied      = false;
    private   $_canceled     = false;
    private   $_submitfields = [];
    private   $_conditions = [];
    private   $_requiredfields_server = [];
    private   $_requiredfields_client = [];
    private   $_label_width = '3';
    private   $_field_width = '9';
    protected $options;


    const FORM_SUBMITTED = 12;
    const FORM_CANCELED = 14;
    const FORM_APPLIED = 16;

    const FIELD_REQUIRED_NONE = 0;
    const FIELD_REQUIRED_CLIENT = 10;
    const FIELD_REQUIRED_SERVER = 20;
    const FIELD_REQUIRED_BOTH = 30;

    public function __construct($options = array(), $horizontal = false) {
        $this->_formfieldname = '__sf__' . get_class($this);
        $this->add_field(new form_field\hidden('__sf__sessionid', sessionid(), PARAM_SESSIONID));
        $this->_formdeffield = new form_field\hidden($this->_formfieldname, 1, PARAM_INT);
        $this->add_field($this->_formdeffield);
        $this->options = $options;
        $this->_horizontal = $horizontal;
        $this->def();
    }

    protected function add_field(form_field\form_field $field) {
        if (isset($this->_fields[$field->get_name()])) {
            raise_warning('Field ' . $field->get_name() . ' already existing!');
        }
        $field->set_form($this);

        $this->_fields[$field->get_name()] = $field;
        if ($field instanceof form_field\submit) {
            $this->_submitfields[] = $field;
        }
    }

    public function set_values($values) {
        $this->set_data($values);
    }

    public function set_data($data) {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        foreach ($this->_fields as $fieldname => $field) {
            if (isset($data[$fieldname])) {
                $field->set_value($data[$fieldname]);
            }
        }
    }

    public function set_horizontal($hor) {
        $this->_horizontal = $hor == true ? true : false;
    }

    public function set_inline($inline) {
        $this->_inline = $inline == true ? true : false;
    }

    public function set_label_width($label_width) {
        $this->_label_width = $label_width;
    }

    public function set_field_width($field_width) {
        $this->_field_width = $field_width;
    }

    public function get_label_width() {
        return strval($this->_label_width);
    }

    public function get_field_width() {
        return strval($this->_field_width);
    }

    public function get_fields_names() {
        $fields = array_keys($this->_fields);
        array_shift($fields); //remove first two fields cause they are internal
        array_shift($fields);
        return $fields;
    }

    public function render(\core\output\core $output = null) {
        global $PAGE;
        if (is_null($output)) {
            $output = $PAGE->get_output();
        }
        $horizontal = $this->_horizontal ? 'horizontal' : '';
        $formtype = $this->_inline ? 'form-inline' : 'form-horizontal';
        $ret = "<form class='$formtype $horizontal' method='post' enctype='multipart/form-data'>";
        $hidden = '';
        $visible = '';
        foreach ($this->_fields as $field) {

            $hidden .= $field->hidden_render();
            $tmp = $field->render();
            if ($tmp === false) {
                $visible .= $field->render_from_template($output);
            } else {
                $visible .= $tmp;
            }

        }
        $ret .= $hidden . $visible . '</form>';
        $context = new stdClass;
        $context->form = $ret;
        $context->conditions = array();
        foreach ($this->_conditions as $condition) {
            $context->conditions[] = $condition->export_for_template($output);
        }

        return $output->render_from_template('form/form', $context);
    }

    public function get_data() {
        if (!$this->form_data_submitted()) {
            return false;
        }
        $count = 0;
        $ret = new stdClass;

        foreach ($this->_fields as $field) {
            if ($field->get_name() == '__sf__sessionid' || $field->get_name() == $this->_formfieldname) {
                continue;
            }
            $p = $field->get_data();
            if (!is_null($p)) {
                $count++;
                $ret->{$field->get_name()} = $p;
            }
        }
        if ($count > 0) {
            return $ret;
        }
        return false;
    }

    private function form_data_submitted() {
        if ($this->_datasubmitted) {
            return true;
        }

        if (is_null($this->_formdeffield->get_data())) {
            return false;
        }

        $sessionid = optional_param('__sf__sessionid', false, PARAM_SESSIONID);
        if (confirm_sessionid($sessionid) !== true) {
            raise_exception('invalid sessionid');
        }

        $this->_datasubmitted = true;

        return true;
    }

    public function is_submitted() {
        if ($this->_submitted) {
            return true;
        }

        if (!$this->form_data_submitted()) {
            return false;
        }

        foreach ($this->_submitfields as $field) {
            $r = $field->get_data();
            if ($r == self::FORM_SUBMITTED) {
                $this->_submitted = true;
                return true;
            }
        }

        return false;
    }

    public function is_applied() {
        if ($this->_applied) {
            return true;
        }

        if (!$this->form_data_submitted()) {
            return false;
        }

        foreach ($this->_submitfields as $field) {
            $r = $field->get_data();
            if ($r == self::FORM_APPLIED) {
                $this->_applied = true;
                return true;
            }
        }

        return false;
    }

    public function is_cancelled() {
        if ($this->_canceled) {
            return true;
        }

        if (!$this->form_data_submitted()) {
            return false;
        }

        foreach ($this->_submitfields as $field) {
            $r = $field->get_data();
            if ($r == self::FORM_CANCELED) {
                $this->_canceled = true;
                return true;
            }
        }

        return false;
    }

    public function add_submit_buttons($submit_label, $back_label = null, $apply_label = null) {
        $field = new form_field\submit('subm_btn', $submit_label, $back_label, $apply_label);
        $this->add_field($field);
        $this->_submitfields[] = $field;
    }


    public function set_submit_buttons_label_submit($label) {
        $buttons = reset($this->_submitfields);
        if ($buttons) {
            $buttons->set_label_submit($label);
        }
    }

    public function set_submit_buttons_label_cancel($label) {
        $buttons = reset($this->_submitfields);
        if ($buttons) {
            $buttons->set_label_cancel($label);
        }
    }

    public function set_submit_buttons_label_apply($label) {
        $buttons = reset($this->_submitfields);
        if ($buttons) {
            $buttons->set_label_apply($label);
        }
    }

    protected function add_condition(form_condition $condition) {
        if (!isset($this->_fields[$condition->targetfield])) {
            raise_warning('Target field "' . $condition->targetfield . '" doesn\'t exist!');
            return;
        }
        if (!isset($this->_fields[$condition->controlfield])) {
            raise_warning('Test field "' . $condition->controlfield . '" doesn\'t exist!');
            return;
        }
        $this->_conditions[] = $condition;
    }

    protected function hideif($targetfieldname, $controlfieldname, $comparator, $controlvalue) {
        $condition = new form_condition($targetfieldname, $controlfieldname, $comparator, $controlvalue, true);
        $this->add_condition($condition);
    }

    protected function disableif($targetfieldname, $controlfieldname, $comparator, $controlvalue) {
        $condition = new form_condition($targetfieldname, $controlfieldname, $comparator, $controlvalue, false);
        $this->add_condition($condition);
    }

    protected function set_required_field($fieldname, $requiretype = self::FIELD_REQUIRED_SERVER) {
        if (!isset($this->_fields[$fieldname])) {
            raise_warning('Target field "' . $fieldname. '" doesn\'t exist!');
            return;
        }

        if ($requiretype == self::FIELD_REQUIRED_NONE) {
            if (isset($this->_requiredfields_client[$fieldname])) {
                unset($this->_requiredfields_client[$fieldname]);
            }
            if (isset($this->_requiredfields_server[$fieldname])) {
                unset($this->_requiredfields_server[$fieldname]);
            }
        }
        if ($requiretype == self::FIELD_REQUIRED_SERVER || $requiretype == self::FIELD_REQUIRED_BOTH) {
            $this->_requiredfields_server[$fieldname] = true;
        }
        if ($requiretype == self::FIELD_REQUIRED_CLIENT || $requiretype == self::FIELD_REQUIRED_BOTH) {
            $this->_requiredfields_client[$fieldname] = true;
        }
    }

    protected function validation() {
        $errors = [];
        foreach ($this->_requiredfields_server as $fieldname => $t) {
            $field = $this->_fields[$fieldname];
            $data = $field->get_data();
            if (empty($data)) {
                $errors[$fieldname] = 'empty';
            }
        }

        return $errors;
    }

    protected function def() {

    }

}




//TODO: add radio group field