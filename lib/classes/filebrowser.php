<?php

namespace core;


use core\output\core;
use stdClass;

class filebrowser implements \core\output\renderable {
    private $_root;
    private $_browseonly;
    private $_ckfuncnum;
    private $_submittoform;
    private $_uploadform;
    private $_newdirform;
    private $_fileeditform;

    public function __construct(\core\file $root, $browseonly, $ckfuncnum, $submittoform, \core\form $uploadform, \core\form $newdirform, \core\form $fileeditform) {
        $this->_root = $root;
        $this->_browseonly = $browseonly;
        $this->_ckfuncnum = $ckfuncnum;
        $this->_submittoform = $submittoform;
        $this->_uploadform = $uploadform;
        $this->_newdirform = $newdirform;
        $this->_fileeditform = $fileeditform;
    }

    public function export_for_template(core $output) {
        global $PAGE;
        $context = new stdClass;
        $context->currentpath = $this->_root->get_path_as_string();
        $context->browseonly = $this->_browseonly;
        $context->ckfuncnum = $this->_ckfuncnum;
        $context->hasckfuncnum = strlen($this->_ckfuncnum) > 0;
        $context->submittoform = $this->_submittoform;
        $parent = $this->_root->get_parent();
        if ($parent) {
            $parenturl = new \surl($PAGE->get_url(), array('root' => $parent->get_uniquehash()));
            $context->parent = $parenturl->out();
        }

        $children = $this->_root->get_children();
        if (empty($children)) {
            $context->children = false;
        } else {
            $context->children = array();
            foreach ($children as $child) {
                $childcontext = $child->export_for_template($output);
                $context->children[] = $output->render_from_template('admin/file/item', $childcontext);//$renderer->render();
            }
        }

        $context->uploadform = $this->_uploadform->render($output);
        $context->newdirform = $this->_newdirform->render($output);
        $context->fileeditform = $this->_fileeditform->render($output);

        return $context;
    }

    public function get_template_name(core $output) {
        return 'admin/file/browser';
    }

}