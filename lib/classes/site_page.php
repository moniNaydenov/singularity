<?php


namespace core;

use stdClass;

class site_page {
    private $_sp;
    private $_nav;

    const PAGETYPE_DEFAULT = 'fullwidth';
    const PAGETYPE_FULLWIDTH = 'fullwidth';
    const PAGETYPE_FIXEDWIDTH_CENTER = 'fixedwidth_center';
    const PAGETYPE_FIXEDWIDTH_LEFT = 'fixedwidth_left';
    const PAGETYPE_FIXEDWIDTH_RIGHT = 'fixedwidth_right';

    public function __construct($sitepage) {
        $this->_sp = $sitepage;
        if ($this->_sp->navitemid != -1) {
            $this->_nav = \core\navigation\nav_node::create_from_id($this->_sp->navitemid);
        }
    }

    public function get_title() {
        return $this->_sp->title;
    }

    public function get_id() {
        return $this->_sp->id;
    }

    public function get_pageid() {
        return $this->_sp->pageid;
    }

    public function set_title($title) {
        $this->_sp->title = $title;
    }

    public function is_empty() {
        return strlen($this->_sp->structure) == 0;
    }

    public function get_used_blockids() {
        $allblocks = block\block::get_all_blockids_from_html($this->_sp->structure);
        return $allblocks;
    }

    public function set_page_metatags() {
        global $PAGE;
        if (!empty($this->_sp->metadescription)) {
            $PAGE->set_metadescription($this->_sp->metadescription);
        }
    }

    public function get_metadescription() {
        return $this->_sp->metadescription;
    }

    public function export_for_template(\core\output\core $output) {
        $context = new stdClass();
        $raw = $this->_sp->structure;
        $parsed = block\block::parse_allblocks_html($raw, $this->_sp->lang, $output);
        $context->content = $parsed;
        $context->css = $this->_sp->css;;
        $context->title = $this->get_title();
        $context->showtitle = $this->_sp->showtitle;
        $context->lang = $this->_sp->lang;
        $context->pageid = $this->_sp->pageid;
        $context->hascss = !empty($context->css);


        $pt = $this->_sp->pagetype;
        $context->pagetype = $pt;

        $context->pagetype_class = $this->get_pagetype_cssclass();

        if (!is_null($this->_nav)) {
            $navblock = new \core\block\nav_block($this->_nav);
            if ($navblock) {
                $context->nav = $output->render_block($navblock);
            }
        }
        return $context;
    }

    //TODO: think of a better name
    public function index_for_search(\core\output\core $output) {
        $context = $this->export_for_template($output);

        $url = new \surl('/page/' . $context->pageid);
        $title = $context->title;
        $lang = $context->lang;
        $content = preg_replace('/\s+/', ' ', strip_tags($context->content));

        $search = new \core\search\search();
        $search->index_page($url, $title, $lang, $content);
    }

    public function get_pagetype_cssclass() {
        $pt = $this->_sp->pagetype;
        $class = '';
        // deal with pagetypes - margins and container
        if ($pt != self::PAGETYPE_FULLWIDTH && $pt != self::PAGETYPE_DEFAULT) { // if page is not full-width, add container class
            $class .= ' container ';
        }
        if ($pt == self::PAGETYPE_FIXEDWIDTH_RIGHT) { // align page to the right
            $class .= ' ml-auto ';
        } else if ($pt == self::PAGETYPE_FIXEDWIDTH_CENTER) { // align page to the center
            $class .= ' mx-auto ';
        } // else keep it aligned to left
        return $class;
    }

    public function add_breadcrumb() {
        global $PAGE;
        if (!is_null($this->_nav)) {
            $this->_nav->fetch_children(-1);
            $activenode = $this->_nav->find_activenode();
            if ($activenode) {
                $parent = $activenode->get_parentnode();
                $breadcrumbs = array();
                while (!is_null($parent)) {
                    $breadcrumb = new stdClass;
                    $breadcrumb->title = $parent->get_title();
                    $breadcrumb->url = $parent->get_url();
                    array_unshift($breadcrumbs, $breadcrumb);
                    $parent = $parent->get_parentnode();
                }

                foreach ($breadcrumbs as $breadcrumb) {

                    $PAGE->add_breadcrumb($breadcrumb);
                }

                $PAGE->add_breadcrumb($activenode->get_title(), $activenode->get_url());
            }
        } else {
            $PAGE->add_breadcrumb($this->get_title());
        }

    }

    public static function create_from_id($id) {
        global $DB;
        $sp = $DB->get_record('site_page', array('id' => $id));
        if ($sp != false) {
            return new self($sp);
        }
        return false;
    }

    /**
     * @deprecated
     * @param $pageid
     * @param $lang
     * @return site_page|false
     *
     */
    public static function create_from_site_pageid_lang($pageid, $lang) {
        global $DB;
        raise_warning('Warning! Deprecated function create_from_site_pageid_lang! Use create_from_pageid_lang instead!');
        $sp = $DB->get_record('site_page', array(
            'pageid' => $pageid,
            'lang' => $lang
        ));
        if ($sp != false) {
            return new self($sp);
        }
        return false;
    }

    public static function create_from_pageid_lang($pageid, $lang) {
        global $DB;
        $sp = $DB->get_record('site_page', array(
            'pageid' => $pageid,
            'lang' => $lang
        ));
        if ($sp != false) {
            return new self($sp);
        }
        return false;
    }


    public static function get_available_pagetypes() {
        global $CFG;
        $pagetypes = array(
            self::PAGETYPE_DEFAULT => get_string('standard'),
            self::PAGETYPE_FULLWIDTH => get_string('fullwidth', 'admin'),
            self::PAGETYPE_FIXEDWIDTH_CENTER => get_string('fixedwidth:center', 'admin'),
            self::PAGETYPE_FIXEDWIDTH_LEFT => get_string('fixedwidth:left', 'admin'),
            self::PAGETYPE_FIXEDWIDTH_RIGHT => get_string('fixedwidth:right', 'admin'),
        );
        $sitefunction = 'site_extend_site_page_types';

        if (function_exists($sitefunction)) {
            $pagetypes = array_merge($pagetypes, $sitefunction());
        }

        return $pagetypes;
    }

    public static function get_bootstrap_css_attributes() {
        $attributes = ['p' => 'padding', 'm' => 'margin'];
        $sizes = ['', 'sm', 'md', 'lg', 'xl'];
        $values = ['0', '1', '2', '3', '4', '5'];
        $sides = ['' => 'all', 't' => 'top', 'b' => 'bottom', 'l' => 'left', 'r' => 'right', 'x' => 'horizontal', 'y' => 'vertical'];

        $ret = [];
        foreach ($attributes as $abbrattr => $attr) {
            foreach ($sides as $abbrside => $side) {
                $obj = [
                    'name' => $attr . ' - ' . $side,
                    'items' => []
                ];
                foreach ($sizes as $size) {
                    $items = [];
                    foreach ($values as $value) {
                        if (strlen($size) == 0) {
                            $items[] = "$abbrattr$abbrside-$value";
                        } else {
                            $items[] = "$abbrattr$abbrside-$size-$value";
                        }
                    }

                    $obj['items'][] = $items;
                }
                $ret[] = $obj;
            }
        }
        return $ret;
    }
}