<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;

use stdClass;

class nav_block extends block {

    protected static $_tablename = 'nav_item';

    public function export_for_template(\core\output\core $output) {
        global $DB;
        $this->_obj->fetch_children(-1); //maximum depth of three TODO: to be "modifieble" from {{{#..}}} tag
        $context = $this->_obj->export_for_template($output);
        $context->id = $this->_obj->get_id();
        return $context;
    }

    public function export_for_api() {

        $context = new stdClass;
        $context->id = $this->_obj->get_id();
        $context->blockid = $this->_obj->get_title();
        $context->lang = $this->_obj->get_lang();
        return $context;
    }


    public function get_template_name() {
        return 'blocks/nav_block';
    }

    public function get_admin_template_name() {
        return 'admin/page/editor/blocks/nav_block';
    }


    public static function get_all_instances($lang) {
        global $DB;
        $records = $DB->get_records(static::$_tablename, array('lang' => $lang), '*', 'parent ASC, sortorder ASC');
        $collection = array();
        foreach ($records as $record) {
            $block = static::create_from_id($record->id);
            if ($block) {
                $collection[] = $block;
            }
        }
        return $collection;
    }

    public static function support_new_block() {
        return false;
    }

    public static function create_from_id($id) {

        $nav = \core\navigation\nav_node::create_from_id($id);
        if ($nav) {
            $nav_block = new self($nav);
            return $nav_block;
        }
        return null;
    }


    public static function parse_html($html, $lang, \core\output\core $output) {
        global $CFG;

        $pattern = '/(\{\{\{#nav_block\}\}\}([^{]+)\{\{\{\/nav_block\}\}\})/';

        $repl = array();
        if (preg_match_all($pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $hb = self::create_from_id($matches[2][$i]);
                if ($hb != false) {
                    $repl[$match] = $output->render_block($hb); //replace all {{{#html_block}}}...{{{/html_block}}} block content
                }
            }
        }
        $html = str_replace(array_keys($repl), array_values($repl), $html);
        return $html;
    }
}