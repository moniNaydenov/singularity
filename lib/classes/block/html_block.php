<?php
/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;

use stdClass;

class html_block extends block {
    private static $_pattern = '/(\{\{\{#html_block\}\}\}([^{]+)\{\{\{\/html_block\}\}\})/';
    protected static $_tablename = 'html_block';

    public function export_for_template(\core\output\core $output) {
        $context = new \stdClass();
        $context->content = $this->_obj->content;
        $context->css = $this->_obj->css;
        $context->hascss = !empty($context->css);
        $context->id = $this->_obj->id;
        $context->blockid = $this->_obj->blockid;
        $context->lang = $this->_obj->lang;
        return $context;
    }

    public function export_for_api() {
        $context = new \stdClass();
        $context->id = $this->_obj->id;
        $context->blockid = $this->_obj->blockid;
        $context->lang = $this->_obj->lang;
        return $context;
    }


    public function get_template_name() {
        return 'blocks/html_block';
    }

    public function get_admin_template_name() {
        return 'admin/page/editor/blocks/html_block';
    }

    /**
     * has to be implemented from child classes
     */
    public static function create_new($blockid, $lang) {
        global $USER, $DB;
        if (empty($blockid)) {
            return null;
        }
        $block = static::create_from_blockid_and_lang($blockid, $lang);
        if ($block) {
            return $block;
        }
        $obj = new stdClass;
        $obj->lang = $lang;
        $obj->blockid = $blockid;
        $obj->content = '<p>EDIT ME</p>';
        $obj->css = '';
        $obj->modifiedby = $USER->id;
        $obj->lastmodified = time();
        $obj->id = $DB->insert_record(static::$_tablename, $obj);
        return new static($obj);
    }

    public static function parse_html($html, $lang, \core\output\core $output) {
        global $CFG;


        $repl = array();
        if (preg_match_all(self::$_pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $hb = self::create_from_blockid_and_lang($matches[2][$i], $lang);
                if ($hb) {
                    $repl[$match] = $output->render_block($hb); //replace all {{{#html_block}}}...{{{/html_block}}} block content
                }
            }
        }
        $html = str_replace(array_keys($repl), array_values($repl), $html);
        return $html;
    }


    public static function get_blockids_from_html($html) {
        global $CFG;
        $allblocks = array();
        if (preg_match_all(self::$_pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $hb = self::create_from_blockid_and_lang($matches[2][$i], $CFG->lang);
                if ($hb != false) {
                    $allblocks[] = $matches[2][$i];
                }
            }
        }
        return array('html_block' => $allblocks);
    }

}