<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;


abstract class custom_block extends block {

    private static $_namespaces = array();

    public function __construct() {
        parent::__construct(null);

    }


    public function export_for_api() {
        global $CFG;
        $context = new \stdClass();
        $context->id = $this->get_block_type();
        $context->blockid = $this->get_block_type();
        $context->lang = $CFG->lang;
        return $context;
    }

    public function export_for_template(\core\output\core $output) {
        $context = new \stdClass;
        $context->blocktype = $this->get_block_type();
        $context->uniqid =  'custom_block-' . $this->get_block_type() . '-' . substr(md5(strval(rand(1, 1000000))), 0, 16);
        return $context;
    }

    public function get_admin_template_name() {
        return 'admin/page/editor/blocks/custom_block';
    }


    public function get_block_type() {
        return 'custom_block';
    }

    private static function get_namespaces() {
        global $CFG, $SITE;
        if (empty(self::$_namespaces)) {
            self::$_namespaces = array(
                $CFG->libroot . '/classes/block/custom_block/' => '\\core\\block\\custom_block\\',
                $SITE->dirroot . '/classes/custom_block/' => '\\site\\custom_block\\'
            );

        }
        return self::$_namespaces;
    }


    public static function support_new_block() {
        return false;
    }

    public static function create_from_id($blocktype) { //blocktype instead of id

        foreach (self::get_namespaces() as $namespace) {
            $classname = $namespace . $blocktype;
            if (class_exists($classname)) {
                $cb = new $classname;
                return $cb;
            }
        }
        return null;
    }

    public static function get_all_instances($lang) {
        global $CFG, $SITE;
        $invalid = array('.', '..');
        $instances = array();
        foreach (self::get_namespaces() as $location => $namespace) {

            $files = scandir ( $location);
            foreach ($files as $f) {
                if (in_array($f, $invalid) || strpos($f, '.php') === false) {
                    continue;
                }
                $type = str_replace('.php', '', $f);
                $classname = $namespace . $type;
                if (class_exists($classname)) {
                    $instance = new $classname();
                    $instances[] = $instance;
                }
            }
        }
        return $instances;

    }

    public static function parse_html($html, $lang, \core\output\core $output) {
        global $CFG;

        if ($output instanceof \core\output\admin) {
            return $html;
        }

        $pattern = '/(\{\{\{#custom_block\}\}\}([^{]+)\{\{\{\/custom_block\}\}\})/';

        $repl = array();
        if (preg_match_all($pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $cb = self::create_from_id($matches[2][$i]);
                if ($cb) {
                    $repl[$match] = $output->render_from_template($cb->get_template_name(), $cb->export_for_template($output)); //replace all {{{#custom_block}}}...{{{/custom_block}}} block content
                }
            }
        }
        $html = str_replace(array_keys($repl), array_values($repl), $html);
        return $html;
    }

}