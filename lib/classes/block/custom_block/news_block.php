<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block\custom_block;

use core\block\custom_block;
use stdClass;

class news_block extends custom_block {

    public function get_block_type() {
        return 'news_block';
    }

    public function export_for_template(\core\output\core $output) {
        global $DB, $PAGE;
        $context = parent::export_for_template($output);

        $pageid = optional_param('page', 1, PARAM_INT);
        if ($pageid < 1) {
            $pageid = 1;
        }
        $perpage = 3; //TODO - make it a param somehow

        $startnum = ($pageid - 1) * $perpage;
        $sql = <<<SQL
SELECT
    n.*,
    sp.title,
    sp.lang
FROM
    news n
JOIN
    site_page sp ON sp.id = n.sitepageid
WHERE
    n.visible = 1
ORDER BY 
    n.timestamp DESC
LIMIT 
    $startnum,$perpage
SQL;

        $news = $DB->get_records_sql($sql);

        $pageurl = new \surl($PAGE->url);

        $context->news = array_values($news);
        if ($pageid > 1) {
            $pageurl->param('page', strval($pageid - 1));
            $context->pageprev = $pageurl->out();
        }

        $allnewscount = $DB->count('news', 'id', ['visible' => 1]);
        if ($allnewscount > ($pageid + 1) * $perpage) {
            $pageurl->param('page', strval($pageid + 1));
            $context->pagenext = $pageurl->out();
        }
        return $context;
    }

    public function get_template_name() {
        return 'blocks/news_block';
    }

}