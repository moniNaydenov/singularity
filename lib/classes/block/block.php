<?php
/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;


abstract class block {
    protected static $_tablename = '4353';
    protected $_obj;

    public function __construct($obj) {
        $this->_obj = $obj;
    }

    public function render_content(\core\output\core $output) {

    }

    public abstract function get_template_name();

    public abstract function get_admin_template_name();


    public function export_for_api() {
        $context = new \stdClass();
        $context->id = $this->_obj->id;
        $context->blockid = $this->_obj->blockid;
        $context->lang = $this->_obj->lang;
        return $context;
    }

    public function export_for_template(\core\output\core $output) {
        $context = new \stdClass();
        return $context;
    }

    public static function support_new_block() {
        return true;
    }

    public static function get_all_instances($lang) {
        global $DB;
        $records = $DB->get_records(static::$_tablename, array('lang' => $lang));
        $collection = array();
        foreach ($records as $record) {
            $collection[] = new static($record);
        }
        return $collection;
    }

    public static function get_all_blocktypes() {
        $files = scandir ( __DIR__);
        $blocktypes = array();
        $invalid = array('.', '..', 'block.php');
        foreach ($files as $f) {
            if (in_array($f, $invalid) || strpos($f, '.php') === false) {
                continue;
            }
            $type = str_replace('.php', '', $f);
            $block = new \stdClass;

            $block->type = $type;
            $block->name = get_string('block:' . $type, 'admin');
            $block->classname = '\\core\\block\\' . $type;
            $blocktypes[] = $block;
        }
        return $blocktypes;
    }


    /**
     * has to be implemented from child classes
     */
    public static function create_new($blockid, $lang) {
        $block = static::create_from_blockid_and_lang($blockid, $lang);
        if ($block) {
            return $block;
        } else {
            return null;
        }
    }

    public static function create_from_id($id) {

        global $DB;

        $obj = $DB->get_record(static::$_tablename, array('id' => $id));
        if ($obj) {
            return new static($obj);
        }
        return null;
    }

    public static function create_from_blockid_and_lang($blockid, $lang) {
        global $DB;

        $obj = $DB->get_record(static::$_tablename, array(
            'blockid' => $blockid,
            'lang' => $lang
        ));
        if ($obj) {
            return new static($obj);
        }
        return null;
    }

    public static function parse_allblocks_html($html, $lang, \core\output\core $output) {
        $blocktypes = self::get_all_blocktypes();
        foreach ($blocktypes as $blocktype) {
            if (class_exists($blocktype->classname)) {
                $classname = $blocktype->classname;
                $html = $classname::parse_html($html, $lang, $output);
            }
        }
        return $html;
    }

    public static function parse_html($html, $lang, \core\output\core $output) {
        return $html;
    }

    public static function get_all_blockids_from_html($html) {
        $allblocks = array();
        $blocktypes = self::get_all_blocktypes();
        foreach ($blocktypes as $blocktype) {
            if (class_exists($blocktype->classname)) {
                $classname = $blocktype->classname;
                $blocks = $classname::get_blockids_from_html($html);
                $allblocks = $allblocks + $blocks;
            }
        }
        return $allblocks;
    }

    public static function get_blockids_from_html($html) {
        return array();
    }
}