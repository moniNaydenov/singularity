<?php
/**
 *
 * @package       esmos
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;

use stdClass;

class gallery_block extends block {
    protected $_gallery;

    private static $_pattern = '/(\{\{\{#gallery_block\}\}\}([^{]+)\{\{\{\/gallery_block\}\}\})/';
    protected static $_tablename = 'gallery';

    public function __construct($gallery) {
        parent::__construct($gallery);
        $this->_gallery = new \core\gallery\gallery($gallery);
    }

    public function export_for_template(\core\output\core $output) {
        $context = $this->_gallery->export_for_template($output);
        return $context;
    }

    public function export_for_api() {
        global $CFG;
        $context = new \stdClass();
        $context->id = $this->_obj->id;
        $context->blockid = $this->_obj->shortname;
        $context->lang = '-';
        return $context;
    }


    public function get_template_name() {
        return 'blocks/gallery_block';
    }

    public function get_admin_template_name() {
        return 'admin/page/editor/blocks/gallery_block';
    }

    public static function support_new_block() {
        return false;
    }

    public static function get_all_instances($lang) {
        global $DB;
        $galleries = \core\gallery\gallery::get_all_galleries();
        $collection = [];
        foreach ($galleries as $gallery) {
            $collection[] = new static($gallery);
        }
        return $collection;
    }

    public static function parse_html($html, $lang, \core\output\core $output) {
        global $CFG;


        $repl = array();
        if (preg_match_all(self::$_pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $gb = self::create_from_blockid_and_lang($matches[2][$i], $lang);
                if ($gb) {
                    $repl[$match] = $output->render_block($gb);
                }
            }
        }
        $html = str_replace(array_keys($repl), array_values($repl), $html);
        return $html;
    }

    public static function create_from_blockid_and_lang($blockid, $lang) {
        global $DB;

        $obj = $DB->get_record(static::$_tablename, array(
            'shortname' => $blockid
        ));
        if ($obj) {
            return new static($obj);
        }
        return null;
    }

    public static function get_blockids_from_html($html) {
        global $CFG;
        $allblocks = array();
        if (preg_match_all(self::$_pattern, $html, $matches) > 0) {
            foreach ($matches[0] as $i => $match) {
                $hb = self::create_from_blockid_and_lang($matches[2][$i], $CFG->lang);
                if ($hb != false) {
                    $allblocks[] = $matches[2][$i];
                }
            }
        }
        return array('gallery_block' => $allblocks);
    }

}