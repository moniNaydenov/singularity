<?php
/**
 *
 * @package       avo_nw
 * @author        Simeon Naydenov (moniNaydenov@gmail.com)
 * @copyright     2017
 * @license       http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace core\block;

use stdClass;

class inline_block extends block {

    protected static $inlineblocks = null;

    public function export_for_api() {
        $context = clone $this->_obj;
        return $context;
    }


    public function export_for_template(\core\output\core $output) {
        $context = new \stdClass();
        $context->content = $this->_obj->content;
        return $context;
    }


    public function get_template_name() {
        return 'blocks/inline_block';
    }

    public function get_admin_template_name() {
        return 'admin/page/editor/blocks/inline_block';
    }


    public static function support_new_block() {
        return false;
    }

    public static function create_from_id($id) {
        global $CFG;
        $id = intval($id);
        $instances = self::get_all_instances($CFG->lang);

        $count = count($instances);
        for ($i = 1; $i <= $count; $i++) {
            if ($id == $i) {
                return $instances[$i-1];
            }
        }
        return null;
    }

    public static function get_all_instances($lang) {
        global $CFG;
        if (empty(static::$inlineblocks)) {
            static::$inlineblocks = json_decode(file_get_contents($CFG->libroot . '/inlineblocks.json'));
        }
        $i = 1;
        $collection = array();
        foreach (static::$inlineblocks as $inlineblock) {
            $obj = new stdClass;
            $obj->id = $i;
            $obj->blockid = $inlineblock->name;
            $obj->lang = $lang;
            $obj->content = $inlineblock->sampleContent;
            $collection[] = new inline_block($obj);

            $i++;
        }
        return $collection;
    }

}