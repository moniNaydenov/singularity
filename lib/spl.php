<?php



defined('INTERNAL') || die();

// setup autoloading classes
spl_autoload_register(function ($class) {
    $namespaces = explode('\\', $class);

    $dirroot = dirname(__DIR__) . '/'; // siteroot
    switch ($namespaces[0]) {
        case 'core':
            $namespaces[0] = 'classes';
            $dirroot .= 'lib/'; // /lib/classes/...
            break;
        case 'site':
            $namespaces[0] = 's/classes'; // /s/classes/...
            break;
        case 'surl':
            $dirroot .= 'lib/classes/'; // /lib/classes/surl.php
            break;
        case 'ScssPhp':
            $dirroot .= 'lib/';
            $namespaces[0] = 'scssphp';
            $namespaces[1] = 'src';
            break;
        default:
            //echo 'namespace ' . $namespaces[0] . ' not defined yet';
            return;
    }
    $path = $dirroot . implode('/', $namespaces);
    $path .= '.php';
    if (file_exists($path)) {
        include_once($path);
    }

});
