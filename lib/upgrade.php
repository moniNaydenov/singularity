<?php


defined('INTERNAL') || die;


use \core\db\provider_mysqli;

function upgrade_singularity() {
    global $CFG, $DB, $PAGE;


    $oldconfigs = $DB->get_records_sql('SELECT * FROM config WHERE site IS NULL');
    if (count($oldconfigs) > 0) {
        foreach ($oldconfigs as $oldconfig) {
            $oldconfig->site = 'core';
            $DB->update_record('config', $oldconfig);
        }
    }
    $oldsiteconfigs = $DB->get_records_sql('SELECT * FROM config WHERE site != \'core\'');

    if (count($oldsiteconfigs) > 0) {
        foreach ($oldsiteconfigs as $oldsiteconfig) {
            $oldsiteconfig->site = 'site';
            $DB->update_record('config', $oldsiteconfig);
        }
    }

    $oldversion = get_config('singularityversion', 0, 'core');
    $dbversion = get_config('version', 0, 'core');



    if ($dbversion < $oldversion) {
        $dbversion = $oldversion;
        set_config('version', $dbversion);
        unset_config('singularityversion', 'core');
    }

    if ($dbversion == 0) { // set first singularity version
        set_config('version', 0, 'core');
    }


    if ($dbversion < 2018032601) {
        $DB->add_table_column('config', 'site', \core\db\provider_mysqli::FIELDTYPE_VARCHAR, 32, 'name', \core\db\provider_mysqli::DEFAULT_NULL);

        upgrade_savepoint(true, 2018032601, 'core');
    }

    if ($dbversion < 2018070900) {
        $DB->add_table_column('site_page', 'showtitle', \core\db\provider_mysqli::FIELDTYPE_INT, 1, 'title', \core\db\provider_mysqli::DEFAULT_NONE);
        upgrade_savepoint(true, 2018070900, 'core');
    }


    if ($dbversion < 2018070901) {
        $DB->update_record_set('site_page', 'showtitle=1');
        upgrade_savepoint(true, 2018070901, 'core');
    }

    if ($dbversion < 2018070903) {
        $DB->execute('UPDATE site_page SET structure=REPLACE(structure, "spe-col", "")');
        $DB->execute('UPDATE site_page SET structure=REPLACE(structure, "spe-row", "")');
        upgrade_savepoint(true, 2018070903, 'core');
    }

    if ($dbversion < 2019091900) {
        $DB->create_table('gallery');
        $DB->add_table_column('gallery', 'shortname', provider_mysqli::FIELDTYPE_VARCHAR, 64, 'id', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery', 'title', provider_mysqli::FIELDTYPE_TEXT, null, 'shortname', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery', 'type', provider_mysqli::FIELDTYPE_VARCHAR, 16, 'title', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery', 'lastmodified', provider_mysqli::FIELDTYPE_INT, 10, 'type', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery', 'modifiedby', provider_mysqli::FIELDTYPE_INT, 10, 'lastmodified', provider_mysqli::DEFAULT_NOTNULL);
        $DB->create_table('gallery_item');
        $DB->add_table_column('gallery_item', 'galleryid', provider_mysqli::FIELDTYPE_INT, 10, 'id', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery_item', 'sortorder', provider_mysqli::FIELDTYPE_INT, 10, 'galleryid', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery_item', 'title', provider_mysqli::FIELDTYPE_VARCHAR, 64, 'sortorder', provider_mysqli::DEFAULT_NULL);
        $DB->add_table_column('gallery_item', 'image', provider_mysqli::FIELDTYPE_VARCHAR, 256, 'title', provider_mysqli::DEFAULT_NULL);
        $DB->add_table_column('gallery_item', 'description', provider_mysqli::FIELDTYPE_TEXT, null, 'image', provider_mysqli::DEFAULT_NULL);
        $DB->add_table_column('gallery_item', 'lastmodified', provider_mysqli::FIELDTYPE_INT, 10, 'description', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('gallery_item', 'modifiedby', provider_mysqli::FIELDTYPE_INT, 10, 'lastmodified', provider_mysqli::DEFAULT_NOTNULL);
        upgrade_savepoint(true, 2019091900, 'core');
    }

    if ($dbversion < 2021032700) {
        $DB->drop_table_column('site_page', 'site');
    }

    if ($dbversion < 2021032701) {
        $DB->create_table('search_index');
        $DB->add_table_column('search_index', 'url', provider_mysqli::FIELDTYPE_VARCHAR, 255, 'id', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('search_index', 'title', provider_mysqli::FIELDTYPE_VARCHAR, 255, 'url', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('search_index', 'lang', provider_mysqli::FIELDTYPE_VARCHAR, 10, 'title', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('search_index', 'content', provider_mysqli::FIELDTYPE_TEXT, null, 'lang', provider_mysqli::DEFAULT_NOTNULL);
    }

    if ($dbversion < 2021032702) {
        // index current site pages
        $sitepages = $DB->get_records('site_page');
        $output = $PAGE->get_output();
        foreach ($sitepages as $sitepage) {
            $sp = new \core\site_page($sitepage);
            $sp->index_for_search($output);
        }
        upgrade_savepoint(true, 2021032702, 'core');
    }
    if ($dbversion < 2022031500) {
        upgrade_savepoint(true, 2022031500, 'core');
    }

    if ($dbversion < 2022031501) {
        $DB->add_table_column('gallery_item', 'link', provider_mysqli::FIELDTYPE_VARCHAR, 512, 'description', provider_mysqli::DEFAULT_NULL);
        upgrade_savepoint(true, 2022031501, 'core');
    }
    if ($dbversion < 2023051800) {
        $DB->create_table('user_role');
        $DB->add_table_column('user_role', 'userid', provider_mysqli::FIELDTYPE_VARCHAR, 255, 'id', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('user_role', 'role', provider_mysqli::FIELDTYPE_VARCHAR, 32, 'userid', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('user_role', 'activeafter', provider_mysqli::FIELDTYPE_INT, 10, 'role', provider_mysqli::DEFAULT_CUSTOM, 0);
        $DB->add_table_column('user_role', 'activebefore', provider_mysqli::FIELDTYPE_INT, 10, 'activeafter', provider_mysqli::DEFAULT_CUSTOM, 0);
        $DB->add_table_column('user_role', 'lastmodified', provider_mysqli::FIELDTYPE_INT, 10, 'activebefore', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_table_column('user_role', 'modifiedby', provider_mysqli::FIELDTYPE_INT, 10, 'lastmodified', provider_mysqli::DEFAULT_NOTNULL);
        $DB->add_index('user_role', 'userid');

        $DB->add_table_column('user', 'disabled', provider_mysqli::FIELDTYPE_INT, 1, 'password', provider_mysqli::DEFAULT_CUSTOM, 0);
        $DB->add_table_column('user', 'firstname', provider_mysqli::FIELDTYPE_VARCHAR, 64, 'disabled', provider_mysqli::DEFAULT_NULL);
        $DB->add_table_column('user', 'lastname', provider_mysqli::FIELDTYPE_VARCHAR, 64, 'firstname', provider_mysqli::DEFAULT_NULL);
        $DB->add_table_column('user', 'email', provider_mysqli::FIELDTYPE_VARCHAR, 128, 'lastname', provider_mysqli::DEFAULT_NULL);


        $users = $DB->get_records('user');
        $rolerecord = new stdClass();
        $rolerecord->role = 'admin';
        $rolerecord->activeafter = 0;
        $rolerecord->activebefore = 0;
        $rolerecord->lastmodified = time();
        $rolerecord->modifiedby = 0;
        foreach ($users as $user) {
            if ($user->username == 'guest' || $user->id == 999) {
                continue; // guest users should not receive a role
            }
            $rolerecord->userid = $user->id;
            $DB->insert_record('user_role', $rolerecord);
        }
        upgrade_savepoint(true, 2023051800, 'core');
    }
}

function upgrade_site() {
    global $SITE, $CFG;

    $dbversion = get_config('version', 0, 'site');
    if ($dbversion < $SITE->version) {
        $fname = 'site_upgrade';
        if (function_exists($fname)) {
            $fname($dbversion);
        }
    }

}

function upgrade_savepoint($status, $version, $site) {
    if (is_null($site) || $site == 'core') {
        if (!$status) {
            raise_exception('Unable to update singularity to version '. $version);
            return;
        }
        set_config('version', $version);
    } else {
        if (!$status) {
            raise_exception('Unable to update site ' . $site . ' to version '. $version);
            return;
        }
        set_config('version', $version, $site);

    }
}


function upgrade_pending() {
    global $CFG;
    $dbversion = get_config('version', 0);
    if ($dbversion < $CFG->version) {
        return true;
    }
    return false;
}

function site_upgrade_pending() {
    global $CFG, $SITE;

    $dbversion = get_config('version', 0, 'site');
    if ($dbversion < $SITE->version) {
        return true;
    }

    return false;
}