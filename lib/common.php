<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 2:33 PM
 */

defined('INTERNAL') || die();

define('PARAM_RAW', 0); /* allow everything */
define('PARAM_INT', 1); /* parses string as integer */
define('PARAM_FLOAT', 2);
define('PARAM_TEXT', 3);
define('PARAM_USERNAME', 4);
define('PARAM_PASSWORD', 5);
define('PARAM_HASH', 6);
define('PARAM_BOOL', 7);
define('PARAM_ALPHA', 8);
define('PARAM_ALPHANUM', 9);
define('PARAM_BASE64', 10);
define('PARAM_EMAIL', 11);
define('PARAM_ALPHANUM_EXT', 12);
define('PARAM_SESSIONID', 13);
define('PARAM_PATH', 14);
define('PARAM_FILENAME', 14);
define('PARAM_LANG', 15);
define('PARAM_BLOCKTYPE', 16);
define('PARAM_PHONENUMBER', 17);
define('PARAM_FILEHASH', 18);


function required_param($paramname, $type) {
    if (isset($_POST[$paramname])) { // post is has higher priority
        return clean_param($_POST[$paramname], $type);
    } else if (isset($_GET[$paramname])) {
        return clean_param($_GET[$paramname], $type);
    }
    raise_exception("parameter $paramname not set");
}

function required_param_array($paramname, $type) {
    $ret = array();
    if (isset($_REQUEST[$paramname]) && is_array($_REQUEST[$paramname])) {
        foreach ($_REQUEST[$paramname] as $value) {
            $ret[] = clean_param($value, $type);
        }
        return $ret;
    }
    raise_exception("parameter $paramname not set");
}
//TODO: SECURITY
function require_login() {
    security::require_login();
require_permission('core/admin:manage');
}

function is_siteadmin($userid = 0) {
    return security::is_siteadmin($userid);
}


function isloggedin() {
    return security::isloggedin();
}

function is_loggedin() {
    return security::isloggedin();
}

function require_permission($permission, $ignoreadmin = false) {
    security::require_permission($permission, $ignoreadmin);
}

function has_permission($permission, $ignoreadmin = false) {
    return security::has_permission($permission, $ignoreadmin);
}

function optional_param($paramname, $default = '', $type = PARAM_TEXT) {
    if (isset($_POST[$paramname])) { // post has higher priority
        return clean_param($_POST[$paramname], $type);
    } else if (isset($_GET[$paramname])) {
        return clean_param($_GET[$paramname], $type);
    }
    return $default;
}

function optional_param_array($paramname, $default = '', $type = PARAM_TEXT) {
    $ret = array();
    if (isset($_REQUEST[$paramname]) && is_array($_REQUEST[$paramname])) {
        foreach ($_REQUEST[$paramname] as $value) {
            $ret[] = clean_param($value, $type);
        }
        return $ret;
    }
    return $default;
}

function clean_param($value, $type) {
    global $CFG;
    switch ($type) {
        case PARAM_INT:
            return intval($value);
        case PARAM_FLOAT:
            return sprintf('%.F', $value);
        case PARAM_TEXT:
            return htmlspecialchars($value, ENT_QUOTES | ENT_HTML5, "UTF-8", false);
        case PARAM_USERNAME:
            if(preg_match('/^([a-zA-Z0-9]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_PASSWORD:
            return $value;
        case PARAM_SESSIONID:
            $pattern = '/^([A-Za-z0-9\-_=]{44})$/';
            if(preg_match($pattern, $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_FILEHASH:
            if(preg_match('/^([a-fA-F0-9]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            if(preg_match('/^([a-zA-Z0-9\-_=]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_HASH:
            if(preg_match('/^([a-fA-F0-9]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_BOOL:
            if (strtolower($value) == 'true'  || strtolower($value) == 'yes'  || intval($value) == 1 || strtolower($value) == 'on') {
                return true;
            } else if (strtolower($value) == 'false'  || strtolower($value) == 'no'  || intval($value) == 0) {
                return false;
            }
            return !empty($value);
        case PARAM_RAW:
            return $value;
        case PARAM_ALPHA:
            if(preg_match('/^([[:alpha:]]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_ALPHANUM:
            if(preg_match('/^([[:alnum:]]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_ALPHANUM_EXT:
            if(preg_match('/^([[:alnum:]\-_,.]+)$/', $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_BASE64:
            $pattern = '/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=|[A-Za-z0-9+\/]{4})$/';
            if(preg_match($pattern, $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_EMAIL:
            //$pattern = '/^.{1,64}@.{1,255}$/';
            $pattern = '/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';
            if(preg_match($pattern, $value, $match) > 0) {
                return $match[0];
            }
            return '';
        case PARAM_PATH:
            $slash = '';
            if (strpos($value, '/') === 0) {
                $value = substr($value, 1);
                $slash = '/';
            }
            $value = preg_replace('/[^a-zA-Z0-9\/_.\ \-]/i', '', $value);
            $parts = explode('/', $value);
            $path_parts = array();
            foreach ($parts as $p) {
                if (empty($p) || $p == '.' || $p == '..') {
                    continue;
                }
                $path_parts[] = trim($p);
            }
            return $slash . implode('/', $path_parts);
        case PARAM_FILENAME:
            $value = preg_replace('/[^a-zA-Z0-9_.\ \-]/i', '', $value); //ALLOW Unicode filenames
            return trim($value);
        case PARAM_LANG:
            $value = strtolower($value);
            if (in_array($value, $CFG->languages)) {
                return $value;
            }
            return '';
        case PARAM_BLOCKTYPE:
            $value = strtolower($value);

            $allowedblocktypes = \core\block\block::get_all_blocktypes();
            foreach ($allowedblocktypes as $blocktype) {
                if ($blocktype->type === $value) {
                    return $value;
                }
            }
            return '';
        case PARAM_PHONENUMBER:
            if (preg_match('/(\+?[0-9\- ]{6,30})/', $value, $matches)) {
                return $matches[0];
            }
            return '';
    }
    return false;
}


function get_cookie($cookiename, $default = '', $type = PARAM_TEXT) {
    if (isset($_COOKIE[$cookiename])) {
        return clean_param($_COOKIE[$cookiename], $type);
    }
    return $default;
}

function remove_cookie($cookiename) {
    if (isset($_COOKIE[$cookiename])) {
        setcookie($cookiename, null, -1);
    }
    return true;
}

function set_cookie($cookiename, $value, $expire = 0, $path = null, $domain = null, $secure = null, $httponly = null) {
    //todo: secure cookies??
    return setcookie($cookiename, $value, $expire, $path, $domain, $secure, $httponly);
}


/**
 * @return surl
 *
 */
function current_page_url() {

    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
        $protocol = 'https';
    } else {
        $protocol = 'http';
    }

    if (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80') {
        $port = ':' . $_SERVER['SERVER_PORT'];
    } else {
        $port = '';
    }

    $url = $protocol . '://' . $_SERVER['SERVER_NAME'] . $port . '' . $_SERVER['REQUEST_URI'];
    return new surl($url);
}

function float_to_string($fl) {
    return number_format($fl, 2, ',', ' ');
}

//TODO: SECURITY
function raise_exception($explanation) {
    global $CFG, $PAGE;
    error_log($explanation, 0);
    if (!$CFG->debug) {
        security::die_bad_request();
    }
    if ($PAGE) {
        $output = $PAGE->get_output();

        if (!$output->is_output_started()) {
            echo $output->header(false);
        }
    }
    echo '<strong>EXCEPTION</strong> ';
    echo $explanation;
    echo '<br />';
    $bt = debug_backtrace();
    foreach ($bt as $trace) {
        echo 'Call to function <strong>' . $trace['function'] . '</strong> in file <em>' . $trace['file'] . '</em> on line <em>' . $trace['line'] . '</em><br />';
    }
    die;
}

function raise_warning($explanation) {
    global $CFG, $PAGE;
    $explanation = 'WARNING: ' . $explanation;
    if ($CFG->debug) {

        $output = $PAGE->get_output();
        if ($output->is_output_started()) {
            echo $output->render_messagebox($explanation, 'danger', false);
        } else {
            add_session_message($explanation, 'danger', false);
        }
    } else {

        error_log($explanation, 0);
    }
}


function sessionid() {
    return security::sessionid();
}

function require_sessionid() {
    security::require_sessionid();
}

function confirm_sessionid($sessionid) {
    return security::confirm_sessionid($sessionid);
}

function get_all_sites_list() {
    global $CFG;
    $sitesroot = $CFG->dirroot . '/s/';
    $files = scandir($sitesroot);
    $sites = array();
    foreach ($files as $f) {
        if ($f !== '..' &&
            is_dir($sitesroot . $f) &&
            file_exists($sitesroot . $f . '/config.php')) {
            $sites[] = $f;
        }
    }
    return $sites;
}

function purge_caches() {
    global $CFG;
    $cachedirs = array(
        $CFG->dataroot . '/cache'
    );
    $output = '';
    foreach ($cachedirs as $dir) {
        $output .= '$ rm -rvf ' . $dir . '*' . PHP_EOL;
        $output .= shell_exec('rm -rvf ' . $dir . '*');
    }

    update_revision();


    return $output;
}

function update_revision() {
    global $CFG;
    // get next site revision
    $nextrevision = time();
    $currentrevision = intval($CFG->revision);
    if ($nextrevision - $currentrevision > 30) {

    }
    set_config('revision', $nextrevision);
    return $nextrevision;
}

function get_string($identifier, $site = '') {
    global $CFG;
    return $CFG->stringmanager->get_string($identifier, $site);
}

/**
 * Redirects the user to the specified url. Uses header if message is empty, otherwise a meta tag
 *
 * @param     $url
 * @param     $message
 * @param int $timeout
 */
function redirect($url, $message = '', $timeout = -1) {
    //TODO: create url class
    global $PAGE;
    if ($timeout == -1 && empty($message)) {
        header('Location: ' . $url);
        die;
    }
    if ($PAGE) {
        $PAGE->add_meta_tag_httpequiv('refresh', $timeout . '; url=' . $url);
        $output = $PAGE->get_output();
        $continue = '<div style="width: 100%; text-align: center;"><a href="' . $url . '">Continue...</a></div>';
        if ($output->is_output_started()) {
            echo $output->render_messagebox($message, 'info', false);
            echo $continue;
            return;
        }
    }
    add_session_message($message, 'info');
    header('Location: ' . $url);
    die;
}


function add_session_message($message, $messagetype = 'info', $jsdismissable = true) {
    $m = new stdClass();
    $m->messagetext = $message;
    $m->messagetype = $messagetype;
    $m->jsdismissable = $jsdismissable;
    if (!isset($_SESSION['messages'])) {
        $_SESSION['messages'] = array();
    }
    $_SESSION['messages'][] = $m;
}

function add_to_log($action, $affected_table = '', $affected_objid = '', $additional_info = '', $severity = security::LOG_SEVERITY_OK, $url = null ) {
    security::add_to_log($action, $severity, $affected_table, $affected_objid, $additional_info, $url);
}

function get_http_boundary() {
    $hash = hash('sha256', random_bytes(256), true);
    $encoded = base64_encode($hash);
    return substr($encoded, 0, 32);
}

function pix_url($path, $site = '', $usefileextension = false, $addrevision = true) {
    // raise_warning('Deprecated function pix_url! Please use imgurl instead!');
    return imgurl($path, $site, $usefileextension, $addrevision);
}

function imgurl($path, $site = '', $usefileextension = false, $addrevision = true) {
    global $CFG;

    if (empty($site) || $site == 'site') {
        $path = '/s/pix/' . $path;
    } else if ($site == 'core') {
        $path = '/pix/' . $path;
    } else {
        //raise_warning('Unknown site ' . $site . '!');
        $path = '/s/pix/' . $path; // Backwards compatibility
    }
    $allowedformats = array('svg', 'png', 'jpg', 'gif', 'jpeg');

    if (!$usefileextension) {
        foreach ($allowedformats as $format) {
            $img = $CFG->dirroot . $path . '.' . $format;
            if (file_exists($img)) {
                $path .= '.' . $format;
                break;
            }
        }
    }
    return $CFG->wwwroot . $path . ($addrevision ? '?version=' . $CFG->revision : '');
}


/**
 * @param      $name string name of the setting
 * @param      $value string value of the setting
 * @param null $site string either 'core' or 'site', default 'core'. Other possible values:  true - for site, false - for core
 */
function set_config($name, $value, $site = null) {
    global $DB, $CFG;
    $params = array('name' => $name);
    if ($site === true) {
        $params['site'] = 'site';
    } else if (!is_null($site)) {
        if ($site != 'site' && $site != 'core') {
            raise_exception('invalid parameter for set_config. $site should be either \'site\' or \'core\'!');
        }
        $params['site'] = $site;
    } else {
        $params['site'] = 'core';
    }
    $conf = $DB->get_record('config', $params);
    if ($conf) {
        if ($conf->value != $value) {
            $conf->value = $value;
            $DB->update_record('config', $conf);
        }
    } else {
        $conf = new stdClass;
        $conf->name = $name;
        $conf->value = $value;
        $conf->site = $params['site'];
        $DB->insert_record('config', $conf);
    }
}

/**
 * @param      $name string name of the setting
 * @param null $default string default value if setting not found in DB
 * @param null $site string either 'core' or 'site', default 'core'. Other possible values:  true - for site, false - for core
 *
 * @return mixed|null value stored in db or $default if not found
 */
function get_config($name, $default = null, $site = null) {
    global $CFG, $DB;

    $params = array('name' => $name);
    if ($site === true) {
        $params['site'] = 'site';
    } else if (!is_null($site)) {
        if ($site != 'site' && $site != 'core') {
            raise_exception('invalid parameter for get_config. $site should be either \'site\' or \'core\'!');
        }
        $params['site'] = $site;
    } else {
        $params['site'] = 'core';
    }
    $conf = $DB->get_record('config', $params);
    if ($conf) {
        return $conf->value;
    }
    return $default;
}

/**
 * @param      $name string name of the setting
 * @param null $site string either 'core' or 'site', default 'core'. Other possible values:  true - for site, false - for core
 *
 * @return null
 */
function unset_config($name, $site = null) {
    global $DB;
    $params = array('name' => $name);
    if ($site === true) {
        $params['site'] = 'site';
    } else if (!is_null($site)) {
        if ($site != 'site' && $site != 'core') {
            raise_exception('invalid parameter for unset_config. $site should be either \'site\' or \'core\'!');
        }
        $params['site'] = $site;
    } else {
        $params['site'] = 'core';
    }
    $conf = $DB->get_record('config', $params);
    if ($conf) {
        $DB->delete_record('config', array('id' => $conf->id));
    }
    return null;
}

/*
 * @author: Jeffrey Sambells http://jeffreysambells.com/2012/10/25/human-readable-filesize-php
 */
function human_filesize($bytes, $dec = 2) {
    $size   = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $factor = intval(floor((strlen($bytes) - 1) / 3));

    return sprintf("%.{$dec}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}


/*
 * @author: Unknown
 */
function get_browser_name($user_agent) {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'IE';
    return 'Other';
}

function get_referer() {
    if (isset($_SERVER['HTTP_REFERER'])) {
        return new surl($_SERVER['HTTP_REFERER']);
    }
    return null;
}