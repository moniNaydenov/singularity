/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.filebrowserBrowseUrl = window.CFG.wwwroot + '/admin/filebrowser.php?browseonly=true';
    config.timestamp = window.CFG.revision;
    //config.allowedContent=  true;

    config.extraAllowedContent = 'p div li ul i h1 h2 h3 h4 h5 h6 (*)[*]{*};h*(text-*);i(*)[*]{*};span(fa*);span(text-*);span(m*)'; //TODO Bootstrap classes
    CKEDITOR.config.disableObjectResizing = true;
    CKEDITOR.dtd.$removeEmpty['i'] = false;
    CKEDITOR.dtd.$removeEmpty['span'] = false;
    //config.fontawesomePath = window.CFG.wwwroot + '/css.php?p=/scss/singularity.scss&version=' + config.timestamp;
    config.contentsCss = window.CFG.wwwroot + '/css.php?p=/scss/singularity.scss&version=' + config.timestamp;
    config.extraPlugins = '';
    config.indentUnit = 'rem';
    config.indentOffset = 1;
    config.justifyClasses = [ 'text-left', 'text-center', 'text-right', 'text-justify' ];
    config.removeButtons = 'FontSize,Font';
    config.removePlugins = 'form,save,flash'
};
