<?php

defined('INTERNAL') || die();

$roles = [
    'admin' => [
        'name' => 'Site Administrator',
        'permissions' => [
            'core/admin:manage'
        ]
    ],
    'user' => [
        'name' => 'User',
        'permissions' => [
            'core/user:editprofile',
            'core/user:changepassword',
            'core/user:viewprofile',
        ],
    ],
    'guest' => [
        'name' => 'Guest',
        'permissions' => [],
    ],
];