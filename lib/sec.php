<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 1/24/15
 * Time: 12:59 PM
 */

defined('INTERNAL') || die();

class security {

    private static $logfile = '/log.log';
    private static $logtable = 'log';
    private static $bcrypt_cost = 10;
    private static $headers = null;
    const LOG_SEVERITY_OK = 0;
    const LOG_SEVERITY_WARNING = 1;
    const LOG_SEVERITY_DANGER = 2;
    const LOG_SEVERITY_FAIL = 3;

    public static function require_login() {
        global $CFG, $PAGE;
        if (self::isloggedin() !== true) {
            $_SESSION['wanturl'] = $PAGE->get_url()->out();
            self::die_forbidden('You have to be logged in to view this page <a href="' . $CFG->wwwroot . '/login/index.php">go to login</a>');
        }
    }

    public static function require_permission($permission, $ignoreadmin = false) {
        $userhandler = \core\userhandler::instance_from_USER();
        if ($userhandler->has_permission($permission) === true) {
            return;
        }
        if (!$ignoreadmin && $userhandler->has_permission('core/admin:manage') === true) {
            return;
        }
        self::die_forbidden('You do not have permission to view this page');
    }

    public static function has_permission($permission, $ignoreadmin = false) {
        $userhandler = \core\userhandler::instance_from_USER();
        if ($userhandler->has_permission($permission) === true) {
            return true;
        }
        if (!$ignoreadmin && $userhandler->has_permission('core/admin:manage') === true) {
            return true;
        }
        return false;
    }

    public static function is_siteadmin($userid = 0) {
        global $DB, $USER;
        if ($userid == 0) {
            $userid = $USER->id;
        }
        return $DB->record_exists('admin', array('userid' => $userid));
    }

    public static function isloggedin() {
        global $USER;
        return
            !empty($_SESSION['userid']) &&
            !empty($_SESSION['sessionid']) &&
            !empty($USER) &&
            $_SESSION['userid'] == $USER->id &&
            $_SESSION['loggedin'] == 1;
    }

    public static function check_domain() {
        global $CFG;
        $url = current_page_url();

        if ($url->is_suburlof($CFG->wwwroot_surl) != true) {
            self::moved_permanently($CFG->wwwroot);
        }
    }

    public static function encrypt_password($password) {
        $hash = password_hash($password, PASSWORD_BCRYPT, array('cost' => self::$bcrypt_cost));
        return $hash;
    }

    public static function base64_encode_urlsafe($contents) {

        $contents = base64_encode($contents);
        $contents = str_replace(array('+', '/'), array('-', '_'), $contents);
        return $contents;
    }

    public static function safe_compare_string($hash1, $hash2) {
        $len = strlen($hash1);
        $len2 =  strlen($hash2);
        if ($len !== $len2) {
            return false;
        }

        $status = 0;
        for ($i = 0; $i < $len; $i++) {
            $status |= (ord($hash1[$i]) ^ ord($hash2[$i]));
        }

        return $status === 0;
    }

    public static function initiate_session() {
        global $DB;

        session_start();

        if (empty($_SESSION['sessionid'])) {
            self::regenerate_sessionid();
        }

        if (!empty($_SESSION['userid']) &&
            !empty($_SESSION['sessionid']) &&
            $_SESSION['loggedin'] == 1) {
            $user = $DB->get_record('user', array('id' => $_SESSION['userid']));
            $GLOBALS['USER'] = $user;
        } else {
            $user = $DB->get_record('user', array('username' => 'guest'));
            $GLOBALS['USER'] = $user;
        }
    }

    public static function regenerate_sessionid() {
        $contents = array(strval(time()));
        for ($i = 0; $i < 20; $i++) {
            $contents[] = strval(rand(100000,999999));
        }
        $hash = hash('sha256', implode('-', $contents), true);
        $hash = self::base64_encode_urlsafe($hash);
        $_SESSION['sessionid'] = $hash;
        session_regenerate_id();
    }


    public static function sessionid() {
        return isset($_SESSION['sessionid']) ? $_SESSION['sessionid'] : false;
    }

    public static function require_sessionid($sessionid = null) {

        if (is_null($sessionid)) {
            $sessionid = required_param('sessionid', PARAM_SESSIONID);
        }
        if (self::confirm_sessionid($sessionid) !== true) {
            self::die_forbidden('Invalid session!');
        }
    }

    public static function confirm_sessionid($sessionid) {

        if (empty($sessionid) || !is_string($sessionid)) {
            return false;
        }

        $currentsessionid = self::sessionid();
        if (empty($currentsessionid)) {
            return false;
        }

        return self::safe_compare_string($currentsessionid, $sessionid);
    }

    public static function add_to_log($action, $severity = self::LOG_SEVERITY_OK, $affected_table = '', $affected_objid = '', $additional_info = '', $url = null) {
        global $DB, $USER;

        $log = new stdClass;
        $log->severity = $severity;
        $log->action = $action;
        $log->affected_table = $affected_table;
        $log->affected_objid = $affected_objid;
        $log->additional_info = $additional_info;
        $log->ip = self::get_user_ip();
        $log->userid = $USER->id;
        $log->timestamp = time();

        if (is_null($url)) {
            $log->url = current_page_url()->out(false, true);
        } else if ($url instanceof surl) {
            $log->url = $url->out(false, true);
        } else {
            $url = new surl($url);
            $log->url = $url->out(false, true);
        }
        $DB->insert_record('log', $log);
    }

    public static function redirect($url = '', $headers = []) {
        header('HTTP/1.1 302 Found');
        header('Location: ' . $url);
        foreach ($headers as $header) {
            header($header);
        }
        die;
    }

    public static function moved_permanently($url, $headers = []) {
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $url);
        foreach ($headers as $header) {
            header($header);
        }
        die;
    }

    public static function die_forbidden($message = '', $headers = []) {
        global $PAGE;
        header("HTTP/1.1 403 Forbidden");
        foreach ($headers as $header) {
            header($header);
        }
        $output = $PAGE->get_output();
        echo $output->header(false);
        echo $message;
        echo $output->footer(false);
        die;
    }

    public static function die_bad_request($message = '', $headers = []) {
        header("HTTP/1.1 400 Bad request");
        foreach ($headers as $header) {
            header($header);
        }
        echo $message;
        die;
    }

    public static function die_404($message = '', $headers = []) {
        global $CFG, $PAGE;
        require_once($CFG->dirroot . '/404.php');
        /*header("HTTP/1.1 404 Not Found");
        foreach ($headers as $header) {
            header($header);
        }
        $output = $PAGE->get_output();
        echo $output->header(false);
        echo $message;
        echo $output->footer(false);*/
        die;
    }

    public static function die_416($message = '', $headers = []) {
        header("HTTP/1.1 416 Range Not Satisfiable");
        foreach ($headers as $header) {
            header($header);
        }
        echo $message;
        die;
    }

    public static function get_user_ip() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        return $_SERVER['REMOTE_ADDR'];
    }


    //TODO:
    public static function get_new_captcha() {
        global $DB;
        $now = time();

        $DB->delete_record_where('captcha', 'timestamp <= :now', array('now' => ($now - 1800)));

        $text = md5(strval($now) . ' - ' . rand(10000, 99999));
        $text = base64_encode($text);
        $text = substr($text, 0, 8);
        $captcha = new stdClass;
        $captcha->content = $text;
        $captcha->timestamp = $now;
        $captcha->id = $DB->insert_record('captcha', $captcha);
        return $captcha;
    }

    //TODO:
    public static function check_captcha($id, $input) {
        global $DB;
        $captcha = $DB->get_record('captcha', array('id' => $id));
        if (empty($captcha)) {
            return false;
        }
        $captcha->content = strtolower($captcha->content);
        $input = strtolower($input);
        if (self::safe_compare_string($captcha->content, $input)) {
            $DB->delete_record_where('captcha', 'id = :id', array('id' => $captcha->id));
            return true;
        }
        return false;
    }

    public static function get_log_file() {
        return strval(self::$logfile);
    }


}

