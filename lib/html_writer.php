<?php
/**
 * Created by PhpStorm.
 * User: moni
 * Date: 3/5/15
 * Time: 12:43 PM
 */

defined('INTERNAL') || die();

/**
 * Class html_writer
 */
class html_writer {
    /**
     * @param        $text
     * @param string $type
     * @param array  $attributes
     *
     * @return string
     */
    public static function messagebox($text, $type = 'warning', $attributes = array()) {
        $class = isset($attributes['class']) ? $attributes['class'] : '';
        $attributes['class'] = "alert alert-$type " . $class;
        $attributes['role'] = 'alert';
        $html = self::tag('div', $text, $attributes);
        return $html;
    }

    /**
     * @param $size
     * @param $text
     *
     * @return string
     */
    public static function heading($size, $text, $attributes = array()) {
        $size = intval($size);
        if ($size < 1 || $size > 6) {
            $size = 6;
        }
        return self::tag('h' . strval($size), $text, $attributes);
    }

    /**
     * @param        $url
     * @param        $text
     * @param string $classname
     * @param array  $attributes
     *
     * @return string
     */
    public static function link($url, $text, $classname = '', $attributes = array()) {
        if (!($url instanceof surl)) {
            $url = new surl($url);
        }
        $attributes['href'] = $url->out();
        if (!empty($classname)) {
            if (isset($attributes['class'])) {
                $attributes['class'] = $attributes['class'] . ' ' . $classname;
            } else {
                $attributes['class'] = $classname;
            }
        }
        return self::tag('a', $text, $attributes);

    }

    /**
     * @param        $tagname
     * @param string $content
     * @param array  $attributes
     *
     * @return string
     */
    public static function tag($tagname, $content = '', $attributes = array()) {
        $html = self::start_tag($tagname, $attributes);
        $html .= $content;
        $html .= self::end_tag($tagname);

        return $html;
    }


    /**
     * @param       $tagname
     * @param array $attributes
     *
     * @return string
     */
    public static function empty_tag($tagname, $attributes = array()) {
        $html = "\t<$tagname ";
        foreach ($attributes as $attribute => $value) {
            $html .= sprintf(' %s="%s"', $attribute, $value);
        }

        $html .= "/>" . PHP_EOL;
        return $html;
    }

    /**
     * @param       $tagname
     * @param array $attributes
     *
     * @return string
     */
    public static function start_tag($tagname, $attributes = array()) {
        $html = "<$tagname ";
        foreach ($attributes as $attribute => $value) {
            $html .= sprintf(' %s="%s"', $attribute, $value);
        }
        $html .= '>' . PHP_EOL;
        return $html;
    }

    /**
     * @param $tagname
     *
     * @return string
     */
    public static function end_tag($tagname) {
        $html = "</$tagname>" . PHP_EOL;
        return $html;
    }

    /**
     * @param string $content
     * @param string $classname
     * @param array  $attributes
     *
     * @return string
     */
    public static function div($content = '', $classname = '', $attributes = array()) {
        if (!empty($classname)) {
            if (isset($attributes['class'])) {
                $attributes['class'] = $attributes['class'] . ' ' . $classname;
            } else {
                $attributes['class'] = $classname;
            }
        }
        return self::tag('div', $content, $attributes);
    }

    /**
     * @param string $content
     * @param string $classname
     * @param array  $attributes
     *
     * @return string
     */
    public static function span($content = '', $classname = '', $attributes = array()) {
        if (!empty($classname)) {
            if (isset($attributes['class'])) {
                $attributes['class'] = $attributes['class'] . ' ' . $classname;
            } else {
                $attributes['class'] = $classname;
            }
        }
        return self::tag('span', $content, $attributes);
    }

    /**
     * @param string $content
     *
     * @return string
     */
    public static function script($content = '') {
        return self::tag('script', $content . PHP_EOL, array('type' => 'text/javascript'));
    }



    /**
     * returns html needed for a bootstrap icon in the form
     * <span class="glyphicon glyphicon-$iconname $classname"></span>
     *
     * @param string $iconname - name of the icon
     * @param string $classname - additional class name to be added
     *
     * @return string html markup
     */
    public static function bootstrap_icon($iconname, $classname = '') {

        $html = "<span class='glyphicon glyphicon-$iconname $classname'></span>";
        return $html;
    }

    /**
     * @param $content
     * @param string $type
     * @return string
     */
    public static function bootstrap_label($content, $type = '') {
        $types = array('primary', 'success', 'info', 'warning', 'danger');
        if (!in_array($type, $types)) {
            $type = 'default';
        }
        return self::span($content, 'label label-' . $type);
    }
    

    /**
     * @param        $label
     * @param string $onclick
     * @param string $href
     * @param string $btntype
     * @param array  $attributes
     *
     * @return string
     */
    public static function link_button($label, $onclick = '', $href = 'javascript: void(0);', $btntype = 'default', $attributes = array()) {
        $attrs = array(
            'onclick' => $onclick,
            'href' => $href,
            'class' => 'btn btn-'.$btntype
        );
        if (isset($attributes['class'])) {
            $attrs['class'] .= ' '. $attributes['class'];
            unset($attributes['class']);
        }
        $attrs = array_merge($attrs, $attributes);

        $html = self::tag('a', $label, $attrs);

        return $html;
    }


    /**
     * @param string $content
     * @param string $alt
     * @param string $class
     * @param array  $attributes
     *
     * @return string
     */
    public static function img($src, $alt = '', $classname = '', $attributes = array()) {

        if (!empty($classname)) {
            if (isset($attributes['class'])) {
                $attributes['class'] = $attributes['class'] . ' ' . $classname;
            } else {
                $attributes['class'] = $classname;
            }
        }
        $attributes['src'] = $src;
        if (!empty($alt)) {
            $attributes['alt'] = $alt;
        }
        $html = self::empty_tag('img', $attributes);

        return $html;
    }

    public static function strong($text, $attributes = array()) {
        return self::tag('strong', $text, $attributes);
    }

    /**
     * @param $data
     * @param string $classname
     * @param bool $firstrowasheader
     * @param array $attributes
     * @return string
     */
    public static function table($data, $classname = 'table table-striped', $firstrowasheader = true, $attributes = array()) {

        if (!empty($classname)) {
            if (isset($attributes['class'])) {
                $attributes['class'] = $attributes['class'] . ' ' . $classname;
            } else {
                $attributes['class'] = $classname;
            }
        }

        if (empty($data) || !is_array($data)) {
            return self::tag('table', '', $attributes);
        }

        $html = self::start_tag('table', $attributes);
        $r = 0;
        foreach ($data as $row) {
            if ($r == 0) {
                $html .= self::start_tag('thead');
                if (!$firstrowasheader) {
                    $html .= self::end_tag('thead');
                    $html .= self::start_tag('tbody');
                }
            }

            $html .= self::start_tag('tr');
            $tag = 'td';
            if ($firstrowasheader && $r == 0) {
                $tag = 'th';
            }
            if (is_object($row)) {
                $row = (array)$row;
            }
            if (is_array($row)) {
                foreach ($row as $value) {
                    $html .= self::tag($tag, $value);
                }
            }

            $html .= self::end_tag('tr');
            if ($r == 0 && $firstrowasheader) {
                $html .= self::end_tag('thead');
                $html .= self::start_tag('tbody');
            }
            $r++;

        }

        $html .= self::end_tag('tbody');
        $html .= self::end_tag('table');
        return $html;

    }
}

//TODO: tables