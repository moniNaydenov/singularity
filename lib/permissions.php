<?php

defined('INTERNAL') || die();

$permissions = [
    [
        'name' => 'core/admin:manage',
        'description' => 'Manage site administration',
    ],
    [
        'name' => 'core/user:editprofile',
        'description' => 'Edit user profile',
    ],
    [
        'name' => 'core/user:changepassword',
        'description' => 'Change user password',
    ],
    [
        'name' => 'core/user:viewprofile',
        'description' => 'View user profile',
    ]
];